package com.me.mygdxgame.entite;

import java.io.Serializable;
import java.util.ArrayList;

public class Monstre extends Etre implements Serializable {

	private static final long serialVersionUID = 1L;
	private String image;

	private static ArrayList<Monstre> monstresQuete;

	static {
		monstresQuete = new ArrayList<Monstre>();
		monstresQuete.add(new Monstre(
				"data/images/mondes/PL/monstres/vampireZombieGarou.png",
				"Chef Vampire Zombie Garou", 100, 25, 7));
		monstresQuete.add(new Monstre("data/maps/Image/indien.png",
				"Indien Prétendant", 30, 10, 2));
		monstresQuete.add(new Monstre("data/maps/Image/chefindien.png",
				"Chef Indien", 40, 12, 3));
	}

	public Monstre(String image, String nom, int vie, int attaque, int defense) {
		super(nom, vie, attaque, defense);
		this.image = image;
	}

	@Override
	public String getImage() {
		return image;
	}

	public static Monstre getMonstreQuete(String indiceQuete, int avancement) {
		if (indiceQuete.equals("PLP"))
			return monstresQuete.get(0);
		if (indiceQuete.equals("GIP") && avancement == 2)
			return monstresQuete.get(1);
		if (indiceQuete.equals("GIP") && avancement == 3)
			return monstresQuete.get(2);
		return null;
	}

}
