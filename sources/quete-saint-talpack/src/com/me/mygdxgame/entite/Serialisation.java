package com.me.mygdxgame.entite;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Serialisation {

	private static ArrayList<Monstre> monstresGI = new ArrayList<Monstre>();
	private static ArrayList<Monstre> monstresPH = new ArrayList<Monstre>();
	private static ArrayList<Monstre> monstresPL = new ArrayList<Monstre>();
	private static ArrayList<Monstre> monstresIU = new ArrayList<Monstre>();
	private static ArrayList<Monstre> monstresMA = new ArrayList<Monstre>();
	private static ArrayList<Monstre> monstresSF = new ArrayList<Monstre>();

	public static void main(String[] args) throws FileNotFoundException, IOException {

		monstresGI.add(new Monstre("data/images/mondes/GI/monstres/cactusPiquant.png",
				"Cactus piquant", 10, 2, 2));
		monstresGI.add(new Monstre("data/images/mondes/GI/monstres/serpentTerrose.png",
				"Serpent Terrose", 15, 5, 1));
		monstresGI.add(new Monstre("data/images/mondes/GI/monstres/wildAmerican.png",
				"Wild american", 20, 7, 3));
		monstresGI.add(new Monstre("data/images/mondes/GI/monstres/bisonRuse.png",
				"Bison rus�", 25, 7, 8));
		monstresGI.add(new Monstre("data/images/mondes/GI/monstres/buffleBill.png",
				"Buffle Bill", 30, 12, 3));
		
		ObjectOutputStream fOut = new ObjectOutputStream(new FileOutputStream(
				"GI/listeMonstres.talpack"));
		fOut.writeObject(monstresGI);
		fOut.close();

		monstresPH.add(new Monstre("data/images/mondes/PH/monstres/caillouFeroce.png",
				"Caillou f�roce", 10, 2, 2));
		monstresPH.add(new Monstre("data/images/mondes/PH/monstres/poulMoute.png",
				"Poulmoute", 15, 5, 1));
		monstresPH.add(new Monstre("data/images/mondes/PH/monstres/guerrierIsole.png",
				"Guerrier isol�", 20, 7, 3));
		monstresPH.add(new Monstre("data/images/mondes/PH/monstres/tigreASabreDeDents.png",
				"Tigre � sabre de dents", 25, 7, 8));
		monstresPH.add(new Monstre("data/images/mondes/PH/monstres/mammouthon.png",
				"Mammouthon", 30, 12, 3));
		
		fOut = new ObjectOutputStream(new FileOutputStream(
				"PH/listeMonstres.talpack"));
		fOut.writeObject(monstresPH);
		fOut.close();

		monstresPL.add(new Monstre("data/images/mondes/PL/monstres/planteVegetarienne.png",
				"Plante V�g�tarienne", 10, 2, 2));
		monstresPL.add(new Monstre("data/images/mondes/PL/monstres/nounours.png",
				"Bisounours radioactif", 15, 5, 1));
		monstresPL.add(new Monstre("data/images/mondes/PL/monstres/korriganGster.png",
				"Korrigan Gster", 20, 7, 3));
		monstresPL.add(new Monstre("data/images/mondes/PL/monstres/poneySatanique.png",
				"Poney Satanique", 25, 7, 8));
		monstresPL.add(new Monstre("data/images/mondes/PL/monstres/vampireZombieGarou.png",
				"Vampire Zombie Garou", 30, 12, 3));
		
		fOut = new ObjectOutputStream(new FileOutputStream(
				"PL/listeMonstres.talpack"));
		fOut.writeObject(monstresPL);
		fOut.close();

		monstresIU.add(new Monstre("data/images/mondes/IU/monstres/poubellepleine.png",
				"Poubelle pleine", 10, 2, 2));
		monstresIU
				.add(new Monstre("data/images/mondes/IU/monstres/dst.png", "DST", 15, 5, 1));
		monstresIU.add(new Monstre("data/images/mondes/IU/monstres/hipster.png", "Hipster",
				20, 7, 3));
		monstresIU.add(new Monstre("data/images/mondes/IU/monstres/secretaire.png",
				"Secr�taire", 25, 7, 8));
		monstresIU.add(new Monstre("data/images/mondes/IU/monstres/professeurEnrage.png",
				"Professeur enrag�", 30, 12, 3));
		
		fOut = new ObjectOutputStream(new FileOutputStream(
				"IU/listeMonstres.talpack"));
		fOut.writeObject(monstresIU);
		fOut.close();

		monstresMA.add(new Monstre("data/images/mondes/MA/monstres/fange.png", "Fange", 10,
				2, 2));
		monstresMA.add(new Monstre(
				"data/images/mondes/MA/monstres/nainNonConchyliculteur.png",
				"Nain non conchyliculteur", 15, 5, 1));
		monstresMA.add(new Monstre("data/images/mondes/MA/monstres/menestrelLaid.png",
				"M�nestrel laid", 20, 7, 3));
		monstresMA.add(new Monstre("data/images/mondes/MA/monstres/pouilleuse.png",
				"Pouilleuse", 25, 7, 8));
		monstresMA.add(new Monstre("data/images/mondes/MA/monstres/baronFleur.png",
				"Baron Fleur", 30, 12, 3));
		
		fOut = new ObjectOutputStream(new FileOutputStream(
				"MA/listeMonstres.talpack"));
		fOut.writeObject(monstresMA);
		fOut.close();

		monstresSF.add(new Monstre("data/images/mondes/SF/monstres/dechetToxique.png",
				"D�chet Toxique", 10, 2, 2));
		monstresSF.add(new Monstre("data/images/mondes/SF/monstres/loupPhoque.png",
				"Loup-Phoque", 15, 5, 1));
		monstresSF.add(new Monstre("data/images/mondes/SF/monstres/sousMarinEnuclee.png",
				"Sous-marin �nucl��", 20, 7, 3));
		monstresSF.add(new Monstre("data/images/mondes/SF/monstres/genciveCostaud.png",
				"Gencive Costaud", 25, 7, 8));
		monstresSF.add(new Monstre("data/images/mondes/SF/monstres/bathyscaphandre.png",
				"Bathyscaphandre", 30, 12, 3));
		
		fOut = new ObjectOutputStream(new FileOutputStream(
				"SF/listeMonstres.talpack"));
		fOut.writeObject(monstresSF);
		fOut.close();
	}
}
