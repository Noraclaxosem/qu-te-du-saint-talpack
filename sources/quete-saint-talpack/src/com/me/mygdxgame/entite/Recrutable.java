package com.me.mygdxgame.entite;

import java.util.ArrayList;

public class Recrutable extends Etre {

	private static final long serialVersionUID = 1L;
	private String image;
	private static ArrayList<Recrutable> recrues;

	static {
		recrues = new ArrayList<Recrutable>();
		recrues.add(new Recrutable("Ponette moqueuse", 20, 8, 2,
				"data/images/recrues/ponetteBleue.png"));
		recrues.add(new Recrutable("Chasseuse", 40, 15, 0,
				"data/images/recrues/chasseuse.png"));
		recrues.add(new Recrutable("Vampire Zombie Garou", 100, 20, 5,
				"data/images/recrues/vampireZombieGarou.png"));
	}

	public Recrutable(String nom, int vie, int attaque, int defense,
			String image) {
		super(nom, vie, attaque, defense);
		this.image = image;
	}

	@Override
	public String getImage() {
		return image;
	}

	public static Etre getRecrue(String indiceQuete) {
		if (indiceQuete.equals("PLS1"))
			return recrues.get(0);
		else if (indiceQuete.equals("PLP"))
			return recrues.get(1);
		else if (indiceQuete.equals("PLS2"))
			return recrues.get(2);
		return null;
	}

	// @Override
	// public TextureRegion getCombatPosture() {
	// return image;
	// }
	//
	// @Override
	// public boolean isSelected() {
	// return isSelected;
	// }
	//
	// @Override
	// public void selected(boolean sel) {
	// isSelected = sel;
	// }
	//
	// @Override
	// public void dispose() {
	// }
	//
	// @Override
	// public int getPositionCombat() {
	// return positionCombat;
	// }
	//
	// @Override
	// public void setPositionCombat(int position) {
	// positionCombat = position;
	// }

}
