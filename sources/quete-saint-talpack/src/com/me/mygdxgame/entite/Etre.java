package com.me.mygdxgame.entite;

import java.io.Serializable;


public abstract class Etre extends Entite implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6735048682106243964L;
	private String nom;
	private int vie,attaque,defense,maxVie;
	
	private Equipement arme;
	private Equipement armure;
	private Equipement accessoire;
	
	public Etre(String nom, int vie, int attaque, int defense){
		this.maxVie = vie;
		this.vie = maxVie;
		this.nom = nom;
		this.attaque = attaque;
		this.defense = defense;
	}
	
	public abstract String getImage();
	
	public int getVie(){
		return vie;
	}
	
	public void setVie(int vie){
		this.vie = vie;
	}
	
	public int getMaxVie(){
		return maxVie;
	}

	public String getNom() {
		return nom;
	}

	public int getAttaque() {
		return attaque;
	}

	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}
	
	public boolean equiper(Equipement eq){
		if(eq.getClass()==Arme.class){
			arme = eq;
			return true;
		}
		
		if(eq.getClass()==Armure.class){
			armure = eq;
			return true;
		}
		
		if(eq.getClass()==Accessoire.class){
			accessoire =  eq;
			return true;
		}
		
		return false;
	}

	public Equipement getArme() {
		return arme;
	}

	public Equipement getArmure() {
		return armure;
	}

	public Equipement getAccessoire() {
		return accessoire;
	}
	
	public int getEquipementVie(){
		int vie = 0;
		if(arme!=null){
			vie +=getArme().getEffetVie();
		}
		if(armure!=null){
			vie +=armure.getEffetVie();
		}
		if(accessoire!=null){
			vie+=accessoire.getEffetVie();
		}
		return vie;
	}
	
	public int getEquipementAttaque(){
		int attaque = 0;
		if(arme!=null){
			attaque +=arme.getEffetAttaque();
		}
		if(armure!=null){
			attaque +=armure.getEffetAttaque();
		}
		if(accessoire!=null){
			attaque+=accessoire.getEffetAttaque();
		}
		return attaque;
	}
	
	public int getEquipementDefense(){
		int defense = 0;
		if(arme!=null){
			defense +=arme.getEffetDefense();
		}
		if(armure!=null){
			defense +=armure.getEffetDefense();
		}
		if(accessoire!=null){
			defense +=accessoire.getEffetDefense();
		}
		return defense;
	}

	public void setArme(Equipement arme) {
		this.arme = arme;
	}

	public void setArmure(Equipement armure) {
		this.armure = armure;
	}

	public void setAccessoire(Equipement accessoire) {
		this.accessoire = accessoire;
	}
}
