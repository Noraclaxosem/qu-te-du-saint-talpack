package com.me.mygdxgame.entite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class Combattant{
	private Etre combattant;
	private Rectangle zone;
	private boolean battu = false;
	private Texture image;

	public Combattant(Etre combattant, float x , float y){
		this.combattant = combattant;
		image = new Texture(Gdx.files.internal(combattant.getImage()));
		zone = new Rectangle(x,y,image.getWidth(),image.getHeight());
	}
	
	public Texture getCombatImage() {
		return image;
	}

	public float getX() {
		return zone.x;
	}

	public float getY() {
		return zone.y;
	}
	
	public void setX(float x){
		zone.x = x;
	}
	
	public void setY(float y){
		zone.y=y;
	}

	public Rectangle getRectangle() {
		return zone;
	}

	public int getVie() {
		return combattant.getVie();
	}
	
	public int getMaxVie(){
		return combattant.getMaxVie();
	}

	public void setVie(int i) {
		combattant.setVie(i);
	}
	
	public boolean isBattu() {
		return battu;
	}

	public void setBattu(boolean battu) {
		this.battu = battu;
	}

	public boolean isPayer() {
		return combattant.getClass() == Joueur.class;
	}

	public boolean isMonstre() {
		return combattant.getClass() == Monstre.class;
	}

	public String getNom() {
		return combattant.getNom();
	}
	
	public int getAttaque(){
		return combattant.getAttaque();
	}

	public int getDefense(){
		return combattant.getDefense();
	}
	
	public void dispose(){
		image.dispose();
	}
	
	public Etre getEtre(){
		return combattant;
	}
	
	
}
