package com.me.mygdxgame.entite;

public abstract class Equipement extends Objet{
	private Effet e;

	public Equipement(String nom, String image, String description, Effet e) {
		super(nom, image, description);
		this.e = e;
	}

	@Override
	public void utiliser(Etre e) {
		e.equiper(this);
	}
	
	public int getEffetVie(){
		return e.getEffetVie();
	}
	
	public int getEffetAttaque(){
		return e.getEffetAttaque();
	}
	
	public int getEffetDefense(){
		return e.getEffetDefense();
	}
}
