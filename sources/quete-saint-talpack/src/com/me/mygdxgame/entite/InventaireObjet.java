package com.me.mygdxgame.entite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class InventaireObjet {
	public static final int TAILLE_OBJET = 40;
	private Texture image;
	private Rectangle pos;
	private Objet obj;
	
	public InventaireObjet(Objet obj, float x, float y){
		this.obj = obj;
		image = new Texture(Gdx.files.internal(obj.getImage()));
		pos = new Rectangle(x,y,TAILLE_OBJET,TAILLE_OBJET);
	}
	
	public Rectangle getRectangle(){
		return pos;
	}
	
	public Objet getObj(){
		return obj;
	}
	
	public Texture getTexture(){
		return image;
	}
	
	public boolean estConsomable(){
		return obj.getClass() == Consommable.class;
	}
	
	public boolean estEquipement(){
		return obj.getClass().getSuperclass() == Equipement.class;
	}
	
	public void dispose(){
		image.dispose();
	}
	
	public void setX(float x){
		pos.x = x;
	}
	public void setY(float y){
		pos.y = y;
	}
}
