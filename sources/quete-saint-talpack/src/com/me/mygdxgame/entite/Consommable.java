package com.me.mygdxgame.entite;

public class Consommable extends Objet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Effet e;

	public Consommable(String nom, String image, String description, Effet e) {
		super(nom, image, description);
		this.e = e;
	}

	public void utiliser(Etre etre){
		if(etre.getVie()+e.getEffetVie()<=etre.getMaxVie()){
			etre.setVie(etre.getVie()+e.getEffetVie());
		}else{
			etre.setVie(etre.getMaxVie());
		}
		etre.setAttaque(etre.getAttaque()+e.getEffetAttaque());
		etre.setDefense(etre.getDefense()+e.getEffetDefense());
	}

}
