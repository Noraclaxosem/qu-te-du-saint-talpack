package com.me.mygdxgame.entite;

import java.util.HashMap;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.me.mygdxgame.heros.Achievement;
import com.me.mygdxgame.heros.MetaAchievement;
import com.me.mygdxgame.heros.Principale;
import com.me.mygdxgame.heros.Quete;
import com.me.mygdxgame.heros.Secondaire;
import com.me.mygdxgame.screen.GameScreen;
import com.me.mygdxgame.screen.InventaireScreen;
import com.me.mygdxgame.screen.PauseScreen;

public class Joueur extends Etre implements InputProcessor {

	private static final long serialVersionUID = 1L;
	public static final int DIR_HAUT = 0;
	public static final int DIR_DROITE = 1;
	public static final int DIR_BAS = 2;
	public static final int DIR_GAUCHE = 3;
	public static final int DIR_DEFAULT = -1;
	public int etatgame = GameScreen.GAME_RUNNING;
	public int cptTrans = 90;

	private static HashMap<String, Quete> quetes;
	private static HashMap<String, MetaAchievement> achievements;

	private static String[] indiceQuete = { "IUTP", "IUTS1", "IUTS2", "SFP",
			"SFS1", "SFS2", "MAP", "MAS1", "MAS2", "PLP", "PLS1", "PLS2",
			"GIP", "GIS1", "GIS2", "PHP", "PHS1", "PHS2" };

	private static String[] nomQuete = { "Aller en cours de reseau",
			"Obtenir une salle info", "Rentrer dans le secretariat",
			"Trouver le purificateur", "Faire nager un nain conchyliculteur",
			"Trouver que tout est de la faute des chinois",
			"Habiller un wesh correctement", "Draguer une wesh",
			"Parler en verlan latin",
			"Vaincre la secte des vampires garous zombies roux",
			"Manger un poney", "Marier un poney avec un vampire garou",
			"Sortir avec la fille du chef",
			"Flageller un am�ricain � coup de nain conchyliculteur",
			"Construire un camp pour am�ricains anorexiques", "Faire du feu",
			"Trouver la grotte du mammouth supr�me",
			"Tailler un silex avec un nain conchyliculteur" };

	private static final int[] nbEtapesQuete = { 51, 52, 53, 51, 52, 53, 51,
			52, 53, 5, 3, 4, 6, 4, 3, 51, 52, 53 };

	/**
	 * TOTAL : M�tasucc�s : finir tous les autres succ�s QP : Qu�te Principale
	 * **FAIT** COFFRE : 5 coffres ouverts **FAIT** CG : 20 combats gagn�s
	 * **FAIT** TRESOR : Tr�sor Sp�cial trouv� RECRUE : 3 recrues enr�l�es BOSS
	 * : Boss du monde battu NAIN : Parler aux 4 nains **FAIT** MONDE : Parler
	 * aux 2 PNJs pr�sentateurs **FAIT** QS1 : Qu�te secondaire 1 **FAIT** QS2 :
	 * Qu�te secondaire 2 **FAIT**
	 */

	private static String[] indiceAchievement = { "IUTOTAL", "IUQP",
			"IUCOFFRE", "IUCG", "IUTRESOR", "IURECRUE", "IUBOSS", "IUNAIN",
			"IUMONDE", "IUQS1", "IUQS2", "SFTOTAL", "SFQP", "SFCOFFRE", "SFCG",
			"SFTRESOR", "SFRECRUE", "SFBOSS", "SFNAIN", "SFMONDE", "SFQS1",
			"SFQS2", "MATOTAL", "MAQP", "MACOFFRE", "MACG", "MATRESOR",
			"MARECRUE", "MABOSS", "MANAIN", "MAMONDE", "MAQS1", "MAQS2",
			"PLTOTAL", "PLQP", "PLCOFFRE", "PLCG", "PLTRESOR", "PLRECRUE",
			"PLBOSS", "PLNAIN", "PLMONDE", "PLQS1", "PLQS2", "GITOTAL", "GIQP",
			"GICOFFRE", "GICG", "GITRESOR", "GIRECRUE", "GIBOSS", "GINAIN",
			"GIMONDE", "GIQS1", "GIQS2", "PHTOTAL", "PHQP", "PHCOFFRE", "PHCG",
			"PHTRESOR", "PHRECRUE", "PHBOSS", "PHNAIN", "PHMONDE", "PHQS1",
			"PHQS2" };

	private static String[] nomAchievement = { "DUT en poche!",
			"Major de Promo", "Mais que font-ils ici?!", "Psychopathe",
			"Dans le pass� ca valait des milliards", "Charmeur", "Bon Dodo",
			"Ils sont partout", "L'IUT est plus grand que tu ne le penses",
			"Face � l'incompr�hension du monde", "Mission Impossible",
			"SharkNami", "� l'envers, � l'endroit", "L'Atlantide!",
			"WaterWorld", "Matry McFly", "Orateur", "Demi-Dieu Branchiflorien",
			"Ils respirent m�me sous l'eau!", "Kevin Costner", "Ma�tre-Nageur",
			"Il faut un coupable", "Nobles et gueux",
			"Fais pas ton Noble ouesh!", "De l'or ! De l'or ! (Et bah non)",
			"George R. R. Martin", "Massalia", "Parrain", "Jtai niker ca ras",
			"Qui tu veux que �a soit, un ours en deltaplane ?", "Sacr� Graal",
			"D�fil� de mode", "Bilingue", "Over the Rainbow",
			"Voyage en Transylvanie", "#yOl0 #sWwA�g",
			"Dancing Zombies with a moustache", "Nyan Cat addict",
			"Alexandra Lederman au poney club", "Si quelqu'un m'entend",
			"Leur monture favorite", "MyLittlePony", "Ponettivore",
			"Mariage pour tous", "Melting Pot", "Lucky Luke",
			"Ouchaca Ouchaca Ouchaca", "George Armstrong Custer",
			"Totemisation", "Communiste !", "Ready for powa",
			"Amenez-leur de la bi�re!", "Peau Rouge", "Devoir d'�tat",
			"Diablotin", "Vous avez pass� l'�ge de pierre",
			"�a va �tre tout noir", "�a a l'air bon!", "Crime de Gu��",
			"Smurffmut", "Rrrrrr", "Dragmut", "Qu'est qu'c'est c'ptit bout ?",
			"Monsieur, Madame, Accord�on", "Marco Polo", "Tailleur de pierre", };

	private static final int FRAME_COLS = 4; // nombre de colone du sprite sheet
												// du hero
	private static final int FRAME_ROWS = 4; // nombre de ligne
	private static final float DUREEFRAME = 0.115f;

	private TextureRegion[][] framesHero;
	private Animation walkAnimation_gauche;
	private Animation walkAnimation_droite;
	private Animation walkAnimation_haut;
	private Animation walkAnimation_bas;
	private Texture walkSheet;
	// private TextureRegion[] walkFrames;
	private TextureRegion currentFrame;
	private float stateTime;
	private int x, y;

	private int dir; // direction du personnage
	private int nextdir;
	private GameScreen gamescreen;

	private Vector<Objet> listObjets;
	private String imageCourante = "hero";
	private long debut;
	public static final int MAX_OBJETS = 18;

	public static int score = 0;

	public Joueur(String nom, int vie, int attaque, int defense) {
		// init animation
		super(nom, vie, attaque, defense);
		changerImg("hero");
		stateTime = 0f;
		score = 0;

		dir = DIR_DEFAULT;
		nextdir = DIR_DEFAULT;
		currentFrame = framesHero[0][0];
		quetes = new HashMap<String, Quete>();
		for (int i = 0; i < indiceQuete.length; i++) {
			if (i % 3 == 0) {
				quetes.put(indiceQuete[i], new Principale(nomQuete[i],
						nbEtapesQuete[i]));
			} else {
				quetes.put(indiceQuete[i], new Secondaire(nomQuete[i], 53,
						nbEtapesQuete[i]));
			}
		}
		achievements = new HashMap<String, MetaAchievement>();
		MetaAchievement temp = null;
		for (int i = 0; i < indiceAchievement.length; i++) {
			switch (i % 11) {
			case 0:
				temp = new MetaAchievement(nomAchievement[i], false, 8, 33);
				achievements.put(indiceAchievement[i], temp);
				break;
			case 1:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], false, 1, 24, temp));
				break;
			case 2:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], true, 5, 24, temp));
				break;
			case 3:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], false, 10, 24, temp));
				break;
			case 4:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], false, 1, 24, temp));
				break;
			case 5:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], true, 3, 24, temp));
				break;
			case 6:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], false, 1, 24, temp));
				break;
			case 7:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], true, 4, 24, temp));
				break;
			case 8:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], false, 2, 24, temp));
				break;
			case 9:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], true, 1, 24, temp));
				break;
			case 10:
				achievements.put(indiceAchievement[i], new Achievement(
						nomAchievement[i], true, 1, 24, temp));
				break;
			}
		}

		listObjets = new Vector<Objet>();
	}

	public Joueur(String nom, int viemax, int attaque, int defense,
			HashMap<String, Quete> quetes2,
			HashMap<String, MetaAchievement> achievements2,
			Vector<Objet> listObjets2, int vie, String imageJoueur,
			Equipement arme, Equipement armure, Equipement accessoire, int sc) {
		this(nom, viemax, attaque, defense);
		Joueur.quetes = quetes2;
		Joueur.achievements = achievements2;
		this.listObjets = listObjets2;
		this.setVie(vie);
		setArme(arme);
		setArmure(armure);
		setAccessoire(accessoire);
		changerImg(imageJoueur);
		score = sc;

	}

	public void changerImg(String nom) {
		walkSheet = new Texture(Gdx.files.internal("data/" + nom + ".png"));
		int largeurSprite = walkSheet.getWidth() / FRAME_COLS;
		int hauteurSprite = walkSheet.getHeight() / FRAME_ROWS;
		framesHero = TextureRegion.split(walkSheet, largeurSprite,
				hauteurSprite);

		walkAnimation_bas = new Animation(DUREEFRAME, framesHero[0]);
		walkAnimation_droite = new Animation(DUREEFRAME, framesHero[1]);
		walkAnimation_gauche = new Animation(DUREEFRAME, framesHero[2]);
		walkAnimation_haut = new Animation(DUREEFRAME, framesHero[3]);
		currentFrame = walkAnimation_bas.getKeyFrame(stateTime);
		imageCourante = nom;
	}

	public TextureRegion getAnimationTexture(float deltaTime) {
		stateTime += deltaTime;
		switch (dir) {
		case DIR_HAUT:
			currentFrame = walkAnimation_haut.getKeyFrame(stateTime, true);
			break;
		case DIR_DROITE:
			currentFrame = walkAnimation_droite.getKeyFrame(stateTime, true);
			break;
		case DIR_BAS:
			currentFrame = walkAnimation_bas.getKeyFrame(stateTime, true);
			break;
		case DIR_GAUCHE:
			currentFrame = walkAnimation_gauche.getKeyFrame(stateTime, true);
			break;
		}
		return currentFrame;
	}

	public void setFrameStat(int d) {
		switch (d) {
		case DIR_HAUT:
			currentFrame = framesHero[3][0];
			break;
		case DIR_DROITE:
			currentFrame = framesHero[1][0];
			break;
		case DIR_BAS:
			currentFrame = framesHero[0][0];
			break;
		case DIR_GAUCHE:
			currentFrame = framesHero[2][0];
			break;
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}

	public void move() {
		if (nextdir == DIR_GAUCHE) {
			dir = DIR_GAUCHE;
			if (!GameScreen.isBlocked(x - 1, y))
				x--;
			else
				gamescreen.isOther(x - 1, y);
		} else if (nextdir == DIR_DROITE) {
			dir = DIR_DROITE;
			if (!GameScreen.isBlocked(x + 1, y))
				x++;
			else
				gamescreen.isOther(x + 1, y);
		} else if (nextdir == DIR_HAUT) {
			dir = DIR_HAUT;
			if (!GameScreen.isBlocked(x, y + 1))
				y++;
			else
				gamescreen.isOther(x, y + 1);
		} else if (nextdir == DIR_BAS) {
			dir = DIR_BAS;
			if (!GameScreen.isBlocked(x, y - 1))
				y--;
			else
				gamescreen.isOther(x, y - 1);
		}

	}

	@Override
	public boolean keyDown(int keycode) {
		if (etatgame == GameScreen.GAME_RUNNING)
			switch (keycode) {
			case Keys.UP:
				nextdir = DIR_HAUT;
				break;
			case Keys.RIGHT:
				nextdir = DIR_DROITE;
				break;
			case Keys.DOWN:
				nextdir = DIR_BAS;
				break;
			case Keys.LEFT:
				nextdir = DIR_GAUCHE;
				break;
			case Keys.MENU:
				//
				gamescreen.getGame().setScreen(
						new PauseScreen(gamescreen.getGame(), gamescreen));
				break;
			case Keys.A:
				//
				gamescreen.getGame().setScreen(
						new PauseScreen(gamescreen.getGame(), gamescreen));
				break;
			}
		else if (etatgame == GameScreen.GAME_DISCUSSION)
			switch (keycode) {
			case Keys.NUMPAD_1:
				gamescreen.setIdReponse(0);
				break;
			case Keys.NUMPAD_2:
				gamescreen.setIdReponse(1);
				break;
			case Keys.NUMPAD_3:
				gamescreen.setIdReponse(2);
				break;
			}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		nextdir = DIR_DEFAULT;
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		int h = Gdx.graphics.getHeight();
		int w = Gdx.graphics.getWidth();

		gamescreen.touchpos.x = screenX;
		gamescreen.touchpos.y = Gdx.graphics.getHeight() - screenY;

		if (etatgame == GameScreen.GAME_RUNNING) {
			double xd = (double) screenX / (double) w; // position en float en x
			double yd = (double) screenY / (double) h; // position en float en y
			boolean bg = yd > xd; // bas gauche
			boolean hg = yd < 1 - xd; // haut gauche
			if (screenY < h * 0.1f && screenX > w * 0.93f) {
				gamescreen.getGame().setScreen(
						new PauseScreen(gamescreen.getGame(), gamescreen));
			} else if (screenY < h * 0.1f && screenX < w * 0.1f) {
				gamescreen.getGame().setScreen(
						new InventaireScreen(gamescreen.getGame(), gamescreen,
								this));
			} else if (bg && hg) {
				nextdir = DIR_GAUCHE;
			} else if (!bg && !hg) {
				nextdir = DIR_DROITE;
			} else if (!bg && hg) {
				nextdir = DIR_HAUT;
			} else if (bg && !hg) {
				nextdir = DIR_BAS;

			}
		} else if (etatgame == GameScreen.GAME_DISCUSSION) {
			if (gamescreen.touchpos.overlaps(gamescreen.choixDial_1.pos)) {
				gamescreen.setIdReponse(0);
			} else if (gamescreen.touchpos.overlaps(gamescreen.choixDial_2.pos)) {
				gamescreen.setIdReponse(1);
			} else if (gamescreen.touchpos.overlaps(gamescreen.choixDial_3.pos)) {
				gamescreen.setIdReponse(2);
			}
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		nextdir = DIR_DEFAULT;
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		int h = Gdx.graphics.getHeight();
		int w = Gdx.graphics.getWidth();
		double xd = (double) screenX / (double) w; // position en float en x
		double yd = (double) screenY / (double) h; // position en float en y
		boolean bg = yd > xd; // bas gauche
		boolean hg = yd < 1 - xd; // haut gauche
		if (System.currentTimeMillis() > debut + 1000) {
			if (bg && hg) {
				nextdir = DIR_GAUCHE;
			} else if (!bg && !hg) {
				nextdir = DIR_DROITE;
			} else if (!bg && hg) {
				nextdir = DIR_HAUT;
			} else if (bg && !hg) {
				nextdir = DIR_BAS;

			}
		}
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public int getNextDir() {
		return nextdir;
	}

	public void setNextdir(int d) {
		nextdir = d;
	}

	public void setGame(GameScreen g) {
		gamescreen = g;

	}

	public static boolean incrementeSucces(String idSucces) {
		return achievements.get(idSucces).incrementeCompteur();
	}

	public static String getNomSucces(String idSucces) {
		return achievements.get(idSucces).getNom();
	}

	public int getAvancement(String indiceQ) {
		return quetes.get(indiceQ).getAvancement();
	}

	public boolean incrementeQuete(String indiceQuete) {
		return quetes.get(indiceQuete).incrementeAvancement(indiceQuete);
	}

	@Override
	public String getImage() {
		return "data/images/" + imageCourante + "face.png";
	}

	public Vector<Objet> getListeObjets() {
		return listObjets;
	}

	public boolean addObjet(Objet obj) {
		if (listObjets.size() < MAX_OBJETS) {
			listObjets.add(obj);
			return true;
		}
		return false;
	}

	public boolean inventairePleinne() {
		return listObjets.size() >= MAX_OBJETS;
	}

	public HashMap<String, Quete> getQuetes() {
		return quetes;
	}

	public HashMap<String, MetaAchievement> getAchievements() {
		return achievements;
	}

	public Vector<Objet> getListObjets() {
		return listObjets;
	}

	public String getImageCourante() {
		return imageCourante;
	}

	public void setDebut(long d) {
		debut = d;

	}

	public Quete getQuete(String indiceQuete2) {
		return quetes.get(indiceQuete2);
	}

	public MetaAchievement getAchievement(String indiceS) {
		return achievements.get(indiceS);
	}

}
