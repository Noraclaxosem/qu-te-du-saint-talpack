package com.me.mygdxgame.entite;

import java.io.Serializable;

public abstract class Objet extends Entite implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom, image, description;

	public Objet(String nom, String image, String description) {
		super();
		this.nom = nom;
		this.image = image;
		this.description = description;
	}

	public String getNom() {
		return nom;
	}

	public String getImage() {
		return image;
	}

	public String getDescription() {
		return description;
	}
	
	public abstract void utiliser(Etre e);
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Objet other = (Objet) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

}
