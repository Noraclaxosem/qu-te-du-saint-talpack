package com.me.mygdxgame.entite;

import java.io.Serializable;

public class Effet implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int effetVie, effetAttaque,effetDefense;

	public Effet(int effetVie, int effetAttaque, int effetDefense) {
		super();
		this.effetVie = effetVie;
		this.effetAttaque = effetAttaque;
		this.effetDefense = effetDefense;
	}

	public int getEffetVie() {
		return effetVie;
	}

	public int getEffetAttaque() {
		return effetAttaque;
	}

	public int getEffetDefense() {
		return effetDefense;
	}
	
}
