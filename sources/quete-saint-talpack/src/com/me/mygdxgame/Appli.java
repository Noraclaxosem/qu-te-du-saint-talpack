package com.me.mygdxgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.mygdxgame.screen.MainMenuScreen;


public class Appli extends Game {

    public SpriteBatch batch;
    public BitmapFont font;
	private MidiPlayer midiPlayer;

//    public Appli(MidiPlayer midiPlayer) {
//    	this.midiPlayer = midiPlayer;
//	}

	public Appli() {
	}

	public void create() {
            batch = new SpriteBatch();
            //Use LibGDX's default Arial font.
            font = new BitmapFont();
            this.setScreen(new MainMenuScreen(this/*, midiPlayer*/));
    }

    public void render() {
            super.render(); //important!
    }
    
    public void dispose() {
            batch.dispose();
            font.dispose();
            super.dispose();
    }
    
}
