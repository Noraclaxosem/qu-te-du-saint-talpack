package com.me.mygdxgame.dialogue;

import java.io.Serializable;

public class Phrase implements Serializable {

	private static final long serialVersionUID = 1L;
	String texte;
	int index;
	int action;

	public Phrase(String texte, int index) {
		this.texte = texte;
		this.index = index;
		this.action = 0;
	}

	public Phrase(String texte, int index, int action) {
		this.texte = texte;
		this.index = index;
		this.action = action;
	}

	public String getTexte() {
		return texte;
	}

	public int getIndex() {
		return index;
	}
	
	public int getAction() {
		return action;
	}

	@Override
	public String toString() {
		return "Phrase [texte=" + texte + ", index=" + index + ", action="
				+ action + "]";
	}
}
