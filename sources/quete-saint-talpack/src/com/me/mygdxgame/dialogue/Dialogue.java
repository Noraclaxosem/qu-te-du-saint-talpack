package com.me.mygdxgame.dialogue;

import java.io.Serializable;
import java.util.ArrayList;

public class Dialogue implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<DialogueOption> listeOptions = new ArrayList<DialogueOption>();
	private String indexQuete;
	private int etatQuete;

	public Dialogue(ArrayList<DialogueOption> listeOptions, String indexQuete) {
		this.listeOptions = listeOptions;
		this.indexQuete = indexQuete;
		this.etatQuete = -1;
	}
	
	public Dialogue(ArrayList<DialogueOption> listeOptions, String indexQuete, int etatQuete) {
		this.listeOptions = listeOptions;
		this.indexQuete = indexQuete;
		this.etatQuete = etatQuete;
	}
	public ArrayList<DialogueOption> getListeOptions() {
		return listeOptions;
	}

	public String getIndexQuete() {
		return indexQuete;
	}

	public int getEtatQuete() {
		return etatQuete;
	}

	@Override
	public String toString() {
		return "Dialogue [listeOptions=" + listeOptions + ", indexQuete="
				+ indexQuete + ", etatQuete=" + etatQuete + "]";
	}

}
