package com.me.mygdxgame.dialogue;

import java.io.Serializable;
import java.util.ArrayList;

public class DialogueOption implements Serializable{

	private static final long serialVersionUID = 1L;
	private Phrase question;
	private ArrayList<Phrase> reponses = new ArrayList<Phrase>();
	
	public DialogueOption(Phrase question, ArrayList<Phrase> reponses) {
		this.question = question;
		this.reponses = reponses;
	}

	public Phrase getQuestion() {
		return question;
	}

	public ArrayList<Phrase> getReponses() {
		return reponses;
	}

	@Override
	public String toString() {
		return "DialogueOption [question=" + question + ", reponses="
				+ reponses + "]";
	}
}
