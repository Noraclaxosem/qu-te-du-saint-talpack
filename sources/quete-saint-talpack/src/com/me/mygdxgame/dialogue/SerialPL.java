package com.me.mygdxgame.dialogue;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.script.ScriptException;

public class SerialPL {

	public static void main(String[] args) throws ScriptException, IOException {
		
		Phrase ph1, ph2, ph3;
		ArrayList<Phrase> reponses = new ArrayList<Phrase>();
		DialogueOption do1, do2, do3, do4, do5, do6, do7;	
		ArrayList<DialogueOption> diagop = new ArrayList<DialogueOption>();
		ObjectOutputStream oos;
		Dialogue dia1, dia2, dia3, dia4;
		
		//*********************
		//** FOU INTRODUCTIF **
		//**      PNJ N1     **
		//*********************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Moi aussi ! Ah non.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Ami du matin, demain ! Mmh, j'ai rat� ma r�plique... Tant pis. Vous �tes ici � " +
				"Poneyland, un contr�e belle, prosp�re et bucoliquement rectangulaire." +
				"Vous mourez d'envie d'entendre parler de ce fleuve de lave subodore-je. Il a englouti la for�t qui se trouvait " +
				"inopin�ment dans ce canal naturel, seulement, ces arbres �taient ignifug�s. Truculent non ? J'en riais encore la " +
				"prochaine fois que j'y pensais. Maintenant, partez, je dois urgemment regarder d'un air vague la verte plaine.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ1 = new ArrayList<Dialogue>();	
		diagPNJ1.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ1.talpack"));
		oos.writeObject(diagPNJ1);
		oos.close();
		
		//*********************
		//** SURVEILLANT LAC **
		//**      PNJ N2     **
		//*********************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Vraiment, vos aptitudes directionnelles m'estourbissent.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Bonjour mon cher, sensuel et tendre ami. Je te vois te gausser int�rieurement de ma petitesse" +
				", mais sache qu'elle ne m'emp�che pas de manier ce gouvernail � la perfection. Je peux le tourner � droite, ainsi qu'� " +
				"gauche. Je ne veux pas politiser mon discours mais peu en sont capables.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ2 = new ArrayList<Dialogue>();	
		diagPNJ2.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ2.talpack"));
		oos.writeObject(diagPNJ2);
		oos.close();		
		
		//****************************
		//** NAIN CONCHYLICULTEUR 1 **
		//**         PNJ N3         **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Serait-ce un sismographe que vous tenez dans les mains ?", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Ce choeur d'ours en coeur me fait vibrer d'�motion, plus particuli�rement l'orange, sa voix " +
				"de baryton m'�meut.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		
		
		//** QUETE GLS2 ETAT 1 **
		
		// PHRASE 1
		ph1 = new Phrase("Comment saurais-je qui je suis ?", 2);
		ph2 = new Phrase("JE SUIS UN DRAGON !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Vous voulez voir la sage pour le mariage ? On ne se montre pas sous sa vulgaire enveloppe" +
				" humaine � la grande pr�tresse. Il faut vous montrez tel que vous �tes au fond de vous.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("*Vous commencez � vous �triper*", -1);
		ph2 = new Phrase("Oui, oui je m'ouvre � vous, je suis, je suis...", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Je vais sonder votre coeur, mais vous devez pour cela vous ouvrir � moi.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Une princesse !", 4, 2); 		//D�clenche PLS2.2
		ph2 = new Phrase("Un sac de sport !", 4, 2);	//D�clenche PLS2.2
		ph3 = new Phrase("Un ornithorynque !", 4, 2);	//D�clenche PLS2.2
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do3 = new DialogueOption(new Phrase("Je vois, un dragon, une licorne, un pied de chaise, un guerrier des tribus du nord, une princesse, " +
				"un ornithorynque", 3), reponses);
		// PHRASE 4
		ph1 = new Phrase("*Vous essayez de parler*", -3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do4 = new DialogueOption(new Phrase("MENSONGES ! Vous voulez �tre un tonneau", 4), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);

		dia2 = new Dialogue(diagop, "PLS2", 1);	//D�clench� par PLS2 1

		ArrayList<Dialogue> diagPNJ3 = new ArrayList<Dialogue>();
		diagPNJ3.add(dia1);
		diagPNJ3.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ3.talpack"));
		oos.writeObject(diagPNJ3);
		oos.close();		
				

		//****************************
		//** NAIN CONCHYLICULTEUR 2 **
		//**         PNJ N4         **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Oui", 0);
		ph2 = new Phrase("Non", 0);
		ph3 = new Phrase("Oui", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do1 = new DialogueOption(new Phrase("Mais ! Vous venez tout juste de ne pas me parler ?", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ4 = new ArrayList<Dialogue>();	
		diagPNJ4.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ4.talpack"));
		oos.writeObject(diagPNJ4);
		oos.close();
		
		//****************************
		//** NAIN CONCHYLICULTEUR 3 **
		//**         PNJ N5         **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("C'est amusant. Mais � quoi servent les touches ?", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Admirez ce piano que nous nous transmettons de cousin en arri�re-grand-p�re. Vous avez bien vu, " +
				"c'est un piano � rideaux ! Il permet de jouer du piano.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ5 = new ArrayList<Dialogue>();	
		diagPNJ5.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ5.talpack"));
		oos.writeObject(diagPNJ5);
		oos.close();
		
		//****************************
		//** NAIN CONCHYLICULTEUR 4 **
		//**         PNJ N6         **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Puis-je me tenir debout sur votre t�te ?", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Attention au passage d'un calembour sans arr�t. �loignez-vous de la bordure du quai.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ6 = new ArrayList<Dialogue>();	
		diagPNJ6.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ6.talpack"));
		oos.writeObject(diagPNJ6);
		oos.close();
		
		//***************
		//** CHASSEUSE **
		//**  PNJ N7   **
		//***************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Cette interdiction est s�rement le fruit d'anciennes interdictions dont on a oubli� la signification" +
				" mais qu'on perp�tue par respect des traditions.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Maudits soient ces vampires zombies garous. Je les lapiderais � coup de nains " +
				"conchyliculteurs si ma religion ne me l'interdisait.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLP ETAT 0 **
		
		// PHRASE 1
		ph1 = new Phrase("Puis-je vous �tre d'une quelconque aide ?", 2);
		ph2 = new Phrase("C'est fort dommage. Bon, moi je m'en vais.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Les vampires zombies garous essayent de soumettre ce monde � leur barbarie.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("N'y a-t-il pas plus simple ?", 3);
		ph2 = new Phrase("*Vous partez vous jeter dans le fleuve de lave en hurlant*", -1);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Oui, il faudrait tous les exterminer. Ils doivent �tre quelques dizaines de milliers � roder " +
				"dans la for�t et se reproduisent � une vitesse folle.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Moi aussi je suis tr�s sage ! Mais j'y vais quand m�me parce que vous �tes jolie.", 0, 1);	//D�clenche PLP.1
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Vous pourriez demander aux vieillard un peu au nord, il est sage.",3), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);

		dia2 = new Dialogue(diagop, "PLP", 0);	//Dialogue d�clench� par PLP 0
		
		//** QUETE PLP ETAT 2 **
		
		// PHRASE 1
		ph1 = new Phrase("*Vous lui exposez le plan*", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Les vampires zombies garous essayent de soumettre ce monde � leur barbarie.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Je vais lui enfoncer ses dents.", 0, 3);	//D�clenche PLP 3
		ph2 = new Phrase("Vous ne voulez pas venir ?", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("C'est tr�s compliqu� mais brillant, vraiment, vous m'�patez. Bon bah, allez-y, moi je vais vous " +
				"regarder de loin, j'ai peur de rater une des phases de ce plan complexe.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("*Vous l'encordez pour qu'elle vienne avec vous* DETRUISONS-LE !", -6, 3);	//D�clenche PLP 3
		ph2 = new Phrase("Moi aussi... Bon, du coup, j'y vais", 0, 3);	//D�clenche PLP 3
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do3 = new DialogueOption(new Phrase("Je suis un peu timide...", 3), reponses);
		

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		dia3 = new Dialogue(diagop, "PLP", 2);	//Dialogue d�clench� par PLP 2
		
		//** QUETE PLP ETAT 4 **
		
		// PHRASE 1
		ph1 = new Phrase("MA PUDEUR ! *Vous la giflez*", -5, 5);	//D�clenche PLP 5 : fin
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Oh, vous avez sauv� Poneyland, tenez, un bisou *smoutch*.", 1), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);

		dia4 = new Dialogue(diagop, "PLP", 4);	//Dialogue d�clench� par PLP 4
		
		ArrayList<Dialogue> diagPNJ7 = new ArrayList<Dialogue>();	
		diagPNJ7.add(dia1);
		diagPNJ7.add(dia2);
		diagPNJ7.add(dia3);
		diagPNJ7.add(dia4);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ7.talpack"));
		oos.writeObject(diagPNJ7);
		oos.close();
		
		//*******************************
		//** CHEF VAMPIRE ZOMBIE GAROU **
		//**          PNJ N8           **
		//*******************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Oh, j'ai cru un instant que vous �tiez cette terrible secte de vampires zombies garous, l'amusante m�prise.", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Je suis le chef de cette bande de joyeux lurons, huhuhu.", 1), reponses);
		ph1 = new Phrase("Vous essayez de m'effrayer, petit galopinou.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Huhuhu, mais comment avez-vo��GRMPRFGROGBLUMGREPFURGH", 2), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLP ETAT 3 **
		
		// PHRASE 1
		ph1 = new Phrase("Et vous allez mourir !", -2, 4);	//D�clenche PLP 4
		ph2 = new Phrase("Hihihi, pas moi !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Je suis le chef de cette bande de joyeux lurons, huhuhu.", 1), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);

		dia2 = new Dialogue(diagop, "PLP", 3);	//Dialogue d�clench� par PLP 3
		
		ArrayList<Dialogue> diagPNJ8 = new ArrayList<Dialogue>();	
		diagPNJ8.add(dia1);
		diagPNJ8.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ8.talpack"));
		oos.writeObject(diagPNJ8);
		oos.close();
		
		//**********************************
		//** SBIRE 1 VAMPIRE ZOMBIE GAROU **
		//**            PNJ N9            **
		//**********************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Je ne connais pas la capitale du Ta�djikistan, d�sol�.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Gr��opfgrmfagrm�nampf", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ9 = new ArrayList<Dialogue>();	
		diagPNJ9.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ9.talpack"));
		oos.writeObject(diagPNJ9);
		oos.close();
		
		//**********************************
		//** SBIRE 2 VAMPIRE ZOMBIE GAROU **
		//**            PNJ N10           **
		//**********************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("C'est fou tous ces points communs entre nous.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("GRfbuGNagrpfbr", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ10 = new ArrayList<Dialogue>();	
		diagPNJ10.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ10.talpack"));
		oos.writeObject(diagPNJ10);
		oos.close();
		
		//**********************************
		//** SBIRE 3 VAMPIRE ZOMBIE GAROU **
		//**            PNJ N11           **
		//**********************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Hahahaha, je la ressortirai � l'occasion, vous �tes un v�ritable boute-en-train.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Drprtgmrepfgrb", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ11 = new ArrayList<Dialogue>();	
		diagPNJ11.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ11.talpack"));
		oos.writeObject(diagPNJ11);
		oos.close();
		

		//****************
		//** GROS PONEY **
		//**  PNJ N12   **
		//****************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Alors, ce ne sont pas tes amies.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Bouhouhou, mes amies se moquent de moi, elles disent que je suis groooosse.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLS1 ETAT 0 **
		
		// PHRASE 1
		ph1 = new Phrase("Nous pouvons r�soudre ce probl�me", 2);
		ph2 = new Phrase("Il faut faire face � la r�alit�...", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Bouhouhou, mes amies se moquent de moi, elles disent que je suis groooosse.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Je vais parler � la plus moqueuse d'entre elles, la bleue.", 0, 1);	//D�clenche PLP 1
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Ah oui, et comment ?", 2), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);

		dia2 = new Dialogue(diagop, "PLS1", 0);	//Dialogue d�clench� par PLS1 0
		
		//** QUETE PLS1 ETAT 2 **
		
		// PHRASE 1
		ph1 = new Phrase("Oui.", 2);
		ph2 = new Phrase("Ahaha, non, non ! Mais non, quelle id�e stupide, ah vraiment, non, jamais.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Je vous ai entendu lui parler, alors vous voulez me manger ?", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("*Vous d�vorez une partie de la ponette*", 0, 3);	//D�clenche PLS1 3 :fin
		ph2 = new Phrase("Vous �tes charmante mais je ne suis malheureusement pas ponettivore", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Oh oui, vite, j'ai h�te, d�vorez-moi.", 2), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);

		dia3 = new Dialogue(diagop, "PLS1", 2);	//Dialogue d�clench� par PLS1 2
		
		//** QUETE PLS1 ETAT 3 **
		
		// PHRASE 1
		ph1 = new Phrase("Oui, je n'avais pas tr�s faim la derni�re fois.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Vous venez finir de me d�vorer ?", 1), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);

		dia4 = new Dialogue(diagop, "PLS1", 3);	//Dialogue d�clench� par PLS1 3
		
		ArrayList<Dialogue> diagPNJ12 = new ArrayList<Dialogue>();
		diagPNJ12.add(dia1);
		diagPNJ12.add(dia2);
		diagPNJ12.add(dia3);
		diagPNJ12.add(dia4);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ12.talpack"));
		oos.writeObject(diagPNJ12);
		oos.close();
		
		
		//************************
		//** PONETTE MOQUEUSE 1 **
		//**      PNJ N13       **
		//************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Maintenant que vous ne le dites pas...", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Ahaha, regardez cette grosse et immonde ponette ! Alors que moi, gr�ce � mon r�gime � base de " +
				"nains conchyliculteurs, je suis svelte et �lanc�e.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLS1 ETAT 1 **
		
		// PHRASE 1
		ph1 = new Phrase("Se moquer des autres, c'est mal.", 2);
		ph2 = new Phrase("Parfois, dans la vie, on ne choisit pas ce que l'on est.", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Ahaha, regardez cette grosse et immonde ponette ! Alors que moi, gr�ce � mon r�gime � base de " +
				"nains conchyliculteurs, je suis svelte et �lanc�e.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Bonne id�e !.", 0, 2);	//D�clenche PLP 2
		ph2 = new Phrase("Vos moeurs me r�pugnent, laissez-moi aller rendre mon d�jeuner.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("C'est vrai. Vous pourriez la manger pour abr�ger ses souffrances.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("C'est m�me pas vrai d'abord !", 0);
		ph2 = new Phrase("Path�tique ponette, allez courir dans le fleuve de lave.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do3 = new DialogueOption(new Phrase("J'imagine que c'est le genre d'excuses que vous trouvez pour justifier votre pr�sence en ce monde" +
				".", 3), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);

		dia2 = new Dialogue(diagop, "PLS1", 1);	//Dialogue d�clench� par PLS1 1
		
		ArrayList<Dialogue> diagPNJ13 = new ArrayList<Dialogue>();
		diagPNJ13.add(dia1);
		diagPNJ13.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ13.talpack"));
		oos.writeObject(diagPNJ13);
		oos.close();
		
		
		
		//************************
		//** PONETTE MOQUEUSE 2 **
		//**      PNJ N14       **
		//************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Je ne cautionne pas la lapidation rocheuse.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Sale ob�se, tu ne m�rites pas de vivre ! Oh bonjour monsieur ! Voulez-vous nous aider � jeter des " +
				"cailloux sur cette ponette ?", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ14 = new ArrayList<Dialogue>();	
		diagPNJ14.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ14.talpack"));
		oos.writeObject(diagPNJ14);
		oos.close();
		
				
		
		
		//************************
		//** PONETTE MOQUEUSE 3 **
		//**      PNJ N15       **
		//************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("C'est scandalique !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Regardez cette ponette, elle devrait payer deux places dans le Transponeylandal " +
				"Express. S�par�es bien entendu.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ15 = new ArrayList<Dialogue>();	
		diagPNJ15.add(dia1);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ15.talpack"));
		oos.writeObject(diagPNJ15);
		oos.close();
		
		
		
		//************************
		//** PONETTE MOQUEUSE 4 **
		//**      PNJ N16       **
		//************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Quelle trag�die.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Figurez-vous qu'un jour, j'ai pris du poids. Heureusement, je me suis aper�u plus tard " +
				"que ce n'�tait pas moi.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLS1 ETAT 3 **
		
		// PHRASE 1
		ph1 = new Phrase("Je pourrais vous chevaucher.", 2);
		ph2 = new Phrase("Vous pourriez faire partie de ma bande trop coule ! Par exemple, dedans, il y a moi.", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Vous nous avez d�barrass� de cette ponette r�pugnante, vous avez sauv� l'honneur " +
				"ponnien ! Comment vous remercier ?", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Je n'ai rien dit", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Hmm, comment �a ?", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Mais c'est d�goutant !", 0);
		ph2 = new Phrase("En route mauvaise troupe !", -6, 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do3 = new DialogueOption(new Phrase("Oh oui, prenez-moi avec vous !", 3), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		
		dia2 = new Dialogue(diagop, "PLS1", 3);	//Dialogue d�clench� par PLS1 3
		
		ArrayList<Dialogue> diagPNJ16 = new ArrayList<Dialogue>();	
		diagPNJ16.add(dia1);
		diagPNJ16.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ16.talpack"));
		oos.writeObject(diagPNJ16);
		oos.close();
		
		
		//***********************************
		//** VAMPIRE ZOMBIE GAROU � MARIER **
		//**           PNJ N17             **
		//***********************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("*Vous taisez vos opinions sur le sujet*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Quand est-ce que le mariage pour tous sera-t-il accept� par nos pairs ? J'aime Lycella et elle" +
				" m'aime.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLS2 ETAT 0 **
		
		// PHRASE 1
		ph1 = new Phrase("Oh oui, un petit couple de plus dans ce monde serait f��rique.", 2);
		ph2 = new Phrase("Je m'y refuse, c'est amoral.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Quand est-ce que le mariage pour tous sera-t-il accept� par nos pairs ? J'aime Lycella et elle" +
				" m'aime, peut-�tre pourriez-vous convaincre la sage de nous marier.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Les nains conchyliculteurs sont mes amis, je vais de ce pas lui en parler.", 0, 1);	//D�clenche PLS2 1
		ph2 = new Phrase("Un nain conchyliculteur ? Ces choses sont vivantes ?", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Merci mon brave, huhu, il faut aller convaincre la sorci�re mais en y allant tout d�guenill� comme " +
				"vous l'�tes, elle n'acceptera pas de vous parler. Le nain conchyliculteur au-dessus du lac saura sans doute vous aider." +
				"", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Le votre peut-�tre.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Goujat ! Ce sont nos fr�res !", 3), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);

		dia2 = new Dialogue(diagop, "PLS2", 0);	//Dialogue d�clench� par PLS2 0
		
		//** QUETE PLS2 ETAT 4 **
		
		// PHRASE 1
		ph1 = new Phrase("Hala, oui.", 0);
		ph2 = new Phrase("Hala, oui.", 2);
		ph3 = new Phrase("Hala, oui.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do1 = new DialogueOption(new Phrase("Nous sommes mari�s et heureux, quelle f��rie.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Ils autorisent les mariages � trois par ici ?", 3);
		ph2 = new Phrase("Finit bien bien qui tout est.", 0);
		ph3 = new Phrase("Voudriez-vous vous joindre � moi ?", 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do2 = new DialogueOption(new Phrase("Voil� voil�.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Voulez-vous que nous nous mariions � 3 ?", 4);
		ph2 = new Phrase("Moi aussi, c'est dingue.", 0);
		ph3 = new Phrase("*Vous insultez le Vampire Zombie Garou de tabouret*", 6);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do3 = new DialogueOption(new Phrase("Je ne comprends pas tout...", 3), reponses);
		// PHRASE 4
		ph1 = new Phrase("Bon bah bien. Tant pis.", 0);
		ph2 = new Phrase("Allez quoi, soyez sympa.", 4);
		ph3 = new Phrase("*Vous �ructez violemment*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do4 = new DialogueOption(new Phrase("Non.", 4), reponses);
		// PHRASE 5
		ph1 = new Phrase("Bon bah bien. Tant pis.", 0);
		ph2 = new Phrase("Allez quoi, soyez sympa.", 4);
		ph3 = new Phrase("*Vous �ructez violemment*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do5 = new DialogueOption(new Phrase("Non.", 5), reponses);
		// PHRASE 6
		ph1 = new Phrase("Bon bah bien. Tant pis.", 0);
		ph2 = new Phrase("Je refuse !", 0);
		ph3 = new Phrase("*Vous �ructez violemment*", -6, 5);	//D�clenche PLS2 5
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do6 = new DialogueOption(new Phrase("Vous m'avez convaincu, je viens avec vous.", 6), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);
		diagop.add(do5);
		diagop.add(do6);

		dia3 = new Dialogue(diagop, "PLS2", 4);	//Dialogue d�clench� par PLS2 4
		
		ArrayList<Dialogue> diagPNJ17 = new ArrayList<Dialogue>();	
		diagPNJ17.add(dia1);
		diagPNJ17.add(dia2);
		diagPNJ17.add(dia3);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ17.talpack"));
		oos.writeObject(diagPNJ17);
		oos.close();
						
		
		
		//**********************
		//** PONETTE � MARIER **
		//**     PNJ N18      **
		//**********************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Vous formez un beau couple.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Hihihi, mon homme, ce vampire garou zombie, me comble de bonheur.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLS2 ETAT 3 **
		
		// PHRASE 1
		ph1 = new Phrase("Vous �tes d�sormais mari�s !", 2);
		ph2 = new Phrase("Vous �tes d�sormais avari�s ! Un peu.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Hihihi, mon homme, ce vampire garou zombie, me comble de bonheur.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Quel doux sentiment d'avoir servi son prochain.", 0, 4);	//D�clenche PLS2 4 : fin
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Oh ouais, trop coule ! Merci, je pleure de joie.", 2), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);

		dia2 = new Dialogue(diagop, "PLS2", 3);	//Dialogue d�clench� par PLS2 3
		
		ArrayList<Dialogue> diagPNJ18 = new ArrayList<Dialogue>();	
		diagPNJ18.add(dia1);
		diagPNJ18.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ18.talpack"));
		oos.writeObject(diagPNJ18);
		oos.close();
								
		
		//*************
		//**   SAGE  **
		//** PNJ N19 **
		//*************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Ah bah bon.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Celui qui veut venir � moi doit se rev�tir de sa v�ritable apparence.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE GLS2 ETAT 2 **
		
		// PHRASE 1
		ph1 = new Phrase("Je souhaiterais que la ponette et le vampire zombie garou soient mari�s.", 2);
		ph2 = new Phrase("Redevenir humain !", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Voil� un beau tonneau... Que voulez-vous mon brave ?", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("C'est plut�t simple en fait...", 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Rien de plus simple. Attendez... voil�, ils sont mari�s.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("D'accord", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Non.", 3), reponses);
		// PHRASE 4
		ph1 = new Phrase("Puis-je redevenir humain maintenant ?", 5);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do4 = new DialogueOption(new Phrase("En apparence seulement. En effet, les choses semblent �tre parfois de grands talpacks " +
				"dont on se serait affubl� au m�pris de la limite de chapeaux portables que dicte le bon sens.", 4), reponses);
		// PHRASE 5
		ph1 = new Phrase("Je suis sage", 6, 3);
		ph2 = new Phrase("Je ne suis pas sage", 7, 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do5 = new DialogueOption(new Phrase("Seulement si vous �tes sage.", 4), reponses);	
		// PHRASE 6
		ph1 = new Phrase("Merci !", -4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do6 = new DialogueOption(new Phrase("Parfait, vous revoil� humain, vous pouvez annoncer la bonne nouvelle aux mari�s.", 4), reponses);
		// PHRASE 7
		ph1 = new Phrase("Gnn.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do7 = new DialogueOption(new Phrase("Parfait, restez tonneau. Vous pouvez quand m�me annoncer la bonne nouvelle aux mari�s.", 4), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);
		diagop.add(do5);
		diagop.add(do6);
		diagop.add(do7);

		dia2 = new Dialogue(diagop, "PLS2", 2);
		
		ArrayList<Dialogue> diagPNJ19 = new ArrayList<Dialogue>();	
		diagPNJ19.add(dia1);
		diagPNJ19.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ19.talpack"));
		oos.writeObject(diagPNJ19);
		oos.close();
						
		
		//*************
		//**  VIEUX  **
		//** PNJ N20 **
		//*************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Je comprends...", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("J'habite dans cet arbre, je m'y perds souvent mais il permet d'organiser de grandes r�ceptions " +
				"mondaines.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		//** QUETE PLP ETAT 1 **
		
		// PHRASE 1
		ph1 = new Phrase("Auriez-vous une id�e pour d�faire les vampires garous ?", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("J'habite dans cet arbre, je m'y perds souvent mais il permet d'organiser de grandes r�ceptions " +
				"mondaines.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Bonne id�e, je retourne voir la chasseuse pour lui exposer le plan.", 0, 2);	//D�clenche PLP 2
		ph2 = new Phrase("Moi aussi je veux m'entred�vorer !", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Vous pourriez tuer leur chef, c'est le seul vampire zombie garou sens�. Sans lui, ils " +
				"s'entred�voreront.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("*Vous bavez*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("C'est cela, allez rejoindre vos cong�n�res d�g�n�r�s dans la for�t.", 3), reponses);

		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);

		dia2 = new Dialogue(diagop, "PLP", 1);	//Dialogue d�clench� par PLP 1
		
		ArrayList<Dialogue> diagPNJ20 = new ArrayList<Dialogue>();	
		diagPNJ20.add(dia1);
		diagPNJ20.add(dia2);
		
		oos = new ObjectOutputStream(new FileOutputStream("PL/PLPNJ20.talpack"));
		oos.writeObject(diagPNJ20);
		oos.close();
	}
}
