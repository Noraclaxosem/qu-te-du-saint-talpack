package com.me.mygdxgame.dialogue;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.script.ScriptException;

public class SerialGI {
	public static void main(String[] args) throws ScriptException, IOException {
		
		Phrase ph1, ph2, ph3;
		ArrayList<Phrase> reponses = new ArrayList<Phrase>();
		DialogueOption do1, do2, do3, do4;	
		ArrayList<DialogueOption> diagop = new ArrayList<DialogueOption>();;
		FileOutputStream fos;
		ObjectOutputStream oos;
		Dialogue dia1, dia2, dia3, dia4;
		
		//*****************************************
		//** NOIR QUETE FOUETTAGE A COUP DE NAIN **
		//**              PNJ N12                **
		//*****************************************
		
		//** QUETE GIS1 ETAT 0 **
		
		// PHRASE 1
		ph1 = new Phrase("Cet homme hagard au regard bovin ?", 2);
		ph2 = new Phrase("C'EST VOUS LE GOULAG. GAUCHER ! ROUX !", 0);
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Regardez ce blanc qui essaye de fuir du goulag.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Je suis volontaire.", 3);
		ph2 = new Phrase("Mais qui n'�tes-vous pas ?", 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Oui, ce fils de chacal putride doit �tre puni.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("*Vous y courez en bavant partout*", 0, 1);	//D�clenche GIS2.1
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Allez le corriger.",3), reponses);
		// PHRASE 4
		ph1 = new Phrase("Oui.", 0);
		ph2 = new Phrase("Non.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do4 = new DialogueOption(new Phrase("Je ne vous demande pas pardon ?", 4), reponses);

		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);

		dia1 = new Dialogue(diagop, "GIS2", 0);	//Dialogue d�clench� par GIS2.0

		//** QUETE GIS1 ETAT 3 **
		
		// PHRASE 1
		ph1 = new Phrase("Oh oui alors !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Il l'a bien m�rit�.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia2 = new Dialogue(diagop, "GIS2", 3);	//Dialogue d�clench� par GIS2.3
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Mais vous allez vous dess�cher !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		
		do1 = new DialogueOption(new Phrase("Je bave d�impatience.", 1), reponses);
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		
		dia3 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ12 = new ArrayList<Dialogue>();	
		diagPNJ12.add(dia3);
		diagPNJ12.add(dia1);
		diagPNJ12.add(dia2);
		
		fos = new FileOutputStream("GI/GIPNJ12.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ12);
		oos.close();
		fos.close();
		
		//*****************************************
		//** NAIN QUETE FOUETTAGE A COUP DE NAIN **
		//**              PNJ N11                **
		//*****************************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("�a alors, un nain qui parle.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Un coquillage, deux coquillages, hmm, cela me ravit.", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		
		//** QUETE GIS1 ETAT 2 **
		
		// PHRASE 1
		ph1 = new Phrase("Pourquoi criez-vous ?", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("BONJOUR !", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Heu...", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("COMMENT ?", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("J'avoue en fr�mir toutes les nuits", 4);
		ph2 = new Phrase("Non", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do3 = new DialogueOption(new Phrase("Mais je vois au fond de vos yeux une lueur d�envie de flagellation " +
				"� coups de moi-m�me sur un am�ricain innocent.", 3), reponses);		
		// PHRASE 4
		ph1 = new Phrase("Ravi de vous servir", 0, 3);	//D�clenche GIS2.3 : fin
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do4 = new DialogueOption(new Phrase("Prenez-moi par le pied. Voil�. Puis fouettez cet homme�! AHAHAHAHA ! " +
				"A�e�! HAHA�! BLOM BAM�! A�E�! PLUS FORT�! BLOM SCHAK BLUM. Ah, " +
				"j�ai mal mais c��tait bien.", 4), reponses);		
				
			
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);
		dia2 = new Dialogue(diagop, "GIS2", 2);	//D�clench� par GIS2.2
		
		
		ArrayList<Dialogue> diagPNJ11 = new ArrayList<Dialogue>();	
		diagPNJ11.add(dia1);
		diagPNJ11.add(dia2);
		
		fos = new FileOutputStream("GI/GIPNJ11.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ11);
		oos.close();
		fos.close();
		
		//**********************************************
		//** AMERICAIN QUETE FOUETTAGE A COUP DE NAIN **
		//**              PNJ N10                     **
		//**********************************************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("J'a pas comprendre.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("J�a faim�! J�a peur�!", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIS2 ETAT 1 **
		// PHRASE 1
		ph1 = new Phrase("Calmez-vous et mangez-moi.", 2);
		ph2 = new Phrase("Alors, je devrai vous flageller � coup de nain conchyliculteur.", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("J�a faim�! J�a peur�!", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("*Vous agonisez lentement en vous faisant d�vorer...* Dommage...", -1);	//MORT
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Si vous insistez�", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Bon d�accord.", 0);
		ph2 = new Phrase("Haha�! Craignez la violence inou�e de la flagellation � coups de nain " +
				"conchyliculteur.", 0, 2); //D�clenche GIS2.2
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do3 = new DialogueOption(new Phrase("Piti�! Ne parlez pas � ce nain taciturne situ� � 2 m�tres 40 � " +
				"droite pour le convaincre de me fouetter avec lui.", 3), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);		
		dia2 = new Dialogue(diagop, "GIS2", 1);	//D�clench� par GIS2.1
		
		ArrayList<Dialogue> diagPNJ10 = new ArrayList<Dialogue>();	
		diagPNJ10.add(dia1);
		diagPNJ10.add(dia2);
		
		fos = new FileOutputStream("GI/GIPNJ10.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ10);
		oos.close();
		fos.close();
		
		//***************************
		//** SAGE QUETE PRINCIPALE **
		//**        PNJ N6         **
		//***************************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Vraiment ? Vous avez de dr�les de fr�quentations.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Les astres m�ont pr�dit un grand �v�nement.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut

		//** QUETE GIP ETAT 5 **
		// PHRASE 1
		ph1 = new Phrase("*Vous prenez sa main et l�embrassez langoureusement...* (votre squaw, pas sa main, petit d�goutant)" +
				"", -5, 6);	//D�clenche GIP.6 : fin
		ph2 = new Phrase("Insulter l�Ancien et fuir cette mascarade", -1);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Hug, �tranger. Hug, fille du chef. Nous �tre ici pr�sents " +
				"pour unir �mes que le destin a r�uni. M�incliner " +
				"devant les esprits je dois. Toi embrasser elle et vos �mes ne faire plus qu�un.", 1), reponses);	
		diagop = new ArrayList<DialogueOption>();;
		diagop.add(do1);	
		dia2 = new Dialogue(diagop, "GIP", 5);	//D�clench� par GIP.5
		
		ArrayList<Dialogue> diagPNJ6 = new ArrayList<Dialogue>();
		
		diagPNJ6.add(dia1);
		diagPNJ6.add(dia2);
		
		for (Dialogue d: diagPNJ6){
			System.out.println("Index Quete: " + d.getIndexQuete() + "       -----------------");
			System.out.println("------");
			ArrayList<DialogueOption> op = d.getListeOptions();
			for (DialogueOption dop : op){
				System.out.println("Question  : " + dop.getQuestion().getTexte());
				System.out.println("LstR�ponse: -----");
				ArrayList<Phrase> ph = dop.getReponses();
				System.out.println(ph.size());
				for (Phrase r : ph){
					System.out.println("R�ponse   : " + r.getAction() + " " + r.getIndex() + " " + r.getTexte());
					
				}
				System.out.println("          ");
			}
		}
		fos = new FileOutputStream("GI/GIPNJ6.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ6);
		oos.close();
		fos.close();
		
		//***************************
		//** CHEF QUETE PRINCIPALE **
		//**        PNJ N14        **
		//***************************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("*Vous taisez vos remarques sur sa coiffe*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Cette tribu am�ricaine est vraiment ridicule.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIP ETAT 1 **
		// PHRASE 1
		ph1 = new Phrase("Salut � grand chef indien, je viens qu�mander la main de votre fille.", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Hug, �tranger.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Je ne suis pas � la hauteur�", 3);
		ph2 = new Phrase("Le tapis form� par son scalp sera assorti � mon mobilier", 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Haha, ma fille est d�j� promise � meilleur homme que vous�!", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("*Vous partez en pleurant*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Les jupons de votre m�re conviendront mieux que ceux de ma " +
				"fille, hors de ma vue.", 3), reponses);	
		// PHRASE 4
		ph1 = new Phrase("Vous dites �a pour me faire peur d'abord !", 0, 2);	//D�clenche GIP.2
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do4 = new DialogueOption(new Phrase("Partez pendant que je r�fl�chis � l'agencement de vos " +
				"tripes sur mon tipi", 4), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);		
		dia2 = new Dialogue(diagop, "GIP", 1);	//D�clench� par GIP.1
		
		//** QUETE GIP ETAT 3 **
		// PHRASE 1
		ph1 = new Phrase("Le tabac c�est tabou, on en viendra tous � bout�!", 2);
		ph2 = new Phrase("Je ne fumerai pas ta pipe, sale vieux�!", 2);
		ph3 = new Phrase("Vous n�avez plus besoin de vous pr�occuper de votre fille, je ferai un " +
				"bon mari", 0, 2);	//D�clenche GIP.4
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do1 = new DialogueOption(new Phrase("Je t'ai sous-estim� jeune �tranger, tu n�es pas un blanc ordinaire." +
				" Fumons le calumet de la paix et ma fille sera tienne.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("*Vous mourez le cr�ne bris� sous les coups de calumet*", -1);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do2 = new DialogueOption(new Phrase("Tu es all� trop loin...", 2), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		dia3 = new Dialogue(diagop, "GIP", 3);	//D�clench� par GIP.3
		
		//** QUETE GIP ETAT 4 **
		// PHRASE 1
		ph1 = new Phrase("Ouhouhouflagedubelu.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Ne faites pas attendre ma fille.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia4 = new Dialogue(diagop, "GIP", 4);	//D�clench� par GIP.4
		
		ArrayList<Dialogue> diagPNJ14 = new ArrayList<Dialogue>();	
		diagPNJ14.add(dia1);
		diagPNJ14.add(dia2);
		diagPNJ14.add(dia3);
		diagPNJ14.add(dia4);
		
		fos = new FileOutputStream("GI/GIPNJ14.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ14);
		oos.close();
		fos.close();
		
		
		//*********************************
		//** FILLE CHEF QUETE PRINCIPALE **
		//**           PNJ N16           **
		//*********************************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Oh, je vais s�rement aller lui parler alors.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Seul mon p�re est ma�tre de mon sort.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIP ETAT 0 **
		// PHRASE 1
		ph1 = new Phrase("Votre belle silhouette m'a attir�.", 2);
		ph2 = new Phrase("Je n�ai pas � r�pondre, FEMME�!", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Hug, jeune �tranger. Quel bon vent vous am�ne ici�?", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Petite pr�tentieuse, la seule chose avec laquelle tu te maries bien, c�est un Big Mac.", 0);
		ph2 = new Phrase("Ton p�re me donnera ta main en voyant ma force, ma modestie, mon charisme " +
				"et ma sensualit�.", 0, 1); //D�clenche GIP.1
		ph3 = new Phrase("Je scalperai tous tes pr�tendants peaux rouges et ton " +
				"p�re s�agenouillera devant ma puissance.", 0, 1); //D�clenche GIP.1
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do2 = new DialogueOption(new Phrase("Merci, gentil �tranger mais vous �tes nombreux � me convoiter " +
				"et je ne suis promise qu�� un seul homme.", 2), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		dia2 = new Dialogue(diagop, "GIP", 0);	//D�clench� par GIP.0
		
		//** QUETE GIP ETAT 4 **
		// PHRASE 1
		ph1 = new Phrase("*Vous aquiescez en souriant*", 0, 5);	//D�clenche GIP.5
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Vous avez fi�re allure et vous �tes si fort (*rougit*). Ce sera un " +
				"honneur d��tre votre squaw�. Je vous attendrai chez l�Ancien qui officiera la c�r�monie de " +
				"notre mariage.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		dia3 = new Dialogue(diagop, "GIP", 4);	//D�clench� par GIP.4
		
		//** QUETE GIP ETAT 5 **
		// PHRASE 1
		ph1 = new Phrase("*Vous gloussez tel un dindon surexcit�*", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("*rougit*", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia4 = new Dialogue(diagop, "GIP", 5);	//D�clench� par GIP.5
		
		ArrayList<Dialogue> diagPNJ16 = new ArrayList<Dialogue>();	
		diagPNJ16.add(dia1);
		diagPNJ16.add(dia2);
		diagPNJ16.add(dia3);
		diagPNJ16.add(dia4);
		
		fos = new FileOutputStream("GI/GIPNJ16.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ16);
		oos.close();
		fos.close();
		
		//****************************************
		//** INDIEN PRETENDANT QUETE PRINCIPALE **
		//**               PNJ N13              **
		//****************************************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Plus bas.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Tu m'a vaincu, je m'incline devant toi.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIP ETAT 2 **
		// PHRASE 1
		ph1 = new Phrase("Tu riras moins quand ton cerveau prendra l�air.", -2, 3);	//D�clenche GIP.3
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Me d�fier, moi� Haha, ces blancs ont un sens de l�humour " +
				"sans limite.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia2 = new Dialogue(diagop, "GIP", 2);	//D�clench� par GIP.2
		
		//** QUETE GIP ETAT 0 **
		// PHRASE 1
		ph1 = new Phrase("Ah c'est dr�le, moi non plus, devenons amis.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Hug, je ne parle pas aux �trangers.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia3 = new Dialogue(diagop, "GIP", 0);	//D�clench� par GIP.0
		
		//** QUETE GIP ETAT 1 **
		// PHRASE 1
		ph1 = new Phrase("Ah c'est dr�le, moi non plus, devenons amis.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Hug, je ne parle pas aux �trangers.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia4 = new Dialogue(diagop, "GIP", 1);	//D�clench� par GIP.1
		
		ArrayList<Dialogue> diagPNJ13 = new ArrayList<Dialogue>();	
		diagPNJ13.add(dia1);
		diagPNJ13.add(dia2);
		diagPNJ13.add(dia3);
		diagPNJ13.add(dia4);
		
		fos = new FileOutputStream("GI/GIPNJ13.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ13);
		oos.close();
		fos.close();
		
		//****************************
		//** PERSONNAGE INTRODUCTIF **
		//**       PNJ N1           **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Je crois ne pas vous avoir aper�u la derni�re fois que je ne suis pas venu ici.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Hug ami du goulag, viens et admire notre oeuvre. Nous, les indiens, " +
				"sommes arriv�s en Am�rique et avons colonis� cette terre de sauvages. Les noirs, b�n�ficiaires " +
				"du commerce triangulaire, ont esclavagis� les am�ricains, ces animaux. Regarde leur peau blanche. " +
				"Je vais vomir tiens. Ah, �a fait du bien. Si tu veux mon avis, les noirs qui s'occupent du goulag " +
				"pourraient avoir besoin de ta cruaut�.", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ1 = new ArrayList<Dialogue>();	
		diagPNJ1.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ1.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ1);
		oos.close();
		fos.close();
		
		
		//****************************
		//** NAIN CONCHYLICULTEUR 1 **
		//**       PNJ N2           **
		//****************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Ils sont bab�liques.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Regardez comme mes coquillages ont bien grandi", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ2 = new ArrayList<Dialogue>();	
		diagPNJ2.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ2.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ2);
		oos.close();
		fos.close();
		
		
		//**************************
		//** INDIEN SURVEILLANT 1 **
		//**      PNJ N3          **
		//**************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Vous aimez aussi l'ob�situde.", 0);
		ph2 = new Phrase("Voil� la cause de votre ob�sement.", 0);
		ph3 = new Phrase("Mais ! Vous �tes gros !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do1 = new DialogueOption(new Phrase("Manitou Donald, c'est tout ce que j'aime.", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ3 = new ArrayList<Dialogue>();	
		diagPNJ3.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ3.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ3);
		oos.close();
		fos.close();
		
		//**************************
		//** INDIEN SURVEILLANT 2 **
		//**      PNJ N9          **
		//**************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Mais je n'ai rien dit.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Tous ces am�ricains sont anorexiques ! Comment ? On ne le donne " +
				"rien � manger ? Aucun rapport.", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ9 = new ArrayList<Dialogue>();	
		diagPNJ9.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ9.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ9);
		oos.close();
		fos.close();
		
		//**************************
		//** INDIEN SURVEILLANT 1 **
		//**      PNJ N5          **
		//**************************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Seulement les jours impairs.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Savais-tu que les autruches n'�taient pas des coquillages ?", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ5 = new ArrayList<Dialogue>();	
		diagPNJ5.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ5.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ5);
		oos.close();
		fos.close();
		
		//******************
		//** INDIEN ETANG **
		//**    PNJ N8    **
		//******************
		
		//** DEFAUT **
		
		// PHRASE 1
		ph1 = new Phrase("Vous aussi.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Quel bel �tang.", 1), reponses);
				
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//Dialogue par d�faut
		
		ArrayList<Dialogue> diagPNJ8 = new ArrayList<Dialogue>();	
		diagPNJ8.add(dia1);
		
		fos = new FileOutputStream("GI/GIPNJ8.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ8);
		oos.close();
		fos.close();
		
		//******************
		//** CONTREMAITRE **
		//**    PNJ N4    **
		//******************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Je doute singuli�rement de ses capacit�s de r�colte.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("L'anorexie, fl�au des am�ricains...", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);		
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIS1 ETAT 0 **
		// PHRASE 1
		ph1 = new Phrase("D'accord.", 0, 1);	//D�clenche GIS1.1
		ph2 = new Phrase("Meurs, contrema�tre tyrannique.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Ah, l'anorexie, fl�au des �tats-Unis ! Sais-tu que 108 millions " +
				"d'am�ricains sont en sous-poids ? Un budget colossal est allou� � ces monstruosit�s, et en tant que " +
				"responsable de ce goulag, je me dois de les prendre en compte, trouve-moi des id�es.", 1), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia2 = new Dialogue(diagop, "GIS1", 0);	//D�clench� par GIS1.0
		
		//** QUETE GIS1 ETAT 3 **
		// PHRASE 1
		ph1 = new Phrase("Vous ne vous souvenez pas de moi ? L'anorexie et tout...", 2);
		ph2 = new Phrase("Qu'entendez-vous par '�tes' ?", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("Qui �tes-vous ?", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("J'ai besoin de votre aide pour la construction d'un camp d'anorexiques", 3);
		ph2 = new Phrase("Blblblblbl", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Grumpf.", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Ouais.", 4);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Ah ouais ?", 3), reponses);	
		// PHRASE 4
		ph1 = new Phrase("*Vous le remerciez alors qu'il se mouche bruyamment*", 0, 4);	//D�clenche GIS1.4 : fin
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do4 = new DialogueOption(new Phrase("J'imagine que je n'ai pas le choix.", 4), reponses);
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		diagop.add(do4);
		dia3 = new Dialogue(diagop, "GIS1", 3);	//D�clench� par GIS2.3
		
		ArrayList<Dialogue> diagPNJ4 = new ArrayList<Dialogue>();	
		diagPNJ4.add(dia1);
		diagPNJ4.add(dia2);
		diagPNJ4.add(dia3);
		
		fos = new FileOutputStream("GI/GIPNJ4.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ4);
		oos.close();
		fos.close();
		
		//***********************
		//** INDIEN QUETE GIS2 **
		//**      PNJ N7       **
		//***********************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Beaucoup.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("N'as-tu jamais entendu parler de la non-conchyliculture ?", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIS1 ETAT 1 **
		// PHRASE 1
		ph1 = new Phrase("Auriez-vous des id�es pour aider les anorexiques ?", 2);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("Je surveille ces immondices pour le bien de l'humanit�.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Stupide analphab�te.", 0);
		ph2 = new Phrase("Les maigres", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Les anneaux quoi ?", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("J'y cours.", 0, 2);	//D�clenche GIS2.1
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do3 = new DialogueOption(new Phrase("Ah. Oui. Euh. Quoi ? Ouais. Allez donc voir le nain � c�t� de la " +
				"tribu au nord-est. Hauteur ou largeur, le combat est le m�me.", 3), reponses);			
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		dia2 = new Dialogue(diagop, "GIS1", 1);	//D�clench� par GIS1.1
		
		ArrayList<Dialogue> diagPNJ7 = new ArrayList<Dialogue>();	
		diagPNJ7.add(dia1);
		diagPNJ7.add(dia2);
		
		fos = new FileOutputStream("GI/GIPNJ7.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ7);
		oos.close();
		fos.close();
		
		
		//**********************
		//** NAIN QUETE GIS2 **
		//**     PNJ N15     **
		//**********************

		//** DEFAUT **
		// PHRASE 1
		ph1 = new Phrase("Vous me flattez.", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		do1 = new DialogueOption(new Phrase("COQUILLAGES.", 1), reponses);		
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		dia1 = new Dialogue(diagop, "DEFAUT");	//D�faut
		
		//** QUETE GIS1 ETAT 2 **
		// PHRASE 1
		ph1 = new Phrase("Moi aussi", 2);
		ph2 = new Phrase("Coquilla quoi ?", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do1 = new DialogueOption(new Phrase("COQUILLAGES.", 1), reponses);
		// PHRASE 2
		ph1 = new Phrase("Non, pas du tout, pourquoi ?", 0);
		ph2 = new Phrase("Oui, regardez ces pauvres h�res", 3);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		do2 = new DialogueOption(new Phrase("Je le savais. J'ai ou� dire que vous cherchiez une solution � " +
				"l'anorexie des am�ricains", 2), reponses);
		// PHRASE 3
		ph1 = new Phrase("Bonne id�e, je vais lui demander", 0, 3);	//D�clenche GIS1.3
		ph2 = new Phrase("Mais ! Vous �tes un nain !", 0);
		ph3 = new Phrase("Mais ! Vous n'�tes pas un nain !", 0);
		reponses = new ArrayList<Phrase>();
		reponses.add(ph1);
		reponses.add(ph2);
		reponses.add(ph3);
		do3 = new DialogueOption(new Phrase("Avec le budget allou� par l'�tat, vous pourriez convaincre le " +
				"contrema�tre de faire construire un centre pour anorexiques o� on les soignerait.", 3), reponses);			
		
		diagop = new ArrayList<DialogueOption>();
		diagop.add(do1);
		diagop.add(do2);
		diagop.add(do3);
		dia2 = new Dialogue(diagop, "GIS1", 2);	//D�clench� par GIS1.2
		
		ArrayList<Dialogue> diagPNJ15 = new ArrayList<Dialogue>();	
		diagPNJ15.add(dia1);
		diagPNJ15.add(dia2);
		
		fos = new FileOutputStream("GI/GIPNJ15.talpack");
		oos = new ObjectOutputStream(fos);
		oos.writeObject(diagPNJ15);
		oos.close();
		fos.close();
		
	}
}
