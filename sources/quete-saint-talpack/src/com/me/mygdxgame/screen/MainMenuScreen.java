package com.me.mygdxgame.screen;

import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Accessoire;
import com.me.mygdxgame.entite.Arme;
import com.me.mygdxgame.entite.Armure;
import com.me.mygdxgame.entite.Consommable;
import com.me.mygdxgame.entite.Effet;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.monde.Save;

public class MainMenuScreen implements Screen, InputProcessor {
    
	private final Appli game;

    private OrthographicCamera camera;
    
	private static final int HAUTEUR=480;
	private static final int LARGEUR=800;
	
	private ShapeRenderer r;
	private Texture fond;
	private Texture titre;
	
	private final int BOUTON_HAUTEUR=50;
	private final int BOUTON_LARGEUR=230;
	private final float INTERSPACE = 25f;
	
	private Rectangle touchpos;
	
	private ArrayList<MenuBouton> boutons;

	private boolean running;

	private float propo = 1;

    public MainMenuScreen(final Appli gam) {
    	
    	
		this.game = gam;
		camera = new OrthographicCamera();// taille du viewPort
		camera.setToOrtho(false, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()); // zome du camera
		Gdx.input.setInputProcessor(this);
		
		fond = new Texture(Gdx.files.internal("data/images/fond_menu.png"));
		titre = new Texture(Gdx.files.internal("data/images/titre.png"));
		
		r = new ShapeRenderer();
		r.setColor(Color.BLACK);
		
		touchpos = new Rectangle(0,0,2,2);
		
		boutons =  new ArrayList<MenuBouton>();
		
		boutons.add(new MenuBouton(1,"Continuer", Gdx.graphics.getWidth()*0.4f, (Gdx.graphics.getHeight()*0.6f), BOUTON_LARGEUR, BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(2, "Nouvelle partie", Gdx.graphics.getWidth()*0.4f, (Gdx.graphics.getHeight()*0.6f)-INTERSPACE, BOUTON_LARGEUR, BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(3, "Score", Gdx.graphics.getWidth()*0.4f, (Gdx.graphics.getHeight()*0.6f)-INTERSPACE, BOUTON_LARGEUR, BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(4, "Cr�dit", Gdx.graphics.getWidth()*0.4f, (Gdx.graphics.getHeight()*0.6f)-INTERSPACE, BOUTON_LARGEUR, BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(5, "Quitter", Gdx.graphics.getWidth()*0.4f, (Gdx.graphics.getHeight()*0.6f)-INTERSPACE*2, BOUTON_LARGEUR, BOUTON_HAUTEUR));
		game.font.setColor(Color.BLACK);
		running = true;
    }


	@Override
	public void render(float delta) {
		if(running){
			Gdx.gl.glClearColor(1, 1, 1, 0.3f);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			camera.update();
			
			r.setProjectionMatrix(camera.combined);
			game.batch.setProjectionMatrix(camera.combined);
			
			r.begin(ShapeType.Line);
			game.batch.begin();
			
			//dessiner le fond
			game.batch.disableBlending();
			game.batch.draw(fond,0,0,LARGEUR,HAUTEUR);
			game.batch.enableBlending();
			
			game.batch.draw(titre,LARGEUR/2 - (titre.getWidth()*propo)/2,HAUTEUR*0.75f,titre.getWidth()*propo ,titre.getHeight()*propo);
			game.font.setScale(propo);
			
			Iterator<MenuBouton> it = boutons.iterator();
			while(it.hasNext()){
				MenuBouton b = it.next();
				game.font.draw(game.batch, b.getNom(), b.pos.x+20, b.pos.y+ 38);
//				r.rect(b.pos.x, b.pos.y, b.pos.width, b.pos.height);
			}
			
			game.batch.end();
			r.end();
		}
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
		
		if(LARGEUR>=titre.getWidth()*2){
			propo = 2;
		}else{
			propo = 1;
		}
		
		Iterator<MenuBouton> it = boutons.iterator();
		int i = 0;
		while(it.hasNext()){
			MenuBouton b = it.next();
			b.pos.x = LARGEUR*0.4f;
			b.pos.y = HAUTEUR*0.6f - INTERSPACE*propo*i;
			i++;
		}
		
	}

	@Override
	public void show() {
		game.font.setColor(Color.BLACK);
		running = true;
	}

	@Override
	public void hide() {
		running = false;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		r.dispose();
		fond.dispose();
		titre.dispose();
	}


	@Override
	public boolean keyDown(int keycode) {
        if(keycode == Keys.BACK){
              Gdx.app.exit();
         }
         return false;
	}


	@Override
	public boolean keyUp(int keycode) {
		return false;
	}


	@Override
	public boolean keyTyped(char character) {
		return false;
	}


	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos.x = screenX * camera.viewportWidth/Gdx.graphics.getWidth();
		touchpos.y = (Gdx.graphics.getHeight() - screenY) * camera.viewportHeight/Gdx.graphics.getHeight();

		for(MenuBouton b: boutons){
			if(touchpos.overlaps(b.pos)){
				switch (b.getId()) {
				case 1:
					ObjectInputStream fIn;
					try { 
						fIn = new ObjectInputStream(Gdx.files.local(
								"talpack.talpack").read());
						Save save = (Save) fIn.readObject();
						GameScreen gs = save.getGameScreen(game);
						game.setScreen(gs);
						fIn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case 2://***************************
					Joueur joueur = new Joueur("hero", 100, 15, 5);
					joueur.addObjet(new Consommable("Potion PV",
							"data/images/objets/022-Potion02.png", "Potion de PV: PV + 20",
							new Effet(20, 0, 0)));
					joueur.addObjet(new Consommable("Potion PV",
							"data/images/objets/022-Potion02.png", "Potion de PV: PV + 20",
							new Effet(20, 0, 0)));
					joueur.addObjet(new Consommable("Potion PV",
							"data/images/objets/022-Potion02.png", "Potion de PV: PV + 20",
							new Effet(20, 0, 0)));
					game.setScreen(new GameScreen(game, 0, joueur));
			        dispose();
					break;
				case 3:
					break;
				case 4:
					break;
				case 5:
					//quitter
					dispose();
					game.dispose();
					break;
				default:
					break;
				}
			}
		}
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}


	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}


	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
