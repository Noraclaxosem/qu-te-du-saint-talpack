package com.me.mygdxgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class CombatBouton {
	private Rectangle pos;
	private Texture image;
	private int id;
	
	public CombatBouton(int id, float x, float y, String png){
		this.image = new Texture(Gdx.files.internal(png)); 
		pos = new Rectangle(x,y,image.getWidth(),image.getHeight());
		this.id=id;
	}
	
	public Rectangle getRectangle(){
		return pos;
	}
	
	public void dispose(){
		image.dispose();
	}
	
	public Texture getTexture(){
		return image;
	}
	
	public int getId(){
		return id;
	}
}
