package com.me.mygdxgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.me.mygdxgame.Appli;

class GameOverScreen implements Screen, InputProcessor{
	private final Appli game;
	private OrthographicCamera camera;
	private static final int HAUTEUR=480;
	private static final int LARGEUR=800;
	private Texture image;
	
	public GameOverScreen(final Appli game){
		this.game = game;
		camera = new OrthographicCamera();// taille du viewPort
		camera.setToOrtho(false, LARGEUR, HAUTEUR);
		image = new Texture("data/images/tombe.png");
		Gdx.input.setInputProcessor(this);
		game.font.setColor(Color.WHITE);
	}

	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.setScale(4);
        game.font.draw(game.batch, "GameOver", 100, HAUTEUR*0.8f);
        game.font.setScale(2);
        game.batch.draw(image, LARGEUR/2-image.getWidth()/2, HAUTEUR/2-image.getHeight(),image.getWidth()*2,image.getHeight()*2);
        game.font.draw(game.batch, "Touchez l'�cran pour retourner au Menu!", 100, HAUTEUR*0.2f);
        game.batch.end();
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
	}

	@Override
	public void show() {}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
     	game.setScreen(new MainMenuScreen(game));
    	dispose();
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
