package com.me.mygdxgame.screen;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.monde.Save;

public class PauseScreen implements Screen, InputProcessor {
	private final Appli game;
	private OrthographicCamera camera;
	private GameScreen scr;
	private ShapeRenderer r;
	private Texture fond;

	private static final int HAUTEUR = 480;
	private static final int LARGEUR = 800;
	private static final int BOUTON_HAUTEUR = 50;
	private static final int BOUTON_LARGEUR = 230;
	private static final float INTERSPACE = 70f;

	private Rectangle touchpos;

	private ArrayList<MenuBouton> boutons;
	
	public PauseScreen(final Appli game, GameScreen scr) {
		this.game = game;
		this.scr = scr;
		camera = new OrthographicCamera();// taille du viewPort
		camera.setToOrtho(false, LARGEUR,HAUTEUR); // zome du camera
		Gdx.input.setInputProcessor(this);

		fond = new Texture(Gdx.files.internal("data/images/fond_menu.png"));

		r = new ShapeRenderer();
		r.setColor(Color.BLACK);

		touchpos = new Rectangle(0, 0, 2, 2);

		boutons = new ArrayList<MenuBouton>();

		boutons.add(new MenuBouton(1, "Continuer",LARGEUR * 0.4f,
				(HAUTEUR * 0.7f), BOUTON_LARGEUR,
				BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(2, "Sauvegarder",
				LARGEUR * 0.4f,
				(HAUTEUR * 0.7f) - INTERSPACE, BOUTON_LARGEUR,
				BOUTON_HAUTEUR));
		boutons.add(new MenuBouton(3, "Menu Principal", LARGEUR * 0.4f, (HAUTEUR * 0.7f)
				- INTERSPACE * 2, BOUTON_LARGEUR, BOUTON_HAUTEUR));
		game.font.setColor(Color.BLACK);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 0.3f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();

		r.setProjectionMatrix(camera.combined);
		game.batch.setProjectionMatrix(camera.combined);
		r.begin(ShapeType.Line);
		game.batch.begin();

		// dessiner le fond
		game.batch.disableBlending();
		game.batch.draw(fond, 0, 0, LARGEUR, HAUTEUR);
		game.batch.enableBlending();

		game.font.setScale(3);
		game.font.draw(game.batch, "Pause", LARGEUR * 0.05f, HAUTEUR * 0.95f);
		game.font.setScale(2);
		game.font.draw(game.batch, "Score: "+ String.valueOf(Joueur.score), LARGEUR * 0.45f, HAUTEUR*0.9f);
		
		Iterator<MenuBouton> it = boutons.iterator();
		while (it.hasNext()) {
			MenuBouton b = it.next();
			game.font.draw(game.batch, b.getNom(), b.pos.x + 20, b.pos.y + 38);
			r.rect(b.pos.x, b.pos.y, b.pos.width, b.pos.height);
		}
		game.batch.end();
		r.end();
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
//		Iterator<MenuBouton> it = boutons.iterator();
//		int i = 0;
//		while (it.hasNext()) {
//			MenuBouton b = it.next();
//			b.pos.x = LARGEUR * 0.4f;
//			b.pos.y = HAUTEUR * 0.8f - INTERSPACE * i;
//			i++;
//		}
	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		r.dispose();
		fond.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos.x = screenX * camera.viewportWidth / Gdx.graphics.getWidth();
		touchpos.y = (Gdx.graphics.getHeight() - screenY)
				* camera.viewportHeight / Gdx.graphics.getHeight();

		for (MenuBouton b : boutons) {
			if (touchpos.overlaps(b.pos)) {
				switch (b.getId()) {
				case 1:
					game.setScreen(scr);
					dispose();
					break;
				case 2:// save
					Save save = new Save(scr);
					ObjectOutputStream fout;
					try {
						fout = new ObjectOutputStream(Gdx.files.local(
								"talpack.talpack").write(false));
						fout.writeObject(save);
						fout.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					break;
				case 3:
					game.setScreen(new MainMenuScreen(game));
					scr.dispose();
					dispose();
					break;
				default:
					break;
				}
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
