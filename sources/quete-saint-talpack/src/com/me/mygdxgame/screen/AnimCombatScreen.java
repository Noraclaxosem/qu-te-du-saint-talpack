package com.me.mygdxgame.screen;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Etre;
import com.me.mygdxgame.entite.Monstre;

public class AnimCombatScreen implements Screen {

	private static final int HAUTEUR = 480;
	private static final int LARGEUR = 800;
	private Appli game;
	private GameScreen scr;
	private OrthographicCamera camera;
	private Texture fond;
	private ShapeRenderer render;
	private float scall;
	private String image;
	private ArrayList<Etre> listHeros;
	private ArrayList<Monstre> listeCombat;
	private Sprite spCombat;
	private long debutAnim;

	public AnimCombatScreen(Appli game2, GameScreen gameScreen, String image,
			ArrayList<Etre> heros, ArrayList<Monstre> listeCombat) {
		this.game = game2;
		this.scr = gameScreen;
		this.image = image;
		this.listHeros = heros;
		this.listeCombat = listeCombat;
		camera = new OrthographicCamera();// taille du viewPort
		camera.setToOrtho(false, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()); // zome du camera

		fond = new Texture(Gdx.files.internal(image));
		render = new ShapeRenderer();
		render.setColor(Color.BLACK);
		scall = 20;
		spCombat = new Sprite(new Texture(
				Gdx.files.internal("data/images/animCombat.png")));
		spCombat.setScale(5f);
		spCombat.setPosition(camera.position.x - 150, camera.position.y);
		spCombat.setOrigin(128, 32);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 0.3f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		render.setProjectionMatrix(camera.combined);
		game.batch.setProjectionMatrix(camera.combined);

		render.begin(ShapeType.Line);
		game.batch.begin();

		// dessiner le fond
		game.batch.disableBlending();
		game.batch.draw(fond, 0, 0, LARGEUR, HAUTEUR);
		game.batch.enableBlending();

		spCombat.draw(game.batch);
		//
		// game.font.setColor(Color.BLACK);
		// game.font.setScale(scall);
		// game.font.draw(game.batch, "COMBAT", LARGEUR * 0.05f, HAUTEUR *
		// 0.95f);
		//
		game.batch.end();
		render.end();
		//
		// scall -= 0.5f;
		// if (scall < 3) {
		// game.font.scale(1.3f);
		// game.setScreen(new CombatScreen(game, scr, image, listHeros,
		// listeCombat));
		// dispose();
		// }
		if (spCombat.getRotation() > 360 * 2) {
			if (System.currentTimeMillis() > debutAnim + 700) {
				game.font.scale(1.3f);
				game.setScreen(new CombatScreen(game, scr, image, listHeros,
						listeCombat));
				dispose();
			}
		} else {
			spCombat.rotate(20);
			spCombat.scale(-0.07f);
			debutAnim = System.currentTimeMillis();
		}
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR);

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

}
