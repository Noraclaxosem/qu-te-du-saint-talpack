package com.me.mygdxgame.screen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Combattant;
import com.me.mygdxgame.entite.Consommable;
import com.me.mygdxgame.entite.Etre;
import com.me.mygdxgame.entite.InventaireObjet;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.entite.Monstre;
import com.me.mygdxgame.entite.Objet;

public class CombatScreen implements Screen, InputProcessor {
	private final Appli game;
	private OrthographicCamera camera;

	private static final int MAX_BARRE_VIE = 100; // des personnages
	private static final long TEMPS_AI_ATTAQUE = 500; // milliseconde
	private static final int HAUTEUR=480;
	private static final int LARGEUR=800;
	private static final float ZOOM=2.5f;
	private static final int CASE_TAILLE=60;
	private static final int NB_CASE_LIGNE=3;
	private static final int NB_CASE_COLONNE=4;

	private ShapeRenderer r;
	private ShapeRenderer r2;
	private ShapeRenderer r3;
	private ShapeRenderer r4;
	private ShapeRenderer selected;
	private SpriteBatch batch2;
	private BitmapFont font2;

	private Texture background;
	private Texture pointeur;
	private Texture tombe;

	private Rectangle touchpos;

	private Combattant selectedCombattant;
	private Combattant selectedTarget;
	private int selectedAttaque;
	private CombatBouton selectedAction;

	private boolean aiPhase = false;
	private boolean targetSelected = false;
	private boolean actionSelected = false;
	private boolean finAnimCombattant = true;
	private boolean finAnimTarget = true;
	private boolean nextCombat = false;

	private boolean finCombat = false;
	private boolean finJeu=false;
	private boolean isHide =false;
	private boolean item =false;
	private boolean animating = false;

	private int cptAnimCombattant = 0;
	private int cptAnimTarget = 0;
	private int cptTextDegat = 0;
	private int degat=0;
	private int cptTour=0;
	private long currentTime;
	
	private ArrayList<Combattant> monstres;
	private ArrayList<Combattant> personnages;
	private ArrayList<Etre> listHeros;

	private ArrayList<CombatBouton> actions;
	private ArrayList<Combattant> combattants;
	private Iterator<Combattant> ordre;
	
	private CombatBouton attaque2;
	private CombatBouton attaque1;
	private CombatBouton attaque3;
	private CombatBouton attaque22;
	private CombatBouton attaque32;
	private CombatBouton action4;
	private GameScreen scr;
	
	private MenuBouton m;
	private MenuBouton btnUtiliser;
	
	private ArrayList<Rectangle> cases;
	private Vector<InventaireObjet> listObj;
	private InventaireObjet armeHero;
	private Joueur joueur;
	private Vector<Objet> listObjetJoueur;
	private InventaireObjet selectedObjet;

	public CombatScreen(final Appli gam, GameScreen scr, String image, ArrayList<Etre> listHeros, ArrayList<Monstre> listM) {
		game = gam;
		
		joueur = scr.joueur;
		batch2 = new SpriteBatch();
		font2 = new BitmapFont();
		background = new Texture(Gdx.files.internal(image));
		
		init();
		
		this.scr = scr;

		this.listHeros = listHeros;//reference
		
		ArrayList<Monstre> listMonstres = new ArrayList<Monstre>(listM);
		monstres = new ArrayList<Combattant>();
		
		for (int i = 0; i < listMonstres.size(); i++) {
			monstres.add(new Combattant(listMonstres.get(i), getMonstresPosX(i), getCombattantPosY(i)));
		}
		personnages = new ArrayList<Combattant>();
		for (int i = 0; i < listHeros.size(); i++) {
			personnages.add(new Combattant(listHeros.get(i),getHerosPosX(i), getCombattantPosY(i)));
		}
		
		for(Combattant c :personnages){
			if(c.isPayer()){
				if(c.getEtre().getArme()!=null)
					armeHero = new InventaireObjet(c.getEtre().getArme(), 0, 0);
				break;
			}
		}
		
		combattants = new ArrayList<Combattant>();
		combattants.addAll(personnages);
		combattants.addAll(monstres);

		ordre = combattants.iterator();
		nextCombattant();
	}
	
	private void init(){
		camera = new OrthographicCamera();// taille du viewPort
		pointeur = new Texture(Gdx.files.internal("data/images/pointeur.png"));
		tombe = new Texture(Gdx.files.internal("data/images/tombe.png"));
		initAttaqueBouton();
		
		r = new ShapeRenderer();
		r2 = new ShapeRenderer();
		r3 = new ShapeRenderer();
		r4 = new ShapeRenderer();
		selected = new ShapeRenderer();
		selected.setColor(Color.CYAN);
		r2.setColor(Color.BLACK);
		r.setColor(Color.CYAN);
		touchpos = new Rectangle(0, 0, 2, 2);
		
		Gdx.input.setInputProcessor(this);
		
		game.font.setScale(1.3f);
		font2.setScale(1.3f);
		font2.setColor(Color.BLACK);
		
		//init la menu item
		cases = new ArrayList<Rectangle>();
		
		for(int i = 0; i<NB_CASE_LIGNE;i++){
			for(int j =0;j<NB_CASE_COLONNE;j++){
				cases.add(new Rectangle(LARGEUR*0.55f+(CASE_TAILLE+5)*j,HAUTEUR*0.5f-(CASE_TAILLE+5)*i,CASE_TAILLE,CASE_TAILLE));
			}
		}
		
		listObj = new Vector<InventaireObjet>();
		listObjetJoueur = joueur.getListeObjets();
		
		Iterator<Objet> it = listObjetJoueur.iterator();
		int i=0,j=0;
		while(it.hasNext()){
			Objet obj = it.next();
			if(obj.getClass() == Consommable.class){
				listObj.add(new InventaireObjet(obj, LARGEUR*0.55f+(CASE_TAILLE+5)*j, HAUTEUR*0.5f-(CASE_TAILLE+5)*i));
				j++;
				if(j>=NB_CASE_COLONNE){
					j=0;
					i++;
				}
			}
		}
		m = new MenuBouton(1,"Item",LARGEUR/2 +30,40, LARGEUR*0.34f, HAUTEUR/2+30);
		btnUtiliser = new MenuBouton(2,"Utiliser",LARGEUR*0.79f + 3,HAUTEUR*0.17f,60,25);
	}
	
	private void initAttaqueBouton(){
		actions = new ArrayList<CombatBouton>();
		attaque1 = new CombatBouton(1, LARGEUR/2-35, HAUTEUR * 0.7f, "data/images/attaque1.png");
		attaque2 = new CombatBouton(2, LARGEUR/2-35, HAUTEUR * 0.55f, "data/images/attaque2.png");
		attaque22 = new CombatBouton(2, LARGEUR/2-35, HAUTEUR * 0.55f, "data/images/attaque2-2.png");
		attaque3 = new CombatBouton(3, LARGEUR/2-35, HAUTEUR * 0.4f, "data/images/attaque3.png");
		attaque32 =new CombatBouton(3, LARGEUR/2-35, HAUTEUR * 0.4f, "data/images/attaque3-2.png");
		action4 = new CombatBouton(4,LARGEUR/2-35,HAUTEUR * 0.25f,"data/images/action.png");
		
		
		actions.add(attaque1);
		actions.add(attaque2);
		actions.add(attaque3);
		actions.add(action4);
	}

	@Override
	public void render(float delta) {
		if(!isHide ){
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		
		game.batch.setProjectionMatrix(camera.combined);
		batch2.setProjectionMatrix(camera.combined);
		r.setProjectionMatrix(camera.combined);
		r2.setProjectionMatrix(camera.combined);
		r3.setProjectionMatrix(camera.combined);
		r4.setProjectionMatrix(camera.combined);
		selected.setProjectionMatrix(camera.combined);
		
		selected.begin(ShapeType.Line);
		game.batch.begin();
		r2.begin(ShapeType.Line);
		r.begin(ShapeType.Filled);
		
		game.batch.disableBlending();
		game.batch.draw(background, 0, 0, LARGEUR, HAUTEUR);
		game.batch.enableBlending();
//		r.rect(touchpos.x, touchpos.y, 10, 10);

		// ___________________ dessiner tous les personnages____________________
		Iterator<Combattant> iter = combattants.iterator();

		while (iter.hasNext()) {	
			Combattant pers = iter.next();
			if (!pers.isBattu()) { // le personnage n'est pas battu
				if(!finCombat&&!finJeu){	//c'est pas la fin du combat 
					r.setColor(Color.GREEN);
					r.rect(pers.getX()-MAX_BARRE_VIE/2, pers.getY()+(HAUTEUR * 0.5f), pers.getVie()+pers.getEtre().getEquipementVie(), 15); // barre de vie
					r2.rect(pers.getX()-MAX_BARRE_VIE/2, pers.getY()+(HAUTEUR * 0.5f), pers.getMaxVie()+pers.getEtre().getEquipementVie(), 15);
					r.setColor(Color.CYAN);
					if (pers == selectedCombattant) { //personnage est selectionn� pour l'attaque
						if (!finAnimCombattant&&targetSelected) {	//verfier l'etat du combattant; pers selectionne a choisi un cible d'attaque 
							if (cptAnimCombattant <= 60) {	//pour l'animation d'attaque
								if(pers.isMonstre()){
									drawCombattant(pers,cptAnimCombattant,0);
								}else{
									drawCombattant(pers,-cptAnimCombattant,0);
								}
								animating = true;
								cptAnimCombattant += 5;
							} else {	//fini l'animation d'attaque
								finAnimCombattant = true;
								degat = getDegat();
								currentTime = System.currentTimeMillis();
							}
						} else { //personnage en etat: pas encore choisir le cible d'attaque
							drawCombattant(pers,0,0);
							if(!selectedCombattant.isPayer()&&!finAnimCombattant){
								if (leTempsPasse(currentTime, TEMPS_AI_ATTAQUE)) { //si une second a passe
									IAAttaque();
								}
							}
						}
					} else if(pers == selectedTarget){ //la personnage n'est pas selectionnne pour l'attaque
						if (finAnimCombattant) { // le pers en etat: fin de l'animation d'attaque
							if(!finAnimTarget){ // si l'animation n'est pas fini
								if (cptAnimTarget <= 60) { // faire l'animation de subir une attaque
									if(pers.isMonstre()){
										drawCombattant(pers,-cptAnimTarget,0);
									}else{
										drawCombattant(pers,+cptAnimTarget,0);
									}
									game.font.draw(game.batch, String.valueOf(degat),pers.getX(), pers.getY()+ cptTextDegat);
									cptAnimTarget+=5;
									cptTextDegat += 10;
								} else { //fin de l'animation de subit une attaque
									finAnimTarget = true;
									currentTime = System.currentTimeMillis();
								}
							}else{// quand l'animation est fini
								drawCombattant(pers,0,0);
								if(leTempsPasse(currentTime,300)){ // si une second est pass�e
									nextCombat = true;
								}else{	//sinon on affiche le degat re�u
									game.font.draw(game.batch, String.valueOf(degat),pers.getX(), pers.getY()+ cptTextDegat);
								}
							
							}
						} else { // personnage en etat: l'attaquant n'a pas encore fini sa animation
							drawCombattant(pers,0,0);
						}
					}else{ // personnage n'est ni une cible ni l'attaquant 
						drawCombattant(pers,0,0);
						if(selectedCombattant.isPayer()&&actionSelected&&pers.isMonstre()){
							game.font.draw(game.batch,pers.getNom(), pers.getX()-MAX_BARRE_VIE/2, pers.getY()+(HAUTEUR * 0.48f)); //afficher le nom du monstre
						}
					}
				}else{
					drawCombattant(pers,0,0);
				}
			} else { //personnage est battu
				game.batch.draw(tombe, pers.getX(), pers.getY(), tombe.getWidth(), tombe.getHeight());
			}
		}
		
		if(selectedAction!=null&&!animating){
			selected.circle(selectedAction.getRectangle().x+32, selectedAction.getRectangle().y+32, 30f);
		}
		
		game.batch.draw(pointeur, selectedCombattant.getX(), selectedCombattant.getY() - 25);
		
		if (selectedCombattant.isPayer()&&!finAnimCombattant){// verifier le personnage est le joueur
			afficherAction(); //afficher les 3 bouton d'attaques
			r.setColor(Color.WHITE);
			game.font.setColor(Color.BLACK);
			r2.rect(LARGEUR/2-100, 0, 200, 40);
			if(!actionSelected){
				game.font.draw(game.batch, "Choisir une action", LARGEUR/2-80, 25);
			}else{
				game.font.draw(game.batch, "Choisir une cible", LARGEUR/2-80, 25);
			}
			r.setColor(Color.CYAN);
			game.font.setColor(Color.WHITE);
			
			if(item){
				r4.begin(ShapeType.Filled);
				r4.setColor(Color.WHITE);
				r4.rect(m.pos.x, m.pos.y,m.pos.width, m.pos.height);
				r4.end();
				r3.begin(ShapeType.Line);
				r3.setColor(Color.BLACK);
				for(Rectangle r:cases){
					r3.rect(r.x, r.y, r.width, r.height);
				}
				if(selectedObjet!=null){
					r3.setColor(Color.YELLOW);
					r3.rect(selectedObjet.getRectangle().x,selectedObjet.getRectangle().y, selectedObjet.getRectangle().width*1.5f, selectedObjet.getRectangle().height*1.5F);
					r3.setColor(Color.BLACK);
					r3.rect(btnUtiliser.pos.x, btnUtiliser.pos.y, btnUtiliser.pos.width, btnUtiliser.pos.height);
				}
				r3.end();
				batch2.begin();
				font2.draw(batch2, "Choisir un objet", LARGEUR/2-80, 25);
				if(selectedObjet!=null){
					font2.setScale(1);
					font2.draw(batch2, btnUtiliser.getNom(), btnUtiliser.pos.x + 5, btnUtiliser.pos.y + 20);
					font2.draw(batch2, selectedObjet.getObj().getNom(),LARGEUR*0.55f, HAUTEUR*0.2f);
					font2.draw(batch2, selectedObjet.getObj().getDescription(),LARGEUR*0.55f, HAUTEUR*0.15f);
					font2.setScale(1.3f);
				}
				for(InventaireObjet obj: listObj){
					batch2.draw(obj.getTexture(), obj.getRectangle().x, obj.getRectangle().y + 10,obj.getRectangle().width*1.5f,obj.getRectangle().height*1.5f);
				}
				batch2.end();
			}
		}
		game.batch.end();
		r.end();
		r2.end();
		selected.end();

		
		update(delta);
		}
	}
	
	private void update(float delta) {
		if(nextCombat&&!finCombat&&!finJeu){	//prochain combattant
			subitDegat();
			if(selectedTarget.getVie() + selectedTarget.getEtre().getEquipementVie()<=0){
				if(selectedTarget.isMonstre()){// si la cible est une monstre
					monstres.get(monstres.indexOf(selectedTarget)).setBattu(true);
				}else{
					personnages.get(personnages.indexOf(selectedTarget)).setBattu(true);
				}
				combattants.get(combattants.indexOf(selectedTarget)).setBattu(true);
			}
			if (hasMonstres()){// verifier si il reste encore des monstres
				if(hasPlayer()){
					nextCombattant();	// prochain combat(d'une autre personnage)
				}else{
					finJeu = true;
					currentTime = System.currentTimeMillis();
				}
			}else {
				// la fin du combat, retourner sur la map
				finCombat = true;
				currentTime = System.currentTimeMillis();
			}
		}
		
		if (finCombat) { //fin du combat
			if(leTempsPasse(currentTime, 800)){
				finDuCombat();
			    game.setScreen(scr);//test
				dispose();
			}
		}
		
		if(finJeu){
			if(leTempsPasse(currentTime, 300)){
				game.setScreen(new GameOverScreen(game));//test
				dispose();
			}
		}
	}
	
	private float getHerosPosX(int i){
		return (LARGEUR * 0.75f) + i * (LARGEUR * 0.048f);
	}
	
	private float getMonstresPosX(int i){
		return (LARGEUR * 0.25f) - i * (LARGEUR * 0.048f);
	}
	
	private float getCombattantPosY(int i){
		return HAUTEUR * 0.4f - i * LARGEUR * 0.06f;
	}
	
	private void finDuCombat() {
		//mise � jour la list des recrutants
		for(Combattant c :personnages){
			if(c.isBattu()){
				listHeros.remove(c.getEtre());
			}
			if(!c.isPayer()){
				c.setVie(c.getEtre().getMaxVie());
			}
		}
	}

	/**
	 * @pre selectedTarget no null
	 * @pre selectedCombattant no null
	 */
	private void subitDegat() {
		selectedTarget.setVie(selectedTarget.getVie()+degat);
	}

	/**
	 * @pre selectedTarget no null
	 * @pre selectedCombattant no null
	 */
	private int getDegat(){
		double prob = Math.random() + 0.5;
		int defense = selectedTarget.getDefense()+selectedTarget.getEtre().getEquipementDefense();
		int attaque = selectedCombattant.getAttaque() + selectedCombattant.getEtre().getEquipementAttaque();
		int degat = (int) (defense-(attaque*prob)*selectedAttaque);
		
		if(degat>=0){
			degat = -1;
		}
		return degat;
	}
	
	/**
	 * @pre apres game.batch.begin()
	 * @pre avant game.batch.end()
	 * @param c
	 */
	private void drawCombattant(Combattant c, float addX, float addY){
		game.batch.draw(c.getCombatImage(), c.getX()-(c.getRectangle().width/2)*ZOOM + addX, c
					.getY()+addY, c.getRectangle().width * ZOOM, 
					c.getRectangle().height * ZOOM);
		if(armeHero!=null&&c.isPayer()){
			game.batch.draw(armeHero.getTexture(), c.getX()-68 + addX, c.getY()+c.getRectangle().height*0.63f + addY,armeHero.getTexture().getWidth()*2,armeHero.getTexture().getHeight()*2);
		}
	}

	private boolean hasPlayer() {
		Iterator<Combattant> iter = personnages.iterator();
		while (iter.hasNext()) {
			Combattant c = iter.next();
			if (c.isPayer() && !c.isBattu())
				return true;
		}
		return false;
	}

	private boolean leTempsPasse(long debut, long miliseconds) {
		return (System.currentTimeMillis()>debut+miliseconds);
	}

	private void IAAttaque() {
		selectedAttaque = 1; // test
		actionSelected = true;
		if(selectedCombattant.isMonstre()){
			selectedTarget = getRamdomTarget(personnages);
//			persSelected = true;
		}else{
			selectedTarget = getRamdomTarget(monstres);
		}
		targetSelected = true;
	}

	private Combattant getRamdomTarget(ArrayList<Combattant> Targets) {
		ArrayList<Combattant> listTargets = new ArrayList<Combattant>();
		Iterator<Combattant> it = Targets.iterator();
		while (it.hasNext()) {
			Combattant c = it.next();
			if (!c.isBattu()) {
				listTargets.add(c);
			}
		}
		int randomNumMonstre = (int) ((int) listTargets.size() * Math.random());
		return listTargets.get(randomNumMonstre);
	}

	/**
	 * @pre hasMonstre()
	 */
	private void nextCombattant() {
		selectedTarget = null;
		selectedCombattant = null;
		selectedAttaque = 0;
		selectedAction = null;
		animating = false;
		targetSelected = false;
		actionSelected = false;
		finAnimCombattant = false;
		finAnimTarget = false;
		nextCombat = false;
		item = false;
		cptTextDegat = 0;
		cptAnimCombattant = 0;
		cptAnimTarget = 0;
		
		if (!ordre.hasNext()) {
			ordre = combattants.iterator();
			cptTour++;
		}
		Combattant c = ordre.next();
		while (c.isBattu()) {
			if (!ordre.hasNext()) {
				ordre = combattants.iterator();
				cptTour++;
			}
			c = ordre.next();
		}
		selectedCombattant = c;
		aiPhase = !selectedCombattant.isPayer(); // si le combattant selectionne n'est pas le joueur, passer en phase  AI
		
		if(cptTour>=3){
			actions.set(1, attaque2);
		}else{
			actions.set(1,attaque22);
		}
		if(cptTour>=5){
			actions.set(2,attaque3);
		}else{
			actions.set(2,attaque32);
		}
	}


	private boolean hasMonstres() {
		Iterator<Combattant> iterMonstre = monstres.iterator();
		while (iterMonstre.hasNext()) {
			if (!iterMonstre.next().isBattu())
				return true;
		}
		return false;
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
		
	}

	@Override
	public void show() {
		isHide=false;
		Gdx.input.setInputProcessor(this);
		game.font.setColor(Color.WHITE);
	}

	@Override
	public void hide() {
		isHide=true;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
//		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void dispose() {
		background.dispose();
		pointeur.dispose();
		r.dispose();
		r2.dispose();
		r3.dispose();
		r4.dispose();
		for(Combattant c : combattants){
			c.dispose();
		}
		attaque1.dispose();
		attaque2.dispose();
		attaque3.dispose();
		attaque22.dispose();
		attaque32.dispose();
		action4.dispose();
		
		batch2.dispose();
		
		if(armeHero!=null)
			armeHero.dispose();
	}

	public void afficherAction() {
		for (CombatBouton cb : actions) {
			game.batch.draw(cb.getTexture(), cb.getRectangle().x, cb.getRectangle().y,
					cb.getTexture().getWidth(), cb.getTexture().getHeight()); // test
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(!aiPhase){// ignorer les touches au phase de combat ai
			touchpos.x = screenX * camera.viewportWidth/Gdx.graphics.getWidth();
			touchpos.y = (Gdx.graphics.getHeight() - screenY) * camera.viewportHeight/Gdx.graphics.getHeight();
			
			if(item&&!touchpos.overlaps(m.pos)){
				item=false;
			}else{
				for(InventaireObjet obj: listObj){
					if(touchpos.overlaps(new Rectangle(obj.getRectangle().x, obj.getRectangle().y, obj.getRectangle().width*2, obj.getRectangle().height*2))){
						selectedObjet = obj;
						break;
					}
				}
				if(selectedObjet!=null){
					if(selectedObjet.estConsomable()){
						if(touchpos.overlaps(btnUtiliser.pos)){
							selectedObjet.getObj().utiliser(joueur);
							listObj.remove(selectedObjet);
							listObjetJoueur.remove(selectedObjet.getObj());
							selectedObjet = null;
							nextCombattant();
						}
					}
				}
			}
			for (CombatBouton cb : actions) {  //verifier les boutons
				if (touchpos.overlaps(new Rectangle(cb.getRectangle().x, cb
						.getRectangle().y, cb.getRectangle().width,
						cb.getRectangle().height * ZOOM))) {
					switch (cb.getId()) {
					case 1:
						selectedAction = cb;
						selectedAttaque = cb.getId();
						actionSelected = true;
						break;
					case 2:
						if(cptTour>=2){
							selectedAction = cb;
							selectedAttaque = cb.getId();
							cptTour =0;
							actionSelected = true;
						}
						break;
					case 3:
						if(cptTour>=3){
							selectedAction = cb;
							selectedAttaque = cb.getId();
							cptTour =0;
							actionSelected = true;
						}
						break;
					case 4:
						selectedAction = cb;
						item = true;
						actionSelected = true;
						break;
					default:
						break;
					}
					break;
				}
			}

			if (!targetSelected&&actionSelected) {
				for (Combattant p : monstres) {
					if (!p.isBattu()&&touchpos.overlaps(new Rectangle(p.getX()-(p.getRectangle().width/2)*ZOOM, p.getY(), p.getRectangle().width*ZOOM, p.getRectangle().height*ZOOM))) {
						selectedTarget = p;
						targetSelected = true;
						finAnimCombattant = false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
