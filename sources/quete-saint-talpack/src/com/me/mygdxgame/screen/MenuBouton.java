package com.me.mygdxgame.screen;

import com.badlogic.gdx.math.Rectangle;

public class MenuBouton {
	private String nom;
	private int id;
	public Rectangle pos;
	public MenuBouton(int id, String nom, float x, float y, float width, float height){
		this.nom = nom;
		pos = new Rectangle(x,y,width,height);
		this.id = id;
	}
	
	public String getNom(){
		return nom;
	}
	
	public int getId(){
		return id;
	}
}
