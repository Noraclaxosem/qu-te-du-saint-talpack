package com.me.mygdxgame.screen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Accessoire;
import com.me.mygdxgame.entite.Arme;
import com.me.mygdxgame.entite.Armure;
import com.me.mygdxgame.entite.Combattant;
import com.me.mygdxgame.entite.Equipement;
import com.me.mygdxgame.entite.Etre;
import com.me.mygdxgame.entite.InventaireObjet;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.entite.Objet;

public class InventaireScreen implements Screen, InputProcessor{
	private final Appli game;
	private OrthographicCamera camera;
	private Screen scr;
	private ShapeRenderer r;
	
	private static final int HAUTEUR=480;
	private static final int LARGEUR=800;
	private static final int BOUTON_HAUTEUR=40;
	private static final int BOUTON_LARGEUR=80;
	private static final int NB_LIGNE=3;
	private static final int NB_COLONNE=6;
	public static final int TAILLE_CASE = 40;
	public static final float ZOOM_OBJ = 1.5f;
	public static final float EQUIPEMENT_X = 15f;
	public float ARME_Y;
	public float ARMURE_Y;
	public float ACCESSOIRE_Y;
	private float INTERSPACE;
	private boolean estPotion = false;
	
	private float debut_X;
	
	private Rectangle touchpos;
	
	private MenuBouton boutonRetour;
	private MenuBouton boutonUtiliser;
	private MenuBouton boutonDetruire;
	
	private Combattant joueur;
	
	private Vector<InventaireObjet> listObjets;
	private Vector<Objet> listObjetsJoueur;
	private ArrayList<Rectangle> listCases;
	private float debut_Y;
	private InventaireObjet selectedObjet;
	
	private InventaireObjet arme;
	private InventaireObjet armure;
	private InventaireObjet accessoire;
	private Texture fond;
	
	public InventaireScreen(final Appli game,Screen scr, Joueur jo){
		this.game = game;
		this.scr = scr;
		camera = new OrthographicCamera();// taille du viewPort
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
		
		Gdx.input.setInputProcessor(this);
		
		r = new ShapeRenderer();
		r.setColor(Color.BLACK);
		
		INTERSPACE = 10f;
		
		debut_X = camera.viewportWidth*0.32f;
		debut_Y = camera.viewportHeight*0.7f;
		
		ARME_Y = debut_Y - 40;
		ARMURE_Y = debut_Y - 40 -(TAILLE_CASE*2 + 30);
		ACCESSOIRE_Y = debut_Y - 40 -(TAILLE_CASE*2 + 30)*2;
		
		touchpos = new Rectangle(0,0,2,2);
		boutonRetour = new MenuBouton(1,"Retour", 0, HAUTEUR-BOUTON_HAUTEUR, BOUTON_LARGEUR, BOUTON_HAUTEUR);
		boutonUtiliser = new MenuBouton(2,"Utiliser", debut_X, HAUTEUR * 0.2f, 150, 30);
		boutonDetruire = new MenuBouton(3,"Detruire", debut_X + 200, HAUTEUR * 0.2f,150, 30);
		
		this.joueur = new Combattant(jo,LARGEUR*0.17f, HAUTEUR*0.45f); 
		
		game.font.setColor(Color.BLACK);
		
		//init les equipements dans l'inventaire du joueur
		if(jo.getArme()!=null){
			arme = new InventaireObjet(jo.getArme(), EQUIPEMENT_X, ARME_Y);
		}
		if(jo.getArmure()!=null){
			armure = new InventaireObjet(jo.getArmure(), EQUIPEMENT_X, ARMURE_Y);
		}
		if(jo.getAccessoire()!=null){
			accessoire = new InventaireObjet(jo.getAccessoire(), EQUIPEMENT_X, ACCESSOIRE_Y);
		}
		
		//initialiser les cases du inventaire(Equipement)
		listCases = new ArrayList<Rectangle>();
		listCases.add(new Rectangle(EQUIPEMENT_X,ARME_Y,TAILLE_CASE,TAILLE_CASE));
		listCases.add(new Rectangle(EQUIPEMENT_X,ARMURE_Y,TAILLE_CASE,TAILLE_CASE));
		listCases.add(new Rectangle(EQUIPEMENT_X,ACCESSOIRE_Y,TAILLE_CASE,TAILLE_CASE));
		
		//initialiser les objets qui sont sur joueur
		listObjets = new Vector<InventaireObjet>();
		
		listObjetsJoueur = jo.getListeObjets();
		Iterator<Objet> it = listObjetsJoueur.iterator();
		for(int j = 0; j< NB_LIGNE;j++){
			for(int i = 0; i<NB_COLONNE;i++){
				if(it.hasNext()){
					listObjets.add(new InventaireObjet(it.next(), debut_X +(TAILLE_CASE*2+INTERSPACE)*i, debut_Y - (TAILLE_CASE*2 + 10)*j ));
				}
				//initialiser les cases du inventaire(Objets)
				listCases.add(new Rectangle(debut_X +(TAILLE_CASE*2+INTERSPACE)*i, debut_Y - (TAILLE_CASE*2 + 10)*j ,TAILLE_CASE,TAILLE_CASE));
			}
		}
		fond = new Texture(Gdx.files.internal("data/images/fond_inventaire.png"));
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		
		r.setProjectionMatrix(camera.combined);
		game.batch.setProjectionMatrix(camera.combined);
		
		r.begin(ShapeType.Line);
		game.batch.begin();
		game.batch.draw(fond, 0, 0, LARGEUR, HAUTEUR);
		//dessiner le cadre state
		r.rect(5,10,LARGEUR*0.3f, HAUTEUR*0.85f);
//		r.rect(touchpos.x,touchpos.y,touchpos.width, touchpos.height);
		
		//dessiner la texte "Inventaire" 
		game.font.setScale(3);
		game.font.draw(game.batch, "Inventaire", LARGEUR*0.2f, HAUTEUR*0.98f);
		
		//dessiner les states du joueur
		game.batch.draw(joueur.getCombatImage(), joueur.getX(), joueur.getY(),joueur.getCombatImage().getWidth()*3.5f,joueur.getCombatImage().getHeight()*3.5f); //l'image Hero
		if(arme!=null)
			game.batch.draw(arme.getTexture(),joueur.getX()-30, joueur.getY()+joueur.getCombatImage().getHeight()*3.5f*0.28f,arme.getTexture().getWidth()*2.5f,arme.getTexture().getHeight()*2.5f); //dessiner l'arme sur hero
		game.font.setScale(1);
		if(joueur.getEtre().getEquipementVie()>0){
			game.font.draw(game.batch, "PV: " + String.valueOf((joueur.getVie())+joueur.getEtre().getEquipementVie())+"/"+String.valueOf(joueur.getMaxVie())+" + "+ joueur.getEtre().getEquipementVie(), LARGEUR*0.15f, HAUTEUR*0.4f);
		}else{
			game.font.draw(game.batch, "PV: " + String.valueOf(joueur.getVie())+"/"+String.valueOf(joueur.getMaxVie()), LARGEUR*0.15f, HAUTEUR*0.4f);
		}
		if(joueur.getEtre().getEquipementAttaque()>0){
			game.font.draw(game.batch, "Attaque: " + String.valueOf(joueur.getAttaque())+" + "+joueur.getEtre().getEquipementAttaque(), LARGEUR*0.15f, HAUTEUR*0.3f);
		}else{
			game.font.draw(game.batch, "Attaque: " + String.valueOf(joueur.getAttaque()), LARGEUR*0.15f, HAUTEUR*0.3f);
		}
		if(joueur.getEtre().getEquipementDefense()>0){
			game.font.draw(game.batch, "Defense: " + String.valueOf(joueur.getDefense())+" + "+joueur.getEtre().getEquipementDefense(), LARGEUR*0.15f, HAUTEUR*0.2f);
		}else{
			game.font.draw(game.batch, "Defense: " + String.valueOf(joueur.getDefense()), LARGEUR*0.15f, HAUTEUR*0.2f);
		}
		
		//dessiner les cases d'inventaire
		for(Rectangle rec:listCases){
			r.rect(rec.x, rec.y, rec.width*2, rec.height*2);
		}
		
		//dessiner les equipements
		if(arme!= null){
			game.batch.draw(arme.getTexture(), arme.getRectangle().x+10, arme.getRectangle().y+10, arme.getRectangle().width * ZOOM_OBJ,arme.getRectangle().height*ZOOM_OBJ); //l'image Hero
		}
		if(armure!=null){
			game.batch.draw(armure.getTexture(), armure.getRectangle().x+10, armure.getRectangle().y+10, armure.getRectangle().width * ZOOM_OBJ,armure.getRectangle().height*ZOOM_OBJ); //l'image Hero
		}
		if(accessoire!=null){
			game.batch.draw(accessoire.getTexture(), accessoire.getRectangle().x+10, accessoire.getRectangle().y+10, accessoire.getRectangle().width * ZOOM_OBJ,accessoire.getRectangle().height*ZOOM_OBJ); //l'image Hero
		}
			
		//dessiner les objets
		Iterator<InventaireObjet> it = listObjets.iterator();
		while(it.hasNext()){
			InventaireObjet obj = it.next();
			game.batch.draw(obj.getTexture(), obj.getRectangle().x+10, obj.getRectangle().y+10, obj.getRectangle().width * ZOOM_OBJ,obj.getRectangle().height*ZOOM_OBJ);//afficher l'image de l'objet
		}
		
		//dessiner les boutons d'action sur l'objet
		if(selectedObjet!=null){
			game.font.draw(game.batch, selectedObjet.getObj().getNom(), debut_X+10, HAUTEUR*0.16f); //Afficher le nom de l'objet
			game.font.draw(game.batch, selectedObjet.getObj().getDescription(), debut_X+10, HAUTEUR*0.1f); //Afficher le description de l'objet
			if(estPotion||selectedObjet.estEquipement()){
				r.rect(boutonUtiliser.pos.x, boutonUtiliser.pos.y,boutonUtiliser.pos.width, boutonUtiliser.pos.height);
				game.font.draw(game.batch, boutonUtiliser.getNom(), boutonUtiliser.pos.x+10, boutonUtiliser.pos.y+25); //Afficher le bouton utiliser
				r.rect(boutonDetruire.pos.x, boutonDetruire.pos.y,boutonDetruire.pos.width, boutonDetruire.pos.height);
				game.font.draw(game.batch, boutonDetruire.getNom(), boutonDetruire.pos.x+10, boutonDetruire.pos.y+25); //Afficher le bouton detruire
			}
			r.setColor(Color.YELLOW);
			r.rect(selectedObjet.getRectangle().x, selectedObjet.getRectangle().y, selectedObjet.getRectangle().width*2, selectedObjet.getRectangle().height*2);//afficher la cadre du objet selectionn�
			
		}
		game.font.draw(game.batch, boutonRetour.getNom(), boutonRetour.pos.x+10, boutonRetour.pos.y+20); //Afficher la texte "retour"
		game.batch.end();
		
		r.setColor(Color.BLACK);
		r.rect(boutonRetour.pos.x+5, boutonRetour.pos.y-5, BOUTON_LARGEUR, BOUTON_HAUTEUR); //afficher la cadre du bouton retour
		r.end();
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, LARGEUR, HAUTEUR); // zome du camera
		
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		r.dispose();
		for(InventaireObjet ob:listObjets){
			ob.dispose();
		}
		fond.dispose();
		if(arme!=null)
			arme.dispose();
		if(armure!=null)
			armure.dispose();
		if(accessoire!=null)
			accessoire.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos.x = screenX * camera.viewportWidth/Gdx.graphics.getWidth();
		touchpos.y = (Gdx.graphics.getHeight() - screenY) * camera.viewportHeight/Gdx.graphics.getHeight();

		
		if(touchpos.overlaps(boutonRetour.pos)){ // toucher le bouton retour
			game.setScreen(scr);
			dispose();
		}else{//sinon
			for(InventaireObjet obj: listObjets){
				if(touchpos.overlaps(new Rectangle(obj.getRectangle().x, obj.getRectangle().y, obj.getRectangle().width*2, obj.getRectangle().height*2))){
					selectedObjet = obj;
					if(obj.estConsomable()){
						estPotion = true;
					}
					break;
				}
			}
			if(selectedObjet!=null){
				if(selectedObjet.estConsomable()){
					if(touchpos.overlaps(boutonUtiliser.pos)){
						selectedObjet.getObj().utiliser((Joueur) joueur.getEtre());
						listObjets.remove(selectedObjet);
						listObjetsJoueur.remove(selectedObjet.getObj());
						selectedObjet = null;
					}
					if(touchpos.overlaps(boutonDetruire.pos)){
						listObjets.remove(selectedObjet);
						listObjetsJoueur.remove(selectedObjet.getObj());
						selectedObjet = null;
					}
				}else if(selectedObjet.estEquipement()){
					if(touchpos.overlaps(boutonUtiliser.pos)){
						Equipement eq = equiper(joueur.getEtre(),selectedObjet); // retourne l'equipement equip�
						if(eq != null){
							InventaireObjet obj = new InventaireObjet(eq, selectedObjet.getRectangle().x, selectedObjet.getRectangle().y);
							listObjets.set(listObjets.indexOf(selectedObjet), obj);
							listObjetsJoueur.set(listObjetsJoueur.indexOf(selectedObjet.getObj()),eq);
						}else{
							listObjets.remove(selectedObjet);
							listObjetsJoueur.remove(selectedObjet.getObj());
						}
						selectedObjet = null;
					}
					if(touchpos.overlaps(boutonDetruire.pos)){
						listObjets.remove(selectedObjet);
						listObjetsJoueur.remove(selectedObjet.getObj());
						selectedObjet = null;
					}
				}
			}
		}
		return false;
	}
	
	private Equipement equiper(Etre etre, InventaireObjet obj){
		Equipement eq = null;
		if(obj.estEquipement()){
			if(obj.getObj().getClass()== Arme.class){
//				obj.setX(EQUIPEMENT_X);
//				obj.setY(ARME_Y);
				arme = new InventaireObjet(obj.getObj(),EQUIPEMENT_X,ARME_Y);
				eq = etre.getArme();
			}
			if(obj.getObj().getClass()== Armure.class){
//				obj.setX(EQUIPEMENT_X);
//				obj.setY(ARMURE_Y);
				armure = new InventaireObjet(obj.getObj(),EQUIPEMENT_X,ARMURE_Y);;
				eq = etre.getArmure();
			}
			if(obj.getObj().getClass()== Accessoire.class){
//				obj.setX(EQUIPEMENT_X);
//				obj.setY(ACCESSOIRE_Y);
				accessoire = new InventaireObjet(obj.getObj(),EQUIPEMENT_X,ACCESSOIRE_Y);;
				eq = etre.getAccessoire();
			}
			obj.getObj().utiliser(etre);
		}
		return eq;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
