package com.me.mygdxgame.screen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.dialogue.Dialogue;
import com.me.mygdxgame.dialogue.Phrase;
import com.me.mygdxgame.entite.Etre;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.entite.Monstre;
import com.me.mygdxgame.entite.Objet;
import com.me.mygdxgame.entite.Recrutable;
import com.me.mygdxgame.heros.Quete;
import com.me.mygdxgame.monde.Coffre;
import com.me.mygdxgame.monde.Monde;
import com.me.mygdxgame.monde.Personnage;

public class GameScreen implements Screen, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int GAME_TRANSITION = 0;
	public static final int GAME_RUNNING = 1;
	public static final int GAME_PAUSED = 2;
	public static final int GAME_OVER = 4;
	public static final int GAME_DISCUSSION = 3;
	private static final int VITESSE = 2;
	private static final int LG_PHRASES = 78;
	private static final float HAUTEUR = 800 * 6 / 10;
	private static final float LARGEUR = 480 * 6 / 10;
	private static TiledMap map;
	private static TiledMapTileLayer collisionLayer;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	public Joueur joueur;
	static Appli game;
	private static int screenX;
	private static int screenY;
	private int[] foreground = { 2 }, background = { 0, 1 };
	private Sprite spritej;
	Monde monde;
	private int nMap;
	private Dialogue dialogue;
	private int idReponse = -1;
	private int idOpDiag = 0;
	private String indiceQuete;
	private ArrayList<Phrase> reponses;
	ArrayList<Etre> heros;
	private TiledMapTileLayer foregroundLayer;

	public MenuBouton choixDial_1;
	public MenuBouton choixDial_2;
	public MenuBouton choixDial_3;
	public ShapeRenderer s;
	public Rectangle touchpos;

	private Texture textureBouton;
	private Texture textureBoutonInventaire;
	private Texture textureDialogue = new Texture(
			Gdx.files.internal("data/fonddialogue.png"));

	private ShapeRenderer r;
	private ShapeRenderer r2;
	private Texture iconTalpack;

	private boolean succesDebloque = false;
	private int cptAnimSucces = 0;
	private long debutAnimSucces;
	private boolean succesDown;
	private SpriteBatch batch2;
	private boolean recrueObtenu = false;
	private String titreSucces;
	private String textSucces;

	private boolean animCoffre = false;
	private ArrayList<Objet> objsCoffre;
	private int cptAnimCoffre = 0;
	private long debutCoffre;
	private boolean inventairePleinne;

	public GameScreen(Appli gam, int numeroMap, Joueur j/* , MidiPlayer m */) {
		// midiPlayer = m;
		nMap = numeroMap;
		game = gam;
		camera = new OrthographicCamera();// taille du viewPort
		map = new TmxMapLoader().load("data/maps/Map_" + Monde.MONDES[nMap]
				+ ".tmx");
		monde = new Monde(nMap);
		renderer = new OrthogonalTiledMapRenderer(map);
		collisionLayer = (TiledMapTileLayer) map.getLayers().get("Objets");
		foregroundLayer = (TiledMapTileLayer) map.getLayers().get("Foreground");
		joueur = j; // avec x,y position du joueur
		heros = new ArrayList<Etre>();
		heros.add(joueur);

		Vector2 depart = new Vector2(monde.getPositionDepart());
		joueur.setX((int) depart.x);
		joueur.setY((int) depart.y);
		joueur.setGame(this);
		screenX = joueur.getX() * 32;
		screenY = joueur.getY() * 32;
		Gdx.input.setInputProcessor(joueur);
		spritej = new Sprite();
		// midiPlayer.setLooping(true);
		// midiPlayer.setVolume(0.5f);
		//
		// //Play midi assets/data/midi/castlevania_stage1.mid
		// midiPlayer.open("data/Theme.mid");
		//
		// midiPlayer.play();
		s = new ShapeRenderer();
		r = new ShapeRenderer();
		r2 = new ShapeRenderer();
		batch2 = new SpriteBatch();
		iconTalpack = new Texture(
				Gdx.files.internal("data/images/saint_talpack.png"));

		s.setColor(Color.BLACK);
		choixDial_1 = new MenuBouton(1, "diag1", 9, Gdx.graphics.getHeight()
				* 0.5f - Gdx.graphics.getHeight() / 6,
				Gdx.graphics.getWidth() - 20, Gdx.graphics.getHeight() / 6);
		choixDial_2 = new MenuBouton(2, "diag2", 9, Gdx.graphics.getHeight()
				* 0.5f - Gdx.graphics.getHeight() * 2 / 6,
				Gdx.graphics.getWidth() - 20, Gdx.graphics.getHeight() / 6);
		choixDial_3 = new MenuBouton(3, "diag3", 9, Gdx.graphics.getHeight()
				* 0.5f - Gdx.graphics.getHeight() * 3 / 6 + 15,
				Gdx.graphics.getWidth() - 20, Gdx.graphics.getHeight() / 6 - 15);
		touchpos = new Rectangle(0, 0, 2, 2);

		textureBouton = new Texture(
				Gdx.files.internal("data/images/boutonMenu.png"));
		textureBoutonInventaire = new Texture(
				Gdx.files.internal("data/images/boutonInventaire.png"));
	}

	public Monde getMonde() {
		return monde;
	}

	public ArrayList<Etre> getListeHero() {
		return heros;
	}

	public GameScreen(Appli game, int nMap, Joueur joueur, int nMapPrec) {
		this(game, nMap, joueur);
		// *********
		if (nMapPrec == 1) {
			joueur.setX(12);
			joueur.setY(22);
			screenX = 12 * 32;
			screenY = 22 * 32;
			collisionLayer.getCell(12, 23).getTile().getProperties().clear();

			Cell cell = foregroundLayer.getCell(12, 23);
			TiledMapTile t = (TiledMapTile) map.getTileSets().getTile(0);
			cell.setTile(t);
			cell = foregroundLayer.getCell(12, 24);
			t = (TiledMapTile) map.getTileSets().getTile(0);
			cell.setTile(t);
			cell = collisionLayer.getCell(12, 23);
			// int id = cell.getTile().getId();
			cell.setTile(t);
			// cell.getTile().getProperties().remove("portail");
		} else if (nMapPrec == 3) {
			joueur.setX(17);
			joueur.setY(22);
			screenX = 17 * 32;
			screenY = 22 * 32;
			Cell cell = foregroundLayer.getCell(17, 23);
			TiledMapTile t = (TiledMapTile) map.getTileSets().getTile(0);
			cell.setTile(t);
			cell = foregroundLayer.getCell(17, 24);
			t = (TiledMapTile) map.getTileSets().getTile(0);
			cell.setTile(t);
			cell = collisionLayer.getCell(17, 23);
			cell.setTile(t);
			// cell.getTile().getProperties().remove("portail");
		}

	}

	public GameScreen(Appli game2, int nMonde, Joueur joueur2,
			Vector2 position, ArrayList<Etre> heros2,
			HashMap<Vector2, Coffre> coffres) {
		this(game2, nMonde, joueur2);
		// System.out.println("liste des heros" + heros2);
		ArrayList<Etre> listeheros = new ArrayList<Etre>();
		listeheros.add(joueur);
		listeheros.addAll(heros2);
		this.heros = listeheros;
		joueur.setX((int) position.x);
		joueur.setY((int) position.y);
		joueur.setGame(this);
		screenX = (int) position.x * 32;
		screenY = (int) position.y * 32;
		Collection<Coffre> poscoffres = coffres.values();
		for (Coffre coffre : poscoffres) {
			if (coffre.isDejaOuvert()) {
				Vector2 pos = coffre.getPosition();
				Cell cell = collisionLayer.getCell((int) pos.x, (int) pos.y);
				TiledMapTile t = (TiledMapTile) map.getTileSets().getTile(
						cell.getTile().getId() + 1);
				cell.setTile(t);
			}
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.position.set(screenX, screenY, 0);
		camera.update();
		renderer.setView(camera);// on indique la cam�ra utilis�e pour coupler
									// les syst�mes de coordonn�es
		// camera.rotate(1);
		// tell the SpriteBatch to render in the
		// coordinate system specified by the camera.
		renderer.getSpriteBatch().setProjectionMatrix(camera.combined);
		renderer.render(background);

		game.batch.setProjectionMatrix(camera.combined);

		game.batch.begin();

		spritej.setRegion(joueur.getAnimationTexture(Gdx.graphics
				.getDeltaTime()));
		// game.batch.draw(spritej, screenX, screenY);
		spritej.setBounds(screenX, screenY, 32, 64);
		spritej.draw(game.batch);

		if (joueur.etatgame == GAME_DISCUSSION) {
			affichDiscussion();
		}
		game.batch.end();

		if (joueur.etatgame == GAME_DISCUSSION) {
			s.begin(ShapeType.Line);
			s.rect(choixDial_1.pos.x, choixDial_1.pos.y, choixDial_1.pos.width,
					choixDial_1.pos.height);
			s.rect(choixDial_2.pos.x, choixDial_2.pos.y, choixDial_2.pos.width,
					choixDial_2.pos.height);
			s.rect(choixDial_3.pos.x, choixDial_3.pos.y, choixDial_3.pos.width,
					choixDial_3.pos.height);
			s.end();
		}
		if (joueur.etatgame != GAME_DISCUSSION) {
			renderer.render(foreground);
			renderer.getSpriteBatch().begin();
			renderer.getSpriteBatch().draw(
					textureBoutonInventaire,
					camera.position.x - camera.viewportWidth / 2 + 5,
					camera.position.y + camera.viewportHeight / 2
							- textureBouton.getHeight());
			renderer.getSpriteBatch().draw(
					textureBouton,
					camera.position.x + camera.viewportWidth / 2
							- textureBouton.getWidth() - 5,
					camera.position.y + camera.viewportHeight / 2
							- textureBouton.getHeight());
			renderer.getSpriteBatch().end();
		}

		afficherSucces();
		afficherObjCoffre();

		update();

	}

	/**
	 * @pre appeller dans la methode render()
	 */
	private void afficherObjCoffre() {
		if (animCoffre) {
			if (animCoffre && cptAnimCoffre < 80) { // animer coffre
				if (!inventairePleinne)
					drawTextObjsTrouvee(objsCoffre);
				else
					drawTextInventairePleinne();

				cptAnimCoffre += 2;
				if (cptAnimCoffre >= 80) { // pour eviter les affectations du
											// variable inutile(performance)
					debutCoffre = System.currentTimeMillis();
				}
			} else {// fin de l'animation
				if (System.currentTimeMillis() > debutCoffre + 1000) {// attendre
																		// un
																		// moment
																		// pour
																		// faire
																		// disparaitre
																		// les
																		// texts
					cptAnimCoffre = 0;
					animCoffre = false;
					objsCoffre = null;
					inventairePleinne = false;
				} else {
					if (!inventairePleinne)
						drawTextObjsTrouvee(objsCoffre);
					else
						drawTextInventairePleinne();
				}
			}
		}
	}

	private void drawTextObjsTrouvee(ArrayList<Objet> objsCoffre) {
		batch2.begin();
		game.font.setColor(Color.CYAN);
		game.font.setScale(1.3f);
		for (int i = 0; i < objsCoffre.size(); i++) {
			if (cptAnimCoffre - 40 * i > 0) {
				game.font.draw(batch2, objsCoffre.get(i).getNom(),
						HAUTEUR * 0.8f, (LARGEUR * 1f + 30 - 30 * i)
								+ cptAnimCoffre);
			}
		}
		batch2.end();
	}

	private void drawTextInventairePleinne() {
		batch2.begin();
		game.font.setColor(Color.RED);
		game.font.setScale(1.3f);
		game.font.draw(batch2, "Inventaire plein", camera.viewportWidth * 0.8f,
				(camera.viewportHeight * 1f + 30) + cptAnimCoffre);
		batch2.end();
	}

	/**
	 * @pre appeller dans la methode render()
	 */
	private void afficherSucces() {
		if (succesDebloque || recrueObtenu) {
			if (!succesDown && cptAnimSucces <= 80) {
				drawSucces(titreSucces, textSucces, 0, cptAnimSucces);
				debutAnimSucces = System.currentTimeMillis();
				cptAnimSucces += 2;
			} else {
				succesDown = true;
				if (System.currentTimeMillis() > debutAnimSucces + 2000) {
					if (cptAnimSucces > 0) {
						drawSucces(titreSucces, textSucces, 0, cptAnimSucces);
						cptAnimSucces -= 2;
					} else {
						succesDebloque = false;
						recrueObtenu = false;
						textSucces = "";
					}
				} else {
					drawSucces(titreSucces, textSucces, 0, cptAnimSucces);
				}
			}
		}
	}

	/**
	 * @param str
	 * @param addX
	 * @param addY
	 */
	private void drawSucces(String titre, String str, float addX, float addY) {
		r2.begin(ShapeType.Line);
		r.begin(ShapeType.Filled);
		r.setColor(Color.GRAY);
		r2.setColor(Color.WHITE);
		r2.rect(2 + addX, -90 + addY, camera.viewportWidth * 1.5f,
				camera.viewportHeight * 0.3f);
		r.rect(2 + addX, -90 + addY, camera.viewportWidth * 1.5f,
				camera.viewportHeight * 0.3f);
		r.end();
		r2.end();
		game.font.setColor(Color.WHITE);
		batch2.begin();
		batch2.draw(iconTalpack, 0 + addX, -85 + addY);
		game.font.setScale(2f);
		game.font.draw(batch2, titre, 100 + addX, -10 + addY);
		game.font.setScale(1.5f);
		game.font.draw(batch2, str, 100 + addX, -45 + addY);
		batch2.end();
	}

	public static String multiLine(String s) {
		int lg = s.length();
		if (lg < LG_PHRASES)
			return s;
		String sRetour = "";
		int i = LG_PHRASES, j = 0;
		while (i < lg) {
			for (; s.charAt(i) != ' '; --i)
				;
			sRetour += s.substring(j, i);
			sRetour += "\n";
			j = i + 1;
			i += LG_PHRASES;
		}
		sRetour += s.substring(j);
		return sRetour;
	}

	private void affichDiscussion() {

		game.font.setScale(0.9f);
		Vector2 basGauche = new Vector2(camera.position.x
				- camera.viewportWidth / 2, camera.position.y
				- camera.viewportHeight / 2);
		game.batch.draw(textureDialogue, basGauche.x, basGauche.y,
				camera.viewportWidth, camera.viewportHeight);
		game.font.setColor(Color.WHITE);

		if (idOpDiag >= 0) {
			Phrase question = dialogue.getListeOptions().get(idOpDiag)
					.getQuestion();
			game.font.drawMultiLine(game.batch, multiLine(question.getTexte()),
					basGauche.x + 9, basGauche.y + camera.viewportHeight - 9,
					camera.viewportWidth, HAlignment.LEFT);
			reponses = dialogue.getListeOptions().get(idOpDiag).getReponses();
			int hauteurTxt = 0;
			for (Phrase reponse : reponses) {
				if (reponse.getTexte().length() > LG_PHRASES)
					game.font.drawMultiLine(game.batch,
							multiLine(reponse.getTexte()), basGauche.x + 3,
							basGauche.y + camera.viewportHeight
									- camera.viewportHeight / 2 - hauteurTxt
									- 10, camera.viewportWidth,
							HAlignment.CENTER);
				else
					game.font.drawMultiLine(game.batch,
							multiLine(reponse.getTexte()), basGauche.x + 3,
							basGauche.y + camera.viewportHeight
									- camera.viewportHeight / 2 - hauteurTxt
									- 20, camera.viewportWidth,
							HAlignment.CENTER);
				hauteurTxt += camera.viewportHeight / 6;
			}

		}
		if (idReponse != -1 && idReponse < reponses.size()) {
			Phrase reponse = reponses.get(idReponse); // toutdoux
			idReponse = -1;
			idOpDiag = reponse.getIndex();
			switch (idOpDiag) {
			case 0:
				joueur.setDebut(System.currentTimeMillis());
				joueur.etatgame = GAME_RUNNING;
				// camera.setToOrtho(false, 800 * 6 / 10, 480 * 6 / 10);
				break;// fin dialogue
			case -1:
				joueur.etatgame = GAME_OVER;
				// vousEtesMort();
				break;
			case -2:
				combat(indiceQuete, joueur.getAvancement(indiceQuete));
				break;
			case -3:
				joueur.changerImg("tonneau");// Vous vous transformez en
				// tonneau
				joueur.etatgame = GAME_RUNNING;
				break;
			case -4:
				joueur.changerImg("hero");// vous vous detransformez
				joueur.etatgame = GAME_RUNNING;
				break;
			case -5:
				System.out.println("findumonde");
				finDuMonde();
				break;
			case -6:
				heros.add(Recrutable.getRecrue(indiceQuete));
				recrueObtenu = true;
				titreSucces = "Recrue Obtenue";
				textSucces = "Maintenant "
						+ Recrutable.getRecrue(indiceQuete).getNom()
						+ " se bat � vos cot�s";
				succesDown = false;
				if (Joueur.incrementeSucces(Monde.MONDES[nMap] + "RECRUE")) {
					Joueur.score += joueur.getAchievement(
							Monde.MONDES[nMap] + "RECRUE").getScore();
					animSucces(Monde.MONDES[nMap] + "RECRUE");
				}
				joueur.etatgame = GAME_RUNNING;
				break;
			default:
				idOpDiag--;
			}
			if (reponse.getAction() != 0)
				if (joueur.incrementeQuete(indiceQuete)) {// fin quete
					Joueur.score += joueur.getQuete(indiceQuete).getScore();
					Joueur.score += joueur.getAchievement(
							Quete.getSuccesByIndice(indiceQuete)).getScore();
					animSucces(Quete.getSuccesByIndice(indiceQuete));
				}
		}

	}

	private void animSucces(String indiceS) {
		// System.out.println(Joueur.score);
		succesDebloque = true;
		titreSucces = "Succ�s D�bloqu�";
		textSucces = Joueur.getNomSucces(indiceS);
		succesDown = false;
	}

	private void finDuMonde() {
		game.setScreen(new GameScreen(game, 0, joueur, nMap));
	}

	private void update() {
		// System.out.println(etatgame);
		switch (joueur.etatgame) {
		case GAME_TRANSITION:
			updateTransition();
			break;
		case GAME_RUNNING:
			updateRunning();
			break;
		case GAME_PAUSED:
			updatePaused();
			break;
		case GAME_OVER:
			// spritej.setOrigin(16, 25);// centre du joueur
			// if (spritej.getScaleY() > 0) {//ecrasement du joueur
			// spritej.setScale(spritej.getScaleX()+0.05f,
			// spritej.getScaleY()-0.05f);
			// }else
			// vousEtesMort();

			spritej.setOrigin(16, 28);// lespieds du joueur
			spritej.rotate(15);
			spritej.scale(-0.02f);
			updateRunning();
			if (spritej.getScaleX() < 0)
				vousEtesMort();
			// updateGameOver();
			break;
		case GAME_DISCUSSION:
			updateDiscussion();
			break;
		}
	}

	private void updateTransition() {

		joueur.setDir(Joueur.DIR_DEFAULT);
		spritej.setOrigin(16, 25);// centre du joueur
		if (spritej.getScaleX() > 0) {
			spritej.setScale(spritej.getScaleX() - 0.1f);
		} else {
			if (joueur.cptTrans > 0) {
				camera.rotate(joueur.cptTrans + 1);
				joueur.cptTrans--;
				// if (joueur.cptTrans == 45) {
				// portail(joueur.getX(), joueur.getY());
				// }
			} else {
				portail(joueur.getX(), joueur.getY());
				joueur.etatgame = GAME_RUNNING;
				joueur.cptTrans = 90;
			}
		}
		updateRunning();
	}

	private void updateRunning() {
		if (screenX == joueur.getX() * 32 && screenY == joueur.getY() * 32) {
			if (joueur.getNextDir() == Joueur.DIR_DEFAULT) {
				joueur.setFrameStat(joueur.getDir());
				joueur.setDir(Joueur.DIR_DEFAULT);
			} else {
				joueur.move();
				combatAleat();
			}
		} else {
			int dir = joueur.getDir();
			switch (dir) {
			case Joueur.DIR_GAUCHE:
				screenX -= VITESSE;
				break;
			case Joueur.DIR_DROITE:
				screenX += VITESSE;
				break;
			case Joueur.DIR_HAUT:
				screenY += VITESSE;
				break;
			case Joueur.DIR_BAS:
				screenY -= VITESSE;
				break;
			}
		}
		// System.out.println(screenX + dragon.getXp());
		camera.update();// important sinon pas de prise en compte des input
						// camera !!
	}

	private void combatAleat() {
		int combat = (int) (Math.random() * 200);

		if (combat == 1 && nMap != 0) {
			game.setScreen(new AnimCombatScreen(game, this,
					"data/images/mondes/" + Monde.MONDES[nMap]
							+ "/fond_combat.png", heros, monde
							.getListeCombat(1)));
			if (Joueur.incrementeSucces(Monde.MONDES[nMap] + "CG")) {
				Joueur.score += joueur
						.getAchievement(Monde.MONDES[nMap] + "CG").getScore();
				animSucces(Monde.MONDES[nMap] + "CG");
			}
		}
	}

	public static boolean isBlocked(int x, int y) {
		// collision du map
		// System.out.println(x + " " + y);
		Cell cell = collisionLayer.getCell(x, y);
		return cell != null && cell.getTile() != null
				&& cell.getTile().getProperties().containsKey("blocked");
	}

	public boolean isOther(int x, int y) {
		Cell cell = collisionLayer.getCell(x, y);
		if (cell == null || cell.getTile() == null)
			return false;
		if (cell.getTile().getProperties().containsKey("talk")) {
			discussion(x, y);
		} else if (cell.getTile().getProperties().containsKey("coffreF")) {
			ouvreCoffre(x, y);
		} else if (cell.getTile().getProperties().containsKey("portail")) {
			joueur.etatgame = GAME_TRANSITION;
			joueur.setX(x);
			joueur.setY(y);
		}
		return true;
	}

	private void portail(int x, int y) {
		// mapDesPortails: recup nMap en fonction de x et y
		nMap = Monde.getNumMap(x, y);
		game.setScreen(new GameScreen(game, nMap, joueur));
	}

	private void ouvreCoffre(int x, int y) {
		if (!joueur.inventairePleinne()) {
			Cell cell = collisionLayer.getCell(x, y);
			TiledMapTile t = (TiledMapTile) map.getTileSets().getTile(
					cell.getTile().getId() + 1);
			cell.setTile(t);
			if (monde.getCoffres(new Vector2(x, y)).getIndiceS() != null) {
				if (monde.getCoffres(new Vector2(x, y)).getIndiceS()
						.equals(Monde.MONDES[nMap] + "COFFRE")) {
					if (Joueur.incrementeSucces(Monde.MONDES[nMap] + "COFFRE")) {
						Joueur.score += joueur.getAchievement(
								Monde.MONDES[nMap] + "COFFRE").getScore();
						animSucces(Monde.MONDES[nMap] + "COFFRE");
					}
				} else if (monde.getCoffres(new Vector2(x, y)).getIndiceS()
						.equals(Monde.MONDES[nMap] + "TRESOR")) {
					Joueur.incrementeSucces(Monde.MONDES[nMap] + "TRESOR");
					Joueur.score += joueur.getAchievement(
							Monde.MONDES[nMap] + "TRESOR").getScore();
					animSucces(Monde.MONDES[nMap] + "TRESOR");
				}
			}
			ArrayList<Objet> objs = monde.getCoffres(new Vector2(x, y)).open();
			objsCoffre = objs; // pour afficher les noms des objets trouvees
			animCoffre = true;
			for (Objet o : objs) {
				joueur.addObjet(o);
			}
		} else {
			animCoffre = true;
			inventairePleinne = true;
		}
	}

	private void discussion(int x, int y) {
		// System.out.println("discussion, x = " + x + " , y = " + y);
		joueur.etatgame = GAME_DISCUSSION;
		Personnage perso = monde.getPersonnage(new Vector2(x, y));
		ArrayList<Dialogue> dialogues = perso.getDialogues();
		indiceQuete = perso.getIndiceQ();
		// System.out.println("quete : " + indiceQuete);
		dialogue = null;
		if (!perso.aDejaParle()) { // sile perso parle pour la 1ere fois
			perso.parle();
			String idSucces = perso.getIndiceS();
			// System.out.println("succes: " + idSucces);
			if (!idSucces.equals("")) // si le perso est associ� � un succes
				if (Joueur.incrementeSucces(idSucces)) {
					Joueur.score += joueur.getAchievement(idSucces).getScore();
					animSucces(idSucces);
				}
		}
		if (indiceQuete.equals("DEFAUT")) { // pas de quete
			dialogue = dialogues.get(0); // dialogue par defaut
		} else {
			int avancement = joueur.getAvancement(indiceQuete);
			for (Dialogue diag : dialogues) {
				if (diag.getEtatQuete() == avancement) {
					dialogue = diag;
					break;
				}
			}
			if (dialogue == null)
				dialogue = dialogues.get(0);
		}
		joueur.getDir();
		idOpDiag = 0;

	}

	private void updateDiscussion() {
		joueur.setFrameStat(joueur.getDir());
		joueur.setDir(Joueur.DIR_DEFAULT);

	}

	private void vousEtesMort() {
		Joueur.score = 0;
		game.setScreen(new GameOverScreen(game));
		dispose();
	}

	private void updatePaused() {
		joueur.setFrameStat(joueur.getDir());
		joueur.setDir(Joueur.DIR_DEFAULT);
	}

	private void combat(String indiceQuete, int avancement) {
		Monstre mQuete = Monstre.getMonstreQuete(indiceQuete, avancement);
		ArrayList<Monstre> monstres = new ArrayList<Monstre>();
		monstres.add(mQuete);
		game.setScreen(new AnimCombatScreen(game, this, "data/images/mondes/"
				+ Monde.MONDES[nMap] + "/fond_combat.png", heros, monstres));

	}

	@Override
	public void resize(int width, int height) {
		// w = Gdx.graphics.getWidth();
		// h = Gdx.graphics.getHeight();
		// System.out.println(h + " " + w);
		camera.setToOrtho(false, HAUTEUR, LARGEUR); // zome du camera
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(joueur);
		joueur.etatgame = GAME_RUNNING;
		joueur.setNextdir(Joueur.DIR_DEFAULT);
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		map.dispose();
		renderer.dispose();
		r.dispose();
		r2.dispose();
		batch2.dispose();
		iconTalpack.dispose();
	}

	public void setIdReponse(int i) {
		this.idReponse = i;

	}

	public Appli getGame() {
		return game;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public int getNMonde() {
		return nMap;
	}

}
