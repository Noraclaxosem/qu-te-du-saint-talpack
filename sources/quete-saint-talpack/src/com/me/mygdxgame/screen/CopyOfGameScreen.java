package com.me.mygdxgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Joueur;

public class CopyOfGameScreen implements Screen {
	private static final int FRAME_COLS = 4; // #1
	private static final int FRAME_ROWS = 4; // #2
	private static final int TAILLE_CASE = 23;
	Animation walkAnimation; // #3
	Texture walkSheet; // #4
	TextureRegion[] walkDroite; // #5
	TextureRegion[] walkHaut;
	TextureRegion currentFrame; // #7
	TextureRegion[][] tmp;
	private float posX;
	private float posY;
	float stateTime; // #8
	private Texture texture;
	private int mvtcpt = 0;
	private int dir = 0;
	private int nextdir = -1;
	private int rotat;
	private boolean cligno = false;
	private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
	private Vector2 milieuEcran;
	private float h;
	private float w; 
	private final float SPEED = 3;
	private Sprite joueur;
	
	private int[] background = new int[] {0}, foreground = new int[] {1};
    
    public CopyOfGameScreen(Appli game) {
    	
	}

	@Override
    public void render(float delta) {
    	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
//		System.out.println("x = "+camera.position.x + " y= "+camera.position.y);
        
        //animation personnage
		stateTime += Gdx.graphics.getDeltaTime(); // #15
		currentFrame = walkAnimation.getKeyFrame(stateTime, true); // #16

		if (mvtcpt < TAILLE_CASE * 0.7)
			if (Gdx.input.isKeyPressed(Keys.LEFT)) {
				nextdir = 2;
			} else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
				nextdir = 1;
			} else if (Gdx.input.isKeyPressed(Keys.UP)) {
				nextdir = 3;
			} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
				nextdir = 0;
			}
		//if(Gdx.input.isKeyPressed(Keys.Z)) cligno = !cligno;
		if (mvtcpt < TAILLE_CASE * 0.7 && Gdx.input.isTouched()){
			Vector3 touchpos = new Vector3();
			touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			
			if (touchpos.x<w/4 && touchpos.y>h/4 && touchpos.y<h*3/4) {
				nextdir = 2;
				
			} else if (touchpos.x > w*3/4 && touchpos.y>h/4 && touchpos.y<h*3/4) {
				nextdir = 1;
			} else if (touchpos.y < h/3 && touchpos.x > w/4 && touchpos.x < w*3/4) {
				nextdir = 3;
			} else if (touchpos.y >h*2/3 && touchpos.x > w/4 && touchpos.x < w*3/4) {
				nextdir = 0;
			}
		}
		//if (Gdx.input.isTouched()) System.out.println(Gdx.input.getX() + " x : y " + Gdx.input.getY());
		if(mvtcpt > 0){
			if (dir == 2 ) {
				joueur.setPosition(joueur.getX()-SPEED, joueur.getY());
				//joueur.setPosition(posX -= SPEED, posY);
				mvtcpt--;
				chgdir();
				//joueur.rotate(-mvtcpt*15);
				//sprite.rotate(-8);
			} else if (dir == 1) {
				joueur.setPosition(joueur.getX()+SPEED, joueur.getY());
				//joueur.setPosition(posX += SPEED, posY);
				mvtcpt--;
				chgdir();
				//joueur.rotate(mvtcpt*15);
				//sprite.rotate(8);
			} else if (dir == 3) {
				joueur.setPosition(joueur.getX(), joueur.getY()+SPEED);
				//joueur.setPosition(posX, posY += SPEED);
				mvtcpt--;
				chgdir();
				//joueur.scale(TAILLE_CASE-mvtcpt);
			} else if (dir == 0) {
				joueur.setPosition(joueur.getX(), joueur.getY()-SPEED);
				//joueur.setPosition(posX, posY -= SPEED);
				mvtcpt--;
				chgdir();
			}
		}else if (nextdir != -1) {
			mvtcpt = TAILLE_CASE;
			//if(dir==1)sprite.rotate(-8*TAILLE_CASE);
			//if(dir==2)sprite.rotate(8*TAILLE_CASE);
			dir = nextdir;
			nextdir = -1;
		} else {
			int index = 0;
			for (int j = 0; j < FRAME_COLS; j++) {
				walkDroite[index++] = tmp[dir][0];
			}
			
		}
	

		renderer.setView(camera);//on indique la cam�ra utilis�e pour coupler les syst�mes de coordonn�es        
		camera.position.set(joueur.getX()-milieuEcran.x,joueur.getY()-milieuEcran.y,0);
		
		renderer.render(background);
		//renderer.getSpriteBatch().setProjectionMatrix(camera.combined);
		renderer.getSpriteBatch().begin();
			renderer.getSpriteBatch().draw(joueur, joueur.getX(),joueur.getY());
		renderer.getSpriteBatch().end();
		
//		renderer.render(foreground);//rendu de la carte  
    }
 
    private void update() {
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			camera.position.x -= SPEED;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			camera.position.x += SPEED;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			camera.position.y += SPEED;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			camera.position.y -= SPEED;
		}
		
//		 camera.position.set(maptest.x - milieuEcran.x, maptest.y - milieuEcran.y, 0);
		 camera.update();//important sinon pas de prise en compte des input camera !!        
	}
    @Override
    public void show() {
    	TiledMap map = new TmxMapLoader().load("data/maps/Map_GI.tmx");

    	renderer = new OrthogonalTiledMapRenderer(map);

    	h = Gdx.graphics.getHeight();
    	w = Gdx.graphics.getWidth();

    	milieuEcran = new Vector2(w/2,h/2);
    	
    	camera = new OrthographicCamera(w,h);//taille du viewPort
    	
		//joueur = new Joueur(0,0);
		joueur.setSize(0.1f, 0.1f * joueur.getHeight() / joueur.getWidth());
		joueur.setOrigin(joueur.getWidth() / 2, joueur.getHeight() / 2);
		//joueur.setPosition(-joueur.getWidth() / 2, -joueur.getHeight() / 2);
		

//    	imgmaptest = new Texture("data/nain/nainFace.png");
    	
    	
    	// animation du personnage________________
    	/*
    	walkSheet = new Texture(Gdx.files.internal("data/hero.png")); // #9
		tmp = TextureRegion.split(walkSheet, walkSheet.getWidth() / FRAME_COLS,
				walkSheet.getHeight() / FRAME_ROWS); // #10
		walkDroite = new TextureRegion[1 * FRAME_ROWS];
		chgdir();
		walkAnimation = new Animation(0.085f, walkDroite); // #11

		
		stateTime = 0f; // #13

		posX = 100;
		posY =  100;
*/
    }

	@Override
    public void resize(int width, int height) {
		camera.setToOrtho(false,w,h);  //zome du camera
	}
	
	private void chgdir() {
		int index = 0;
		for (int j = 0; j < FRAME_COLS; j++) {
			walkDroite[index++] = tmp[dir][j];
		}
	}
 
    @Override
    public void hide() {
         
    }
 
    @Override
    public void pause() {
         
    }
 
    @Override
    public void resume() {
         
    }
 
    @Override
    public void dispose() {
		map.dispose();
		renderer.dispose();
    }
}
