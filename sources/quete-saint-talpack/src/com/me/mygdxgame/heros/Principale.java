package com.me.mygdxgame.heros;

public class Principale extends Quete {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Principale(String nom, int nbEtapes){
		super(nom, nbEtapes);
		score = 0;
		avancement = 0;
	}
	
	@Override
	public String getType() {
		return "Principale";
	}

}
