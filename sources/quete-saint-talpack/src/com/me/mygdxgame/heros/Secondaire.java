package com.me.mygdxgame.heros;

public class Secondaire extends Quete {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Secondaire(String nom, int score, int nbEtapes){
		super(nom, nbEtapes);
		this.score = score;
		avancement = 0;
	}
	public Secondaire(String nom, int score, int avancement, int nbEtapes){
		super(nom, nbEtapes);
		this.score = score;
		this.avancement = avancement;
	}
	
	@Override
	public String getType() {
		return "Secondaire";
	}

}
