package com.me.mygdxgame.heros;

public class Achievement extends MetaAchievement{

	private static final long serialVersionUID = 1L;
	
	private MetaAchievement meta;
	
	public Achievement(String nom, boolean cache, int debloque, int score, MetaAchievement meta){
		super(nom,cache,debloque,score);
		this.meta = meta;
	}

	public MetaAchievement getMeta() {
		return meta;
	}
	
	public boolean incrementeCompteur(){
		if (compteur != debloque)
			this.compteur++;
		if (compteur == debloque){
			this.isTermine = true;
			meta.incrementeCompteur();
		}
		return isTermine;
	}
	
	public String toString(){
		return super.toString() + " " + meta;
	}
}
