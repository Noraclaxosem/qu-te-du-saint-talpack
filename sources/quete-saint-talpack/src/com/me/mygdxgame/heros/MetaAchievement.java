package com.me.mygdxgame.heros;

import java.io.Serializable;

public class MetaAchievement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected boolean isTermine;
	protected String nom;
	protected int compteur;
	protected int debloque;
	protected boolean isCache;
	protected int score;

	public MetaAchievement(String nom, boolean cache, int debloque, int score) {
		this.nom = nom;
		compteur = 0;
		isTermine = false;
		isCache = cache;
		this.debloque = debloque;
		this.score = score;
	}

	public boolean isTermine() {
		return isTermine;
	}

	public String getNom() {
		return nom;
	}

	public boolean incrementeCompteur() {
		if (compteur != debloque)
			this.compteur++;
		if (compteur == debloque)
			this.isTermine = true;
		return isTermine;
	}

	public boolean isCache() {
		return isCache;
	}
	
	public String toString(){
		return nom + " " + isCache + " " + compteur + " " + isTermine;
	}

	public int getScore() {
		return score;
	}
}
