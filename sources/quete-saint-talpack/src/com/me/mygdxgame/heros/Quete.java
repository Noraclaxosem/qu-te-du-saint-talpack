package com.me.mygdxgame.heros;

import java.io.Serializable;

import com.me.mygdxgame.entite.Joueur;

public abstract class Quete implements Serializable {

	private static final long serialVersionUID = 1L;

	protected int avancement = 0;
	protected int score;
	private String nom;
	private final int nbEtapes;

	public Quete(String nom, int nb) {
		this.nom = nom;
		nbEtapes = nb;
	}

	public abstract String getType();

	public String getNom() {
		return nom;
	}

	public int getScore() {
		return score;
	}

	public int getAvancement() {
		return avancement;
	}

	public boolean incrementeAvancement(String indice) {
		avancement++;
		boolean finie = (avancement == nbEtapes);
		String nomSucces;
		if (finie) {
			nomSucces = getSuccesByIndice(indice);
			Joueur.incrementeSucces(nomSucces);
		}
		return finie;
	}

	public static String getSuccesByIndice(String indice) {
		if (indice.length() == 4)
			return (indice.substring(0, 2) + 'Q' + indice.substring(2, 4));
		else
			return (indice.substring(0, 2) + 'Q' + indice.substring(2, 3));
	}

	public String toString() {
		return nom + " " + avancement + " " + score;
	}
}
