package com.me.mygdxgame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;


public class CopyOfappli implements ApplicationListener {

	private static final int FRAME_COLS = 4; // #1
	private static final int FRAME_ROWS = 4; // #2
	private static final int TAILLE_CASE = 23;
	Animation walkAnimation; // #3
	Texture walkSheet; // #4
	TextureRegion[] walkDroite; // #5
	TextureRegion[] walkHaut;
	SpriteBatch spriteBatch; // #6
	TextureRegion currentFrame; // #7
	TextureRegion[][] tmp;
	private float h;
	private float l;
	private float posX;
	private float posY;
	float stateTime; // #8
	private OrthographicCamera camera;
	private Texture texture;
	private Sprite sprite;
	private int mvtcpt = 0;
	private int dir = 0;
	private int nextdir = -1;
	private TiledMap map;
	//private TiledMap map;
	//private OrthogonalTiledMapRenderer renderer;
	private OrthogonalTiledMapRenderer renderer;
	private Texture walkSheet2;
	private TextureRegion[] walktout;
	private Animation walkAnimation2;
	private TextureRegion currentFrame2;
	private int rotat;
	private boolean cligno = false;

	@Override
	public void create() {
			
		/*map = new TmxMapLoader().load("data/maps/desert.tmx");
		
		renderer = new OrthogonalTiledMapRenderer(map);
*/
		l = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1, h / l);

		walkSheet = new Texture(Gdx.files.internal("data/hero.png")); // #9
		tmp = TextureRegion.split(walkSheet, walkSheet.getWidth() / FRAME_COLS,
				walkSheet.getHeight() / FRAME_ROWS); // #10
		walkDroite = new TextureRegion[1 * FRAME_ROWS];
		chgdir();
		walkAnimation = new Animation(0.085f, walkDroite); // #11
		
		
		walkSheet2 = new Texture(Gdx.files.internal("data/hero.png")); // #9
		TextureRegion[][] tmp2 = TextureRegion.split(walkSheet, walkSheet.getWidth() / FRAME_COLS,
				walkSheet.getHeight() / FRAME_ROWS); // #10
		walktout = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++) {
			for (int j = 0; j < FRAME_COLS; j++) {
				walktout[index++] = tmp[i][j];
			}
		}
		
		chgdir();
		walkAnimation = new Animation(0.085f, walkDroite); // #11
		walkAnimation2 = new Animation(0.5f, walktout); // #11
		
		
		spriteBatch = new SpriteBatch(); // #12
		stateTime = 0f; // #13

		
		TmxMapLoader loader = new TmxMapLoader();//cr�ation du loader de carte
        map = loader.load("data/zeldat-terrain.tmx");//chargement de la carte
        renderer = new OrthogonalTiledMapRenderer(map);//cr�ation du renderer
         
        camera = new OrthographicCamera(1, h/l);//taille du viewPort
        //camera.position.set(h/2,l/2, 0);//position de la cam�ra dansle monde 2D...centr�e sur la carte (640*640) !!
        
		texture = new Texture(Gdx.files.internal("data/test2.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		TextureRegion region = new TextureRegion(texture, 0, 0, 128, 128);

		sprite = new Sprite(region);
		sprite.setSize(1f, 1f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		sprite.setPosition(-sprite.getWidth() / 2, -sprite.getHeight() / 2);
		posX = -sprite.getWidth() / 2;
		posY = -sprite.getHeight() / 2;

	}

	@Override
	public void render() {
		//Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT); // #14
		
		//renderer.render();
		
		stateTime += Gdx.graphics.getDeltaTime(); // #15
		currentFrame = walkAnimation.getKeyFrame(stateTime, true); // #16
		
		Sprite sprite2 = new Sprite(currentFrame);
		sprite2.setSize(0.1f, 0.1f * sprite2.getHeight() / sprite2.getWidth());
		sprite2.setOrigin(sprite2.getWidth() / 2, sprite2.getHeight() / 2);
		sprite2.setPosition(-sprite2.getWidth() / 2, -sprite2.getHeight() / 2);
		
		
		

		if (mvtcpt < TAILLE_CASE * 0.7)
			if (Gdx.input.isKeyPressed(Keys.LEFT)) {
				nextdir = 2;
			} else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
				nextdir = 1;
			} else if (Gdx.input.isKeyPressed(Keys.UP)) {
				nextdir = 3;
			} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
				nextdir = 0;
			}
		if(Gdx.input.isKeyPressed(Keys.Z)) cligno = !cligno;
		if (mvtcpt < TAILLE_CASE * 0.7 && Gdx.input.isTouched()){
			Vector3 touchpos = new Vector3();
			touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			
			if (touchpos.x<l/4 && touchpos.y>h/4 && touchpos.y<h*3/4) {
				nextdir = 2;
			} else if (touchpos.x > l*3/4 && touchpos.y>h/4 && touchpos.y<h*3/4) {
				nextdir = 1;
			} else if (touchpos.y < h/3 && touchpos.x > l/4 && touchpos.x < l*3/4) {
				nextdir = 3;
			} else if (touchpos.y >h*2/3 && touchpos.x > l/4 && touchpos.x < l*3/4) {
				nextdir = 0;
			}
		}
		//if (Gdx.input.isTouched()) System.out.println(Gdx.input.getX() + " x : y " + Gdx.input.getY());
		if(mvtcpt > 0){
			if(cligno)
				if((mvtcpt % TAILLE_CASE) < TAILLE_CASE/2)
					sprite2.setColor(Color.PINK);
				else
					sprite2.setColor(Color.BLUE);
			if (dir == 2 ) {
				sprite.setPosition(posX += 0.01, posY);
				mvtcpt--;
				chgdir();
				//sprite2.rotate(-mvtcpt*15);
				//sprite.rotate(-8);
			} else if (dir == 1) {
				sprite.setPosition(posX -= 0.01, posY);
				mvtcpt--;
				chgdir();
				//sprite2.rotate(mvtcpt*15);
				//sprite.rotate(8);
			} else if (dir == 3) {
				sprite.setPosition(posX, posY -= 0.01);
				mvtcpt--;
				chgdir();
				//sprite2.scale(TAILLE_CASE-mvtcpt);
			} else if (dir == 0) {
				sprite.setPosition(posX, posY += 0.01);
				mvtcpt--;
				chgdir();
			}
		}else if (nextdir != -1) {
			mvtcpt = TAILLE_CASE;
			//if(dir==1)sprite.rotate(-8*TAILLE_CASE);
			//if(dir==2)sprite.rotate(8*TAILLE_CASE);
			dir = nextdir;
			nextdir = -1;
		} else {
			int index = 0;
			for (int j = 0; j < FRAME_COLS; j++) {
				walkDroite[index++] = tmp[dir][0];
			}
		}
		

		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
			sprite.draw(spriteBatch);
			sprite2.draw(spriteBatch);

		spriteBatch.end();
		
	}

	private void chgdir() {
		int index = 0;
		for (int j = 0; j < FRAME_COLS; j++) {
			walkDroite[index++] = tmp[dir][j];
		}

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {


	}
}
