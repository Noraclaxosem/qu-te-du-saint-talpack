package com.me.mygdxgame.monde;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.entite.*;

public class Serialisation {

	private static ArrayList<Objet> objets;
	private static ArrayList<Consommable> consommables;

	static {
		objets = new ArrayList<Objet>();
		consommables = new ArrayList<Consommable>();

		objets.add(new Accessoire("Anneau Bel", "data/images/objets/anneau1.png",
				"Un anneau savant : +5 vie, +1 defense", new Effet(5, 0, 1)));
		objets.add(new Accessoire("Anneau Ctambule",
				"data/images/objets/anneau2.png", "Il est nocturne : +4 defense",
				new Effet(0, 0, 4)));
		objets.add(new Accessoire("Anneau Minal", "data/images/objets/anneau3.png",
				"Il n'est pas vraiment verbal +20 vie", new Effet(20, 0, 0)));
		objets.add(new Accessoire("Anneau Rv�gien",
				"data/images/objets/anneau4.png",
				"La puissance du nord...  +10 vie, +1 defense", new Effet(10,
						0, 1)));
		objets.add(new Accessoire("Anneau Stalgique",
				"data/images/objets/anneau5.png",
				"Mais, vous pleurez ! +5 vie, +2 defense", new Effet(5, 0, 2)));
		objets.add(new Accessoire("Anneau Stradamus",
				"data/images/objets/anneau6.png", "Chut. +10 vie, +3 defense",
				new Effet(10, 0, 3)));
		objets.add(new Accessoire("Anneau Vembre",
				"data/images/objets/anneau7.png", "C'est presque No�l +15 vie",
				new Effet(15, 0, 0)));
		objets.add(new Accessoire("Anneau Conchylicole",
				"data/images/objets/anneau8.png",
				"Il r�gne sur l'amer. +20 vie, +4 defense", new Effet(20, 0, 4)));

		objets.add(new Arme("Ep�e Dagogique", "data/images/objets/epee.png",
				"Vous allez apprendre des choses... +3 attaque", new Effet(0,
						3, 0)));
		objets.add(new Arme("Ep�e Conchylicole", "data/images/objets/sabre.png",
				"Non, ce n'est pas un coquillage. +10 vie, +7 attaque",
				new Effet(10, 7, 0)));
		objets.add(new Arme("Hache Arnement", "data/images/objets/hache.png",
				"Vous allez apprendre des choses... +10 vie, +3 attaque",
				new Effet(10, 3, 0)));
		objets.add(new Arme("Lance Lot", "data/images/objets/lance.png",
				"Et la dame du lac. +5 vie, +5 attaque", new Effet(5, 5, 0)));
		objets.add(new Arme("Arc Ha�que", "data/images/objets/arc.png",
				"C'est le bazar. +15 vie, +1 attaque", new Effet(15, 1, 0)));

		objets.add(new Armure("Armure Mure", "data/images/objets/armure1.png",
				"On chuchote. +15 vie, +1 defense", new Effet(15, 0, 1)));
		objets.add(new Armure("Armure �tre", "data/images/objets/armure2.png",
				"C'est d�goutant ! +10 vie", new Effet(10, 0, 0)));
		objets.add(new Armure("Armure Sule", "data/images/objets/armure3.png",
				"Elle est belle. +15 vie, +3 defense", new Effet(15, 0, 3)));
		objets.add(new Armure("Armure Ticaire", "data/images/objets/armure4.png",
				"Vous gratte-ce ? +30 vie, +1 defense", new Effet(30, 0, 1)));
		objets.add(new Armure("Armure Inoir", "data/images/objets/armure5.png",
				"�a ne s'utilise pas comme �a... +20 vie, +2 defense",
				new Effet(20, 0, 2)));
		objets.add(new Armure("Armure Banisme", "data/images/objets/armure6.png",
				"Belle construction. +5 vie, +2 defense", new Effet(5, 0, 2)));
		objets.add(new Armure("Armure Conchylicole",
				"data/images/objets/armure7.png",
				"On dirait une armure. +30 vie, +5 defense",
				new Effet(30, 0, 5)));

		consommables.add(new Consommable("Super mega potion vitamin�e bio",
				"data/images/objets/potion1.png",
				"De l'�nergie en fiole. +30 en vie", new Effet(30, 0, 0)));
		consommables.add(new Consommable("Vinasse",
				"data/images/objets/potion2.png",
				"C'est pas bon, mais �a se boit. +25 en vie", new Effet(25, 0,
						0)));
	}

	private static ArrayList<Objet> getObjsAleat() {
		int conso = (int) (Math.random() * consommables.size());
		int obj = (int) (Math.random() * objets.size());
		//System.out.println(conso + " " + obj);
		ArrayList<Objet> objs = new ArrayList<Objet>();
		objs.add(consommables.get(conso));
		objs.add(objets.get(obj));
		return objs;
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException {

		// ****************
		// *** MONDE GI ***
		// ****************

		HashMap<Vector2, Personnage> GIpersos = new HashMap<Vector2, Personnage>();
		GIpersos.put(new Vector2(15f, 54f), new Personnage("nain1",
				"GI/GIPNJ15.talpack", "GINAIN"));
		GIpersos.put(new Vector2(21f, 50f), new Personnage("chefindien",
				"GI/GIPNJ14.talpack", ""));
		GIpersos.put(new Vector2(25f, 49f), new Personnage("filleindienne",
				"GI/GIPNJ16.talpack", "GIRECRUE"));
		GIpersos.put(new Vector2(17f, 46f), new Personnage("indien1",
				"GI/GIPNJ13.talpack", ""));
		GIpersos.put(new Vector2(28f, 44f), new Personnage("indien2",
				"GI/GIPNJ8.talpack", ""));
		GIpersos.put(new Vector2(42f, 44f), new Personnage("americain1",
				"GI/GIPNJ10.talpack", "GIRECRUE"));
		GIpersos.put(new Vector2(47f, 44f), new Personnage("noir1",
				"GI/GIPNJ12.talpack", ""));
		GIpersos.put(new Vector2(49f, 42f), new Personnage("nain2",
				"GI/GIPNJ11.talpack", "GINAIN"));
		GIpersos.put(new Vector2(35f, 38f), new Personnage("noir2",
				"GI/GIPNJ7.talpack", ""));
		GIpersos.put(new Vector2(11f, 37f), new Personnage("vieux",
				"GI/GIPNJ6.talpack", ""));
		GIpersos.put(new Vector2(17f, 30f), new Personnage("nain3",
				"GI/GIPNJ5.talpack", "GINAIN"));
		GIpersos.put(new Vector2(32f, 14f), new Personnage("contremaitre",
				"GI/GIPNJ4.talpack", "GIMONDE"));
		GIpersos.put(new Vector2(18f, 12f), new Personnage("indien3",
				"GI/GIPNJ1.talpack", "GIMONDE"));
		GIpersos.put(new Vector2(41f, 11f), new Personnage("noir3",
				"GI/GIPNJ3.talpack", ""));
		GIpersos.put(new Vector2(24f, 7f), new Personnage("nain4",
				"GI/GIPNJ2.talpack", "GINAIN"));
		HashMap<Vector2, Coffre> GIcoffres = new HashMap<Vector2, Coffre>();
		GIcoffres.put(new Vector2(33f, 56f), new Coffre("totemcoffre1",
				getObjsAleat(), "GICOFFRE", new Vector2(33f, 56f)));
		GIcoffres.put(new Vector2(28f, 47f), new Coffre("totem1",
				getObjsAleat(), null, new Vector2(28f, 47f)));
		GIcoffres.put(new Vector2(15f, 41f), new Coffre("totemcoffreSP",
				getObjsAleat(), "GITRESOR", new Vector2(15f, 41f)));
		GIcoffres.put(new Vector2(36f, 41f), new Coffre("totemcoffre2",
				getObjsAleat(), "GICOFFRE", new Vector2(36f, 41f)));
		GIcoffres.put(new Vector2(29f, 40f), new Coffre("totem2",
				getObjsAleat(), null, new Vector2(29f, 40f)));
		GIcoffres.put(new Vector2(15f, 30f), new Coffre("totemcoffre3",
				getObjsAleat(), "GICOFFRE", new Vector2(15f, 30f)));
		GIcoffres.put(new Vector2(26f, 22f), new Coffre("totem3",
				getObjsAleat(), null, new Vector2(26f, 22f)));
		GIcoffres.put(new Vector2(23f, 17f), new Coffre("totemcoffre4",
				getObjsAleat(), "GICOFFRE", new Vector2(23f, 17f)));
		GIcoffres.put(new Vector2(35f, 8f), new Coffre("totemcoffre5",
				getObjsAleat(), "GICOFFRE", new Vector2(35f, 8f)));
		GIcoffres.put(new Vector2(46f, 6f), new Coffre("totem4",
				getObjsAleat(), null, new Vector2(46f, 6f)));
		Monde mapD�part = new Monde("GI", new Vector2(15f, 6f), GIpersos,
				GIcoffres);
		ObjectOutputStream fOut = new ObjectOutputStream(new FileOutputStream(
				"GI/map.talpack"));
		fOut.writeObject(mapD�part);
		fOut.close();
		Monde tmp = new Monde("GI");
		System.out.println("GI : " + tmp.equals(mapD�part));

		// ****************
		// *** MONDE PL ***
		// ****************

		HashMap<Vector2, Personnage> PLpersos = new HashMap<Vector2, Personnage>();
		PLpersos.put(new Vector2(39f, 32f), new Personnage("Hubert Lue",
				"PL/PLPNJ1.talpack", "PLMONDE"));
		PLpersos.put(new Vector2(54f, 38f), new Personnage("Bernard Val",
				"PL/PLPNJ2.talpack", "PLMONDE"));
		PLpersos.put(new Vector2(56f, 53f), new Personnage("Nain Fami",
				"PL/PLPNJ3.talpack", "PLNAIN"));
		PLpersos.put(new Vector2(48f, 12f), new Personnage("Nain Farctus",
				"PL/PLPNJ4.talpack", "PLNAIN"));
		PLpersos.put(new Vector2(27f, 35f), new Personnage("Nain Duction",
				"PL/PLPNJ5.talpack", "PLNAIN"));
		PLpersos.put(new Vector2(18f, 54f), new Personnage("Nain Dex",
				"PL/PLPNJ6.talpack", "PLNAIN"));
		PLpersos.put(new Vector2(23f, 21f), new Personnage(
				"Chasseuse Athena Vais", "PL/PLPNJ7.talpack", ""));
		PLpersos.put(new Vector2(9f, 19f), new Personnage(
				"Chef vampire zombie garou", "PL/PLPNJ8.talpack", ""));
		PLpersos.put(new Vector2(8f, 16f), new Personnage(
				"Sbire vampire zombie garou", "PL/PLPNJ9.talpack", ""));
		PLpersos.put(new Vector2(5f, 16f), new Personnage(
				"Sbire vampire zombie garou", "PL/PLPNJ10.talpack", ""));
		PLpersos.put(new Vector2(6f, 17f), new Personnage(
				"Sbire vampire zombie garou", "PL/PLPNJ11.talpack", ""));
		PLpersos.put(new Vector2(23f, 45f), new Personnage("Grasse ponette",
				"PL/PLPNJ12.talpack", ""));
		PLpersos.put(new Vector2(24f, 47f), new Personnage("Christella",
				"PL/PLPNJ13.talpack", ""));
		PLpersos.put(new Vector2(20f, 45f), new Personnage("Priscilla",
				"PL/PLPNJ14.talpack", ""));
		PLpersos.put(new Vector2(21f, 43f), new Personnage("Babella",
				"PL/PLPNJ15.talpack", ""));
		PLpersos.put(new Vector2(25f, 44f), new Personnage("Zabilla",
				"PL/PLPNJ16.talpack", ""));
		PLpersos.put(new Vector2(54f, 28f), new Personnage(
				"Vampire zombie garou fianc�", "PL/PLPNJ17.talpack", ""));
		PLpersos.put(new Vector2(54f, 27f), new Personnage("Lycella",
				"PL/PLPNJ18.talpack", ""));
		PLpersos.put(new Vector2(46f, 12f), new Personnage("Sage",
				"PL/PLPNJ19.talpack", ""));
		PLpersos.put(new Vector2(16f, 41f), new Personnage("Vieux",
				"PL/PLPNJ20.talpack", ""));
		HashMap<Vector2, Coffre> PLcoffres = new HashMap<Vector2, Coffre>();
		PLcoffres.put(new Vector2(41f, 27f), new Coffre("arcEnCiel",
				getObjsAleat(), null, new Vector2(41f, 27f)));
		PLcoffres.put(new Vector2(44f, 44f), new Coffre("arcEnCiel",
				getObjsAleat(), null, new Vector2(44f, 44f)));
		PLcoffres.put(new Vector2(6f, 57f), new Coffre("arcEnCiel",
				getObjsAleat(), null, new Vector2(6f, 57f)));
		PLcoffres.put(new Vector2(22f, 26f), new Coffre("arcEnCiel",
				getObjsAleat(), null, new Vector2(22f, 26f)));
		PLcoffres.put(new Vector2(7f, 12f), new Coffre("arcEnCielCoffre1",
				getObjsAleat(), "PLCOFFRE", new Vector2(7f, 12f)));
		PLcoffres.put(new Vector2(20f, 37f), new Coffre("arcEnCielCoffre2",
				getObjsAleat(), "PLCOFFRE", new Vector2(20f, 37f)));
		PLcoffres.put(new Vector2(16f, 57f), new Coffre("arcEnCielCoffre3",
				getObjsAleat(), "PLCOFFRE", new Vector2(16f, 57f)));
		PLcoffres.put(new Vector2(37f, 46f), new Coffre("arcEnCielCoffre4",
				getObjsAleat(), "PLCOFFRE", new Vector2(37f, 46f)));
		PLcoffres.put(new Vector2(36f, 12f), new Coffre("arcEnCielCoffre5",
				getObjsAleat(), "PLCOFFRE", new Vector2(36f, 12f)));
		PLcoffres.put(new Vector2(24f, 17f), new Coffre("arcEnCielCoffreSP",
				getObjsAleat(), "PLTRESOR", new Vector2(24f, 17f)));
		mapD�part = new Monde("PL", new Vector2(32f, 34f), PLpersos, PLcoffres);
		fOut = new ObjectOutputStream(new FileOutputStream("PL/map.talpack"));
		fOut.writeObject(mapD�part);
		fOut.close();
		tmp = new Monde("PL");
		System.out.println("PL : " + tmp.equals(mapD�part));

		// ********************
		// *** MONDE DEPART ***
		// ********************

		HashMap<Vector2, Personnage> DBpersos = new HashMap<Vector2, Personnage>();
		HashMap<Vector2, Coffre> DBObj = new HashMap<Vector2, Coffre>();
		mapD�part = new Monde("DB", new Vector2(17f, 7f), DBpersos, DBObj);
		fOut = new ObjectOutputStream(new FileOutputStream("DB/map.talpack"));
		fOut.writeObject(mapD�part);
		fOut.close();
		tmp = new Monde("DB");
		System.out.println("DB : " + tmp.equals(mapD�part));
	}
}
