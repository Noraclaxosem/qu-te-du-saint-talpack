package com.me.mygdxgame.monde;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.Appli;
import com.me.mygdxgame.entite.Equipement;
import com.me.mygdxgame.entite.Etre;
import com.me.mygdxgame.entite.Joueur;
import com.me.mygdxgame.entite.Objet;
import com.me.mygdxgame.heros.MetaAchievement;
import com.me.mygdxgame.heros.Quete;
import com.me.mygdxgame.screen.GameScreen;

public class Save implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final int VIE_MAX = 100;

	private int nMonde;
	private Vector2 position;
	private HashMap<String, Quete> quetes;
	private HashMap<String, MetaAchievement> achievements;
	private HashMap<Vector2, Coffre> coffres;
	private Vector<Objet> listObjets;
	private ArrayList<Etre> heros;
	private String nom;
	private int vie;
	private int attaque;
	private int defense;
	private String imageJoueur;

	private Equipement arme;

	private Equipement armure;

	private Equipement accessoire;

	private int score;

	public Save(GameScreen scr) {
		this.nMonde = scr.getNMonde();
		Joueur j = scr.getJoueur();
		this.quetes = j.getQuetes();
		this.achievements = j.getAchievements();
		this.position = new Vector2(j.getX(), j.getY());
		this.listObjets = j.getListeObjets();
		this.coffres = scr.getMonde().getCoffres();
		this.imageJoueur = j.getImageCourante();
		heros = new ArrayList<Etre>();
		for (int i = 1; i < scr.getListeHero().size(); i++) {
			heros.add(scr.getListeHero().get(i));
		}
		this.nom = j.getNom();
		this.vie = j.getVie();
		this.attaque = j.getAttaque();
		this.defense = j.getDefense();
		this.arme = j.getArme();
		this.armure = j.getArmure();
		this.accessoire = j.getAccessoire();
		this.score = Joueur.score;
	}

	public GameScreen getGameScreen(Appli game) {
		ArrayList<Etre> etres = new ArrayList<Etre>();
		System.out.println("liste au load: " + heros);
		etres.addAll(heros);
		GameScreen gs = new GameScreen(game, nMonde, new Joueur(nom, VIE_MAX,
				attaque, defense, quetes, achievements, listObjets, vie,
				imageJoueur,arme,armure,accessoire, score), position, etres, coffres);
		return gs;
	}
}
