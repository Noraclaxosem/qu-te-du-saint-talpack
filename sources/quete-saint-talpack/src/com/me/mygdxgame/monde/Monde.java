package com.me.mygdxgame.monde;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.entite.Monstre;

public class Monde implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int NB_MONSTRE_COMBAT = 4;
	public static final String[] MONDES = { "DB", "GI", "PH", "PL", "MA", "SF",
			"IUT" };

	private ArrayList<Monstre> listeMonstres;

	private String imageCombatFond;
	private String nom;
	private Vector2 positionDepart;
	private HashMap<Vector2, Personnage> personnages;
	private HashMap<Vector2, Coffre> coffres;

	public HashMap<Vector2, Personnage> getPersonnages() {
		return personnages;
	}

	public HashMap<Vector2, Coffre> getCoffres() {
		return coffres;
	}

	private static HashMap<Vector2, Integer> portails;
	static {
		portails = new HashMap<Vector2, Integer>();
		portails.put(new Vector2(12, 23), 1);
		portails.put(new Vector2(22, 23), 3);
		portails.put(new Vector2(17, 23), 3);
	}


	public Monde(String nomMonde) {
		listeMonstres = new ArrayList<Monstre>();
		ObjectInputStream fIn;
		try {
			fIn = new ObjectInputStream(Gdx.files.internal(
					"data/Serial/" + nomMonde + "/map.talpack").read());
//			fIn = new ObjectInputStream(new FileInputStream(nomMonde + "/map.talpack"));
			this.setMonde((Monde) fIn.readObject());
			fIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Monde(int numMonde) {

		try {
			ObjectInputStream fIn = new ObjectInputStream(Gdx.files.internal(
					"data/Serial/" + MONDES[numMonde] + "/map.talpack").read());
//			ObjectInputStream fIn = new ObjectInputStream(new FileInputStream(MONDES[numMonde] + "/map.talpack"));
			this.setMonde((Monde) fIn.readObject());
			fIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Monde(String nom, Vector2 positionDep,
			HashMap<Vector2, Personnage> persos, HashMap<Vector2, Coffre> coffre) {
		this.nom = nom;
		personnages = persos;
		coffres = coffre;
		positionDepart = positionDep;
		ObjectInputStream fIn;
		try {
//			fIn = new ObjectInputStream(Gdx.files.internal(
//					"data/serial/" + nom + "/listeMonstres.talpack").read());
			fIn = new ObjectInputStream(new FileInputStream(nom + "/listeMonstres.talpack"));
			listeMonstres = (ArrayList<Monstre>) fIn.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setMonde(Monde tmp) {
		nom = tmp.nom;
		personnages = tmp.personnages;
		coffres = tmp.coffres;
		positionDepart = tmp.positionDepart;
		listeMonstres = new ArrayList<Monstre>();
		listeMonstres = tmp.listeMonstres;
	}

	public String getNom() {
		return nom;
	}

	public String getCombatBackground() {
		return imageCombatFond;
	}

	public ArrayList<Monstre> getListeCombat(int niveau) {
		int randomNbMonstre = (int) ((int) NB_MONSTRE_COMBAT * Math.random()) + 1;

		ArrayList<Monstre> listeCombat = new ArrayList<Monstre>();
		for (int i = 0; i < randomNbMonstre; ++i) {
			int randomNumMonstre = (int) ((int) listeMonstres.size() * Math
					.random());
			listeCombat.add(calibrage(listeMonstres.get(randomNumMonstre),
					niveau));
		}
		return listeCombat;
	}

	private Monstre calibrage(Monstre m, int niveau) {
		int vie = m.getVie() + (int) (0.1 * m.getVie());
		int attaque = m.getAttaque() + (int) (0.1 * m.getAttaque());
		int defense = m.getDefense() + (int) (0.1 * m.getDefense());
		return new Monstre(m.getImage(), m.getNom(), vie, attaque, defense);
	}

	public Personnage getPersonnage(Vector2 position) {
		return personnages.get(position);
	}

	public Coffre getCoffres(Vector2 position) {
		return coffres.get(position);
	}

	public Vector2 getPositionDepart() {
		return positionDepart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coffres == null) ? 0 : coffres.hashCode());
		result = prime * result
				+ ((personnages == null) ? 0 : personnages.hashCode());
		result = prime * result
				+ ((positionDepart == null) ? 0 : positionDepart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monde other = (Monde) obj;
		if (coffres == null) {
			if (other.coffres != null)
				return false;
		} else if (!coffres.equals(other.coffres))
			return false;
		if (personnages == null) {
			if (other.personnages != null)
				return false;
		} else if (!personnages.equals(other.personnages))
			return false;
		if (positionDepart == null) {
			if (other.positionDepart != null)
				return false;
		} else if (!positionDepart.equals(other.positionDepart))
			return false;
		return true;
	}

	@Override
	public String toString() {

		return "Monde [listeMonstres=" + listeMonstres + ", imageCombatFond="
				+ imageCombatFond + ", nom=" + nom + ", positionDepart="
				+ positionDepart + ", personnages=" + personnages
				+ ", coffres=" + coffres + "]";
	}

	public static int getNumMap(int x, int y) {
		Vector2 v = new Vector2(x, y);
		return portails.get(v);
	}
}
