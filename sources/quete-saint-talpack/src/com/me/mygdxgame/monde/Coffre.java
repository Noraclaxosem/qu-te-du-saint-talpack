package com.me.mygdxgame.monde;

import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.entite.Objet;

public class Coffre implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private ArrayList<Objet> objets;
	private boolean dejaOuvert;
	private String indiceS;
	private Vector2 position;
	
	/*
	 * Faire attention, indiceS (Succ�s) peut �tre null (si il n'intervient dans aucun succ�s)
	 */
	public String getIndiceS() {
		return indiceS;
	}

	public boolean isDejaOuvert() {
		return dejaOuvert;
	}

	public Coffre(String nom, ArrayList<Objet> obj, String indiceS, Vector2 pos) {
		this.nom = nom;
		this.indiceS = indiceS;
		objets = obj;
		this.position = pos;
	}

	public ArrayList<Objet> open(){
		ArrayList<Objet> temp = new ArrayList<Objet>(objets);
		objets.clear();
		dejaOuvert = true;
		return temp;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coffre other = (Coffre) obj;
		if (indiceS == null) {
			if (other.indiceS != null)
				return false;
		} else if (!indiceS.equals(other.indiceS))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (objets == null) {
			if (other.objets != null)
				return false;
		} else if (!objets.equals(other.objets))
			return false;
		return true;
	}

	public String getNom() {
		return nom;
	}
	public ArrayList<Objet> getObjets() {
		return objets;
	}

	public Vector2 getPosition() {
		return position;
	}
}
