package com.me.mygdxgame.monde;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

import com.me.mygdxgame.dialogue.Dialogue;

public class Personnage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nom;
	private ArrayList<Dialogue> dialogues;
	private boolean dejaParle;
	private String indiceQ;
	private String indiceS;

	public Personnage(String nom, String fichierDialogue, String indiceS)
			throws IOException, ClassNotFoundException {
		this.nom = nom;

		indiceQ = "DEFAUT";
		dejaParle = false;
		ObjectInputStream fIn = new ObjectInputStream(new FileInputStream(
				fichierDialogue));
		this.dialogues = (ArrayList<Dialogue>) fIn.readObject();
		fIn.close();
		this.indiceS = indiceS;
		for (int i = 0; i < dialogues.size(); i++) {
			String tmp = dialogues.get(i).getIndexQuete();
			if (!tmp.equals("DEFAUT")) {
				indiceQ = tmp;
				break;
			}
		}

	}

	public String getNom() {
		return nom;
	}

	public ArrayList<Dialogue> getDialogues() {
		return dialogues;
	}

	public boolean aDejaParle() {
		return dejaParle;
	}

	public String getIndiceQ() {
		return indiceQ;
	}

	public String getIndiceS() {
		return indiceS;
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException {
		ArrayList<Personnage> perso = new ArrayList<Personnage>();
		for (int i = 1; i < 17; i++) {
			perso.add(new Personnage("PNJ" + i, "GI/GIPNJ" + i + ".talpack", ""));
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personnage other = (Personnage) obj;
		if (dialogues == null) {
			if (other.dialogues != null)
				return false;
		} else if (!dialogues.equals(other.dialogues))
			return false;
		if (indiceQ == null) {
			if (other.indiceQ != null)
				return false;
		} else if (!indiceQ.equals(other.indiceQ))
			return false;
		if (indiceS == null) {
			if (other.indiceS != null)
				return false;
		} else if (!indiceS.equals(other.indiceS))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	public void parle() {
		dejaParle = true;

	}

	@Override
	public String toString() {
		return "Personnage [nom=" + nom + ", dialogues=" + dialogues
				+ ", dejaParle=" + dejaParle + ", indiceQ=" + indiceQ
				+ ", indiceS=" + indiceS + "]";
	}
}
